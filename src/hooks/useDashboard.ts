import api from "@app/services/api";
import {
  ApiResponse,
} from "@app/services/api/models";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { ChangePasswordParamModel, ListParamModel, PauseResumeDeleteParamModel, SaveProfileParamModel } from "@app/store/dashboard/dashboard-models";
import { EnquiriesResult, MyPropertiesResult, ProfileResult, SavedSearchResult } from "@app/services/api/models/dashboard";
import { onEnquiriesSuccessAction, onMyPropertiesSuccessAction, onProfileDataSuccessAction, onSavedSearchSuccessAction } from "@app/store/dashboard/dashboard-actions";
import { ToasterReferences } from "@app/services/toast-service";

interface Config {
  isLoading: boolean;
  passwordSuccess: boolean;
  getSavedSearchList: (
    params?: ListParamModel
  ) => void;
  pauseResumeSavedSearch: (
    params?: PauseResumeDeleteParamModel
  ) => void;
  deleteSavedSearch: (
    params?: PauseResumeDeleteParamModel
  ) => void;
  getProfileData: (
    userId?: string
  ) => void;
  changePassword: (
    params?: ChangePasswordParamModel
  ) => void;
  saveProfile: (
    params?: SaveProfileParamModel
  ) => void;
  getMyPropertiesList: (
    params?: ListParamModel
  ) => void;
  getEnquiriesList: (
    params?: ListParamModel
  ) => void;
}

const useDashboard = (): Config => {
  const [isLoading, setLoader] = useState(false);
  const [passwordSuccess, setPasswordSuccess] = useState(false);
  const dispatch = useDispatch();
  const getSavedSearchList = async (params?: ListParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<SavedSearchResult> = await api.dashboard.getSavedSearchList(params);
      if (response.responseStatus == "Success") {
        dispatch(onSavedSearchSuccessAction(response.data ?? new SavedSearchResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const pauseResumeSavedSearch = async (params?: PauseResumeDeleteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.dashboard.pauseResumeSavedSearch(params);
      if (response.responseStatus == "Success") {
        const listParamModel: ListParamModel = new ListParamModel();
        listParamModel.userID = params?.userID;
        listParamModel.page = 0;
        getSavedSearchList(listParamModel)
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const deleteSavedSearch = async (params?: PauseResumeDeleteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.dashboard.deleteSavedSearch(params);
      if (response.responseStatus == "Success") {
        const listParamModel: ListParamModel = new ListParamModel();
        listParamModel.userID = params?.userID;
        listParamModel.page = 0;
        getSavedSearchList(listParamModel)
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getProfileData = async (userId?: string) => {
    try {
      // setLoader(true);
      let response: ApiResponse<any> = await api.dashboard.getProfileData(userId);
      if (response.responseStatus == "Success") {
        dispatch(onProfileDataSuccessAction(response.data ?? new ProfileResult()))
      }
    } catch (err) {
    } finally {
      // setLoader(false);
    }
  };

  const changePassword = async (params?: ChangePasswordParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.dashboard.changePassword(params);
      if (response.responseStatus == "Success") {
        setPasswordSuccess(true)
        ToasterReferences.showSucessMsg(response?.message ?? "");
      } else {
        ToasterReferences.showSucessMsg(response?.message ?? "");
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const saveProfile = async (params?: SaveProfileParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.dashboard.saveProfile(params);
      if (response.responseStatus == "Success") {
        setPasswordSuccess(true)
        ToasterReferences.showSucessMsg(response?.message ?? "");
      } else {
        ToasterReferences.showSucessMsg(response?.message ?? "");
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getMyPropertiesList = async (params?: ListParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<MyPropertiesResult> = await api.dashboard.getMyPropertiesList(params);
      if (response.responseStatus == "Success") {
        dispatch(onMyPropertiesSuccessAction(response.data ?? new MyPropertiesResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getEnquiriesList = async (params?: ListParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<EnquiriesResult> = await api.dashboard.getEnquiriesList(params);
      if (response.responseStatus == "Success") {
        dispatch(onEnquiriesSuccessAction(response.data ?? new EnquiriesResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  return {
    isLoading,
    getSavedSearchList,
    pauseResumeSavedSearch,
    deleteSavedSearch,
    getProfileData,
    changePassword,
    passwordSuccess,
    saveProfile,
    getMyPropertiesList,
    getEnquiriesList
  };
};



export default useDashboard;


