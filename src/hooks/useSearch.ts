import api from "@app/services/api";
import {
  ApiResponse,
  Attribute,
  AttributesResponse,
  BedRoomAttribute,
  BedroomResponse,
  BuyRentPrice,
  CityList,
  LanguageList,
  PropertyType,
  RegionLocation,
  RegionResult,
} from "@app/services/api/models";
import { ILocale } from "@app/store/settings/settings-models";
import { IAppState } from "@app/store/state-props";
import { useState } from "react";
import { Settings } from "react-native-fbsdk";
import { useSelector } from "react-redux";
import { PropertySearchParamModel } from "@app/store/search/search-models";
import { IKeyValuePair } from "@app/components/common/widgets/form/dropdown";
import { translate } from "@app/config/locale/i18n";

interface Config {
  isLoading: boolean;
  locations: RegionLocation[];
  projects: RegionLocation[];
  getRegions: (term: string, isProject?: boolean) => void;
  searchParams: PropertySearchParamModel;
  getAttributes: (typeId: string) => void;
  attributes: Attribute[];
  getBedRooms:(typeId:string) => void;
  bedRooms:IKeyValuePair[]
}

const useSearch = (): Config => {
  const search = useSelector((state: IAppState) => state.search);
  const [isLoading, setLoader] = useState(false);
  const [locations, setLocations] = useState<RegionLocation[]>([]);
  const [projects, setProjects] = useState<RegionLocation[]>([]);
  const [attributes, setAttributes] = useState<Attribute[]>([]);
  const [bedRooms, setBedRooms] = useState<IKeyValuePair[]>([]);
  const getRegions = async (term: string, isProject?: boolean) => {
    if (term.length > 0) {
      try {
        setLoader(true);
        let response: ApiResponse<RegionResult> = await api.search.getRegionApi(
          term,
          isProject
        );
        if (response.responseStatus == "Success") {
          setLocations(response.data?.location ?? []);
          setProjects(response.data?.projects ?? []);
        }
      } catch (err) {
      } finally {
        setLoader(false);
      }
    }
  };

  const getAttributes = async (typeId: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<AttributesResponse> = await api.search.getAttributesApi(
        typeId
      );
      if (response.responseStatus == "Success") {
        setAttributes(response.data?.allAttributes ?? []);
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getBedRooms = async (typeId: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<BedroomResponse> = await api.search.getBedroomsApi(
        typeId
      );
      if (response.responseStatus == "Success") {
        let bedRooms: IKeyValuePair[] = [];
        response.data?.bedroom?.map((item) => {
          bedRooms.push({
            key: `${item.attributeID}~${item.attrOptionID}`,
            value: `${item.attrOptName} ${translate("search.bedrooms")}`,
          });
        });
        setBedRooms(bedRooms);
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  return {
    isLoading,
    locations,
    projects,
    getRegions,
    searchParams: search.propertySearchParams,
    getAttributes,
    attributes,
    getBedRooms,
    bedRooms
  };
};

export default useSearch;
