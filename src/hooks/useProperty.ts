import api from "@app/services/api";
import {
  ApiResponse,
  PropertyListApiResponse,
} from "@app/services/api/models";
import { IAppState } from "@app/store/state-props";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ToasterReferences } from "@app/services/toast-service";
import { PauseResumeDeleteParamModel } from "@app/store/dashboard/dashboard-models";
import { DeletePropertySuccessAction } from "@app/store/dashboard/dashboard-actions";

interface Config {
  isLoading: boolean;
  getPropertyList: (
    page?: number
  ) => Promise<ApiResponse<PropertyListApiResponse>>;
  deleteProperty: (
    param?: PauseResumeDeleteParamModel
  ) => void;
}

const useProperty = (): Config => {
  const dispatch = useDispatch();
  const search = useSelector((state: IAppState) => state.search);
  const property = useSelector((state: IAppState) => state.property);
  const [isLoading, setLoader] = useState(false);
  const getPropertyList = async (page?: number):Promise<ApiResponse<PropertyListApiResponse>> => {
    search.propertySearchParams.page = page ?? 0;
    return api.property.getPropertyList(search.propertySearchParams);
    // try {
    //   setLoader(true);
    //   let response: ApiResponse<PropertyListApiResponse> = await api.property.getPropertyList(
    //     search.propertySearchParams
    //   );
    //   // if (response.responseStatus == "Success") {
    //   // dispatch(SetPropertyListAction, PropertyListApiResponse);
    //   return response.data?.propertyLists ?? [];
    //   // }
    // } catch (err) {
    //   return [];
    // } finally {
    //   setLoader(false);
    // }
  };

  const deleteProperty = async (params?: PauseResumeDeleteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.property.deleteProperty(params);
      if (response.responseStatus == "Success") {
        dispatch(DeletePropertySuccessAction(params?.id ?? ''))
        ToasterReferences.showSucessMsg("Deleted" ?? "");
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  return {
    isLoading,
    getPropertyList,
    deleteProperty
  };
};

export default useProperty;
