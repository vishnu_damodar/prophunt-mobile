import api from "@app/services/api";
import {
  ApiResponse,
} from "@app/services/api/models";
import { useState } from "react";
import { ShortlistedAgentResult, ShortlistedCountResult, ShortlistedDeveloperResult, ShortlistedExpertResult, ShortlistedProjectResult, ShortlistedPropertyResult } from "@app/services/api/models/shortlist";
import { onShortlistedAgentSuccessAction, onShortlistedCountSuccessAction, onShortlistedDeveloperSuccessAction, onShortlistedExpertSuccessAction, onShortlistedProjectSuccessAction, onShortlistedPropertySuccessAction, SetShortlistedAgentListAction, SetShortlistedDeveloperListAction, SetShortlistedExpertListAction, SetShortlistedProjectListAction, SetShortlistedPropertyListAction } from "@app/store/shortlist/shortlist-actions";
import { useDispatch } from "react-redux";
import { UnFavoriteParamModel } from "@app/store/shortlist/shortlist-models";
import { ToasterReferences } from "@app/services/toast-service";

interface Config {
  isLoading: boolean;
  getShortlistedCount: (
    userId?: string
  ) => void;
  getShortlistedPropertyList: (
    userId?: string
  ) => void;
  getShortlistedProjectList: (
    userId?: string
  ) => void;
  getShortlistedAgentList: (
    userId?: string
  ) => void;
  getShortlistedDeveloperList: (
    userId?: string
  ) => void;
  getShortlistedExpertList: (
    userId?: string
  ) => void;
  unFavoriteProperty: (
    params?: UnFavoriteParamModel
  ) => void;
  unFavoriteProject: (
    params?: UnFavoriteParamModel
  ) => void;
  unFavoriteUser: (
    params?: UnFavoriteParamModel
  ) => void;
}

const useShortlist = (): Config => {
  const [isLoading, setLoader] = useState(false);
  const dispatch = useDispatch();
  const getShortlistedCount = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedCountResult> = await api.shortlist.getShortlistedCount(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedCountSuccessAction(response.data ?? new ShortlistedCountResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };
  const getShortlistedPropertyList = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedPropertyResult> = await api.shortlist.getShortlistedProperties(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedPropertySuccessAction(response.data ?? new ShortlistedPropertyResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getShortlistedProjectList = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedProjectResult> = await api.shortlist.getShortlistedProjects(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedProjectSuccessAction(response.data ?? new ShortlistedProjectResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getShortlistedAgentList = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedAgentResult> = await api.shortlist.getShortlistedAgents(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedAgentSuccessAction(response.data ?? new ShortlistedAgentResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getShortlistedDeveloperList = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedDeveloperResult> = await api.shortlist.getShortlistedDevelopers(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedDeveloperSuccessAction(response.data ?? new ShortlistedDeveloperResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const getShortlistedExpertList = async (userId?: string) => {
    try {
      setLoader(true);
      let response: ApiResponse<ShortlistedExpertResult> = await api.shortlist.getShortlistedExperts(userId);
      if (response.responseStatus == "Success") {
        dispatch(onShortlistedExpertSuccessAction(response.data ?? new ShortlistedExpertResult()))
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const unFavoriteProperty = async (params?: UnFavoriteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.shortlist.unFavoriteProperty(params);
      if (response.responseStatus.toLowerCase() == "success") {
        ToasterReferences.showSucessMsg(response?.message ?? "");
        getShortlistedCount(params?.userID)
        getShortlistedPropertyList(params?.userID)
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const unFavoriteProject = async (params?: UnFavoriteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.shortlist.unFavoriteProject(params);
      if (response.responseStatus.toLowerCase() == "success") {
        ToasterReferences.showSucessMsg(response?.message ?? "");
        getShortlistedCount(params?.userID);
        getShortlistedProjectList(params?.userID);
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  const unFavoriteUser = async (params?: UnFavoriteParamModel) => {
    try {
      setLoader(true);
      let response: ApiResponse<any> = await api.shortlist.unFavoriteUser(params);
      if (response.responseStatus.toLowerCase() == "success") {
        ToasterReferences.showSucessMsg(response?.message ?? "");
        getShortlistedCount(params?.userID);
        (params?.option == 3)
          ? getShortlistedAgentList(params?.userID)
          : (params?.option == 4) ? getShortlistedDeveloperList(params?.userID)
            : (params?.option == 5) ? getShortlistedExpertList(params?.userID)
              : null
      }
    } catch (err) {
    } finally {
      setLoader(false);
    }
  };

  return {
    isLoading,
    getShortlistedCount,
    getShortlistedPropertyList,
    getShortlistedProjectList,
    getShortlistedAgentList,
    getShortlistedDeveloperList,
    getShortlistedExpertList,
    unFavoriteProperty,
    unFavoriteProject,
    unFavoriteUser
  };
};

export default useShortlist;
