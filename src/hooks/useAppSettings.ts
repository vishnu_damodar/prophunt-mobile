import {
  BuyRentPrice,
  CityList,
  LanguageList,
  PropertyType,
} from "@app/services/api/models";
import { ILocale } from "@app/store/settings/settings-models";
import { IAppState } from "@app/store/state-props";
import { useState } from "react";
import { Settings } from "react-native-fbsdk";
import { useSelector } from "react-redux";

interface Config {
  currency: string;
  unit:string;
  locale: ILocale;
  languageList: LanguageList[];
  buyMinPrices: BuyRentPrice[];
  buyMaxPrices: BuyRentPrice[];
  rentMinPrices: BuyRentPrice[];
  rentMaxPrices: BuyRentPrice[];
  propertyTypes: PropertyType[];
  cityList: CityList[];
}

const useAppSettings = (): Config => {
  const settings = useSelector((state: IAppState) => state.settings);
  return {
    ...settings,
    currency: settings.currency,
    unit:settings.unit,
    languageList: settings.LanguageList,
    buyMinPrices: settings.commonListings?.buyMinPrice ?? [],
    buyMaxPrices: settings.commonListings?.buyMaxPrice ?? [],
    rentMinPrices: settings.commonListings?.rentMinPrice ?? [],
    rentMaxPrices: settings.commonListings?.rentMaxPrice ?? [],
    propertyTypes: settings.commonListings?.propertyType ?? [],
  };
};

export default useAppSettings;
