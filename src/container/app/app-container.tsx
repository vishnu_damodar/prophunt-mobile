import {StatusBar, View, Text} from 'react-native';
import React, {useState, useEffect} from 'react';
import {AppColors} from '@app/config/ui/color';
import Routes from '@app/route/routes';
import {NavigationService} from '@app/services/navigation-service';
import {Root} from 'native-base';
import {ToasterReferences} from '@app/services/toast-service';
import {translate} from '@app/config/locale/i18n';
import {useNetInfo} from '@react-native-community/netinfo';

export const AppContainer = () => {
  const netInfo = useNetInfo();
  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    if (!netInfo.isConnected && isLoaded) {
      ToasterReferences.showErrorMsg(translate('common.noInternet',''));
    }
  }, [netInfo]);
  useEffect(() => {
    setIsLoaded(true);
  }, []);

  return (
    <Root>
      <StatusBar
        barStyle="light-content"
        backgroundColor={AppColors.statusBarColor}
      />

      <Routes
        ref={(navigatorRef: any) => {
          NavigationService.initNavigationService(navigatorRef);
        }}
      />
    </Root>
  );
};
