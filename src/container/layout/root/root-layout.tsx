import React, { Fragment } from "react";
import { FooterComponent } from "@app/components/common/footer";
import { NormalHeader } from "@app/components/common/normal-header";
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  TouchableWithoutFeedback,
  StatusBar,
} from "react-native";
import { AppColors } from "@app/config/ui/color";

export const RootLayout: React.FunctionComponent<LayoutOptions> = (
  props: LayoutOptions = {
    showBackButton: false,
    showSearchIcon: false,
    headerType: HeaderType.Normal,
    showFooter: true,
  }
) => {
  const headerMapper: any = {};
  headerMapper[HeaderType.Normal] = (
    <NormalHeader
      title={props.title}
      subTitle={props.subTitle}
      showBackButton={props.showBackButton}
      showSearchIcon={props.showSearchIcon}
    ></NormalHeader>
  );
  if (props.headerType === HeaderType.Custom) {
    headerMapper[HeaderType.Custom] = props.customHeader ?? <View></View>;
  }

  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: AppColors.primaryThemeColor,
        }}
      />
      <StatusBar barStyle="dark-content" />
      <SafeAreaView
        style={{ flex: 1, backgroundColor: AppColors.primaryThemeColor }}
      >
        {headerMapper[props.headerType]}
        <View style={{ flex: 1, backgroundColor: AppColors.lightColor }}>
          <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : undefined}
            style={{
              flex: 1,
            }}
          >
            {props.children}
          </KeyboardAvoidingView>
        </View>

        {props.showFooter && <FooterComponent />}
      </SafeAreaView>
    </Fragment>
  );
};

export enum HeaderType {
  Normal,
  Custom,
}

export interface LayoutOptions {
  headerType: HeaderType;
  title?: string;
  subTitle?: string;
  showBackButton?: boolean;
  showSearchIcon?: boolean;
  filterButtonAction?: any;
  children?: any;
  customHeader?: any;
  showFooter?: boolean;
}
