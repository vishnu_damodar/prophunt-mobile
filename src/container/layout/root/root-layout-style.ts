import { StyleSheet, PixelRatio } from "react-native";
import { AppColors } from "@app/config/ui/color";
import { Fonts } from "@app/config/ui/font";
import { ViewportService } from "@app/config/ui/viewport-service";

export const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#6CC627",
        justifyContent: "center",
        alignItems: "center"
    },
    body: {
        flexDirection: "row",

        justifyContent: "center",
        width: ViewportService.width-120
        // marginTop:10
    },
    text: {
        fontFamily: Fonts.Bold,
        fontSize: PixelRatio.roundToNearestPixel(20)
    },
    textItaclic: {
        fontStyle: "italic"
    },
    textTM: {
        fontSize: PixelRatio.roundToNearestPixel(10),
        marginRight: 5

        // marginTop: 2
    }
});
