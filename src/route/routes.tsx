import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { AppRoutes } from "./route-keys";
import { AuthCheckerScreen } from "@app/screens/auth-check";
import { SlidingScreen } from "@app/screens/slider";
import { LandingScreen } from "@app/screens/landing";
import { LoginScreen } from "@app/screens/login";
import { RegisterScreen } from "@app/screens/register";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SideBar } from "@app/components/common/widgets/sidebar/side-bar";
import { DrawerScreen } from "@app/screens/landing/components/drawer";
import { SearchScreen } from "@app/screens/search";
import { PropertyListingScreen } from "@app/screens/property-listing";
import { DashboardScreen } from "@app/screens/dashboard";

const UnAuthenticatedStack = createStackNavigator(
  {
    [AppRoutes.Landing]: {
      screen: LandingScreen,
    },
    [AppRoutes.Search]: {
      screen: SearchScreen,
    },
    [AppRoutes.PropertyListing]: {
      screen: PropertyListingScreen,
    },
  },
  {
    initialRouteName: AppRoutes.Landing,
    headerMode: "none",
  }
);

const AuthStack = createStackNavigator(
  {
    [AppRoutes.Slider]: {
      screen: SlidingScreen,
    },
    [AppRoutes.Login]: {
      screen: LoginScreen,
    },
    [AppRoutes.Register]: {
      screen: RegisterScreen,
    },
    [AppRoutes.Search]: {
      screen: SearchScreen,
    },
    [AppRoutes.Landing]: {
      screen: LandingScreen,
    },
    [AppRoutes.Dashboard]: {
      screen: DashboardScreen,
    },
  },
  {
    initialRouteName: AppRoutes.Slider,
    headerMode: "none",
  }
);
const DrawerScreens = createDrawerNavigator(
  {
    [AppRoutes.Landing]: {
      screen: UnAuthenticatedStack,
    },
  },
  {
    initialRouteName: AppRoutes.Landing,
    contentComponent: DrawerScreen,
    drawerWidth: ViewportService.width / 1.2,
    drawerLockMode: "locked-closed",
    // drawerType: 'slide',
  }
);
export default createAppContainer(
  createSwitchNavigator(
    {
      [AppRoutes.AuthCheck]: {
        screen: AuthCheckerScreen,
      },
      [AppRoutes.Slider]: {
        screen: AuthStack,
      },
      [AppRoutes.Landing]: {
        screen: DrawerScreens,
      },
    },
    {
      initialRouteName: AppRoutes.AuthCheck,
    }
  )
);
