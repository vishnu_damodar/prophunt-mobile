export enum AppRoutes {
  AuthCheck = "AuthCheck",
  Slider = "SlidingScreen",
  Landing = "LandingScreen",
  LoginStack = "LoginStack",
  Login = "Login",
  Register = "Register",
  Search = "Search",
  PropertyListing = "PropertyListing",
  Dashboard = "Dashboard"
}
