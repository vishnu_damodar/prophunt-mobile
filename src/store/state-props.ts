import { SettingsState } from "./settings/settings-models";
import { AuthDetails } from "./auth/auth-models";
import { SearchDetails } from "./search/search-models";
import { PropertyDetails } from "./property/property-models";
import { ShortlistDetails } from "./shortlist/shortlist-models";
import { DashboardDetails } from "./dashboard/dashboard-models";

export interface IAppState {
  settings: SettingsState;
  auth: AuthDetails;
  search: SearchDetails;
  property:PropertyDetails;
  shortlist:ShortlistDetails;
  dashboard: DashboardDetails;
}
