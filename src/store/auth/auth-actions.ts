import { LoginResult } from "@app/services/api/models";
import {
  IActionWithPayload,
  IAction,
  createActionWithPayload,
  createAction,
} from "@app/utils/action-helper";
import {
  AppleLoginModel,
  FaceBookLoginModel,
  GoogleLoginModel,
  ILoginModel,
  IRegisterModel,
} from "./auth-models";
export const AUTH_LOADER_ACTION_TYPE = "AUTH-LOADER-ACTION";
export const LOGIN_ACTION_TYPE = "AUTH-LOGIN";
export const LOGIN_SUCCESS_ACTION_TYPE = "AUTH-LOGIN-SUCCESS";
export const LOGOUT_ACTION_TYPE = "AUTH-LOGOUT";
export const GOOGLE_LOGIN_ACTION_TYPE = "GOOGLE-LOGIN-ACTION";
export const FACEBOOK_LOGIN_ACTION_TYPE = "FACEBOOK-LOGIN-ACTION";
export const APPLE_LOGIN_ACTION_TYPE = "APPLE-LOGIN-ACTION";
export const SIGN_UP_ACTION_TYPE = "SIGN-UP-ACTION";

type LoaderAction = IActionWithPayload<boolean, typeof AUTH_LOADER_ACTION_TYPE>;

type LoginAction = IActionWithPayload<ILoginModel, typeof LOGIN_ACTION_TYPE>;

type LoginSuccessAction = IActionWithPayload<
  LoginResult,
  typeof LOGIN_SUCCESS_ACTION_TYPE
>;
type GoogleLoginAction = IActionWithPayload<
  GoogleLoginModel,
  typeof GOOGLE_LOGIN_ACTION_TYPE
>;

type FaceBookLoginAction = IActionWithPayload<
  FaceBookLoginModel,
  typeof FACEBOOK_LOGIN_ACTION_TYPE
>;

type AppleLoginAction = IActionWithPayload<
  AppleLoginModel,
  typeof APPLE_LOGIN_ACTION_TYPE
>;

type SignupAction = IActionWithPayload<
  IRegisterModel,
  typeof SIGN_UP_ACTION_TYPE
>;

type LogoutAction = IActionWithPayload<string, typeof LOGOUT_ACTION_TYPE>;

export type AuthActions =
  | LoaderAction
  | LoginAction
  | GoogleLoginAction
  | FaceBookLoginAction
  | AppleLoginAction
  | LoginSuccessAction
  | SignupAction
  | LogoutAction;

export const setAuthLoader = (status: boolean): LoaderAction => {
  return createActionWithPayload(AUTH_LOADER_ACTION_TYPE, status);
};
export const onPressLoginAction = (userDetails: ILoginModel): LoginAction => {
  return createActionWithPayload(LOGIN_ACTION_TYPE, userDetails);
};

export const onLogoutAction = (screen: string): LogoutAction => {
  return createActionWithPayload(LOGOUT_ACTION_TYPE, screen);
};

export const onPressGoogleLoginAction = (
  details: GoogleLoginModel
): GoogleLoginAction => {
  return createActionWithPayload(GOOGLE_LOGIN_ACTION_TYPE, details);
};

export const onPressFaceBookLoginAction = (
  details: FaceBookLoginModel
): FaceBookLoginAction => {
  return createActionWithPayload(FACEBOOK_LOGIN_ACTION_TYPE, details);
};

export const onPressAppleLoginAction = (
  details: AppleLoginModel
): AppleLoginAction => {
  return createActionWithPayload(APPLE_LOGIN_ACTION_TYPE, details);
};

export const onLoginSuccessAction = (
  userDetails: LoginResult
): LoginSuccessAction => {
  return createActionWithPayload(LOGIN_SUCCESS_ACTION_TYPE, userDetails);
};

export const signupAction = (details: IRegisterModel): SignupAction => {
  return createActionWithPayload(SIGN_UP_ACTION_TYPE, details);
};
