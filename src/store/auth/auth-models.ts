import { LoginResult } from "@app/services/api/models";

export class AuthDetails {
  token?: string;
  isLoading: boolean = false;
  userDetails: LoginResult = new LoginResult();
}

export interface ILoginModel {
  username: string;
  password: string;
}

export interface IRegisterModel {
  userTypeID: string;
  userStatus: string;
  individualUserType: string;
  userLastname: string;
  userFirstName: string;
  userPhone: string;
  userEmail: string;
  userPassword: string;
  cPassword?: string;
  countryCode: string;
  countryId: string;
  userNewsletterSubscribeStatus: "Yes" | "No";
  stateSelectedType: string;
  userCompanyName: string;
  googleStateName: string;
  googleCityName: string;
  googleLocality: string;
  propertyLatitude: string;
  propertyLongitude: string;
  userAddress1: string;
}

export class GoogleLoginModel {
  photo: string = "";
  email: string = "";
  familyName: string = "";
  givenName: string = "";
  name: string = "";
  id: string = "";
  cartId?: string;
  referredCode: string = "";
  deviceType?: string = "";
  deviceUniqueId?: string = "";
  deviceToken?: string = "";
  forceLogout?: string = "";
}

export class FaceBookLoginModel {
  photo: string = "";
  email: string = "";
  firstName: string = "";
  lastName: string = "";
  id: string = "";
  cartId?: string;
  referredCode: string = "";
  deviceType?: string = "";
  deviceUniqueId?: string = "";
  deviceToken?: string = "";
  forceLogout?: string = "";
}

export class AppleLoginModel {
  email: string = "";
  firstName: string = "";
  lastName: string = "";
  user: string = "";
  cartId?: string;
  referredCode: string = "";
  deviceType?: string = "";
  deviceUniqueId?: string = "";
  deviceToken?: string = "";
  forceLogout?: string = "";
}
