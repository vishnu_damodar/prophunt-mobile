import api from "@app/services/api";
import {
  AuthActions,
  AUTH_LOADER_ACTION_TYPE,
  LOGIN_ACTION_TYPE,
  LOGIN_SUCCESS_ACTION_TYPE,
  LOGOUT_ACTION_TYPE,
  SIGN_UP_ACTION_TYPE,
} from "./auth-actions";
import { AuthDetails } from "./auth-models";

export const AuthReducer = (
  auth: AuthDetails = new AuthDetails(),
  action: AuthActions
): AuthDetails => {
  switch (action.type) {
    case AUTH_LOADER_ACTION_TYPE:
      return {
        ...auth,
        isLoading: action.payload,
      };
    // case SIGN_UP_ACTION_TYPE:
    //   return {
    //     ...auth,
    //     isLoading: true,
    //   };
    case LOGIN_SUCCESS_ACTION_TYPE:
      api.setToken(action.payload.token);
      return {
        ...auth,
        token: action.payload.token,
        userDetails: action.payload,
        isLoading: false,
      };

    default:
      return auth;
  }
};
