import api from "@app/services/api";
import { call, delay, put, takeEvery, takeLatest } from "redux-saga/effects";
import { IActionWithPayload } from "@app/utils/action-helper";
import {
  LOGIN_ACTION_TYPE,
  onLoginSuccessAction,
  setAuthLoader,
  SIGN_UP_ACTION_TYPE,
} from "./auth-actions";
import { ILoginModel, IRegisterModel } from "./auth-models";
import { ApiResponse, LoginResult } from "@app/services/api/models";
import { AppRoutes } from "@app/route/route-keys";
import { NavigationService } from "@app/services/navigation-service";
import { ToasterReferences } from "@app/services/toast-service";

function* loginAsync(action: IActionWithPayload<ILoginModel>) {
  yield put(setAuthLoader(true));
  let response: ApiResponse<LoginResult> = yield call(
    api.auth.loginApi,
    action.payload
  );
  if (response.responseStatus == "Success") {
    ToasterReferences.showSucessMsg(
      response?.message ?? "Welcome"
    );
    yield put(onLoginSuccessAction(response.data!));
    NavigationService.pushReplace(AppRoutes.Landing);
  } else {
    ToasterReferences.showErrorMsg(response?.message ?? "Unable to register");
  }
  yield put(setAuthLoader(false));
}

export function* loginSaga() {
  yield takeLatest(LOGIN_ACTION_TYPE, loginAsync);
}

function* signupAsync(action: IActionWithPayload<IRegisterModel>) {
  yield put(setAuthLoader(true));
  let response: ApiResponse<LoginResult> = yield call(
    api.auth.signupApi,
    action.payload
  );
  if (response.responseStatus == "Success") {
    ToasterReferences.showSucessMsg(
      response?.message ?? "Registration Success"
    );
    yield put(onLoginSuccessAction(response.data!));
    NavigationService.pushReplace(AppRoutes.Landing);
  } else {
    ToasterReferences.showErrorMsg(response?.message ?? "Unable to register");
  }
  yield put(setAuthLoader(false));
}

export function* signupSaga() {
  yield takeLatest(SIGN_UP_ACTION_TYPE, signupAsync);
}
