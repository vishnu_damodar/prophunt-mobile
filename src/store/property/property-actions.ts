import {
  createActionWithPayload,
  IActionWithPayload,
} from "@app/utils/action-helper";
import {} from "./property-models";

export const SET_PROPERTY_LIST_ACTION_TYPE = "SET-PROPERTY-LIST-ACTION";

type SetPropertyListAction = IActionWithPayload<
  any,
  typeof SET_PROPERTY_LIST_ACTION_TYPE
>;

export type PropertyActions = SetPropertyListAction;

export const SetPropertyListAction = (
  propertyList: any[]
): SetPropertyListAction => {
  return createActionWithPayload(SET_PROPERTY_LIST_ACTION_TYPE, propertyList);
};
