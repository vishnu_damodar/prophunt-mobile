import {
  PropertyActions,
  SET_PROPERTY_LIST_ACTION_TYPE,
} from "./property-actions";
import { PropertyDetails } from "./property-models";

export const PropertyReducer = (
  propertyState: PropertyDetails = new PropertyDetails(),
  action: PropertyActions
): PropertyDetails => {
  switch (action.type) {
    case SET_PROPERTY_LIST_ACTION_TYPE:
      return {
        ...propertyState,
        propertyList: action.payload,
      };

    default:
      return propertyState;
  }
};
