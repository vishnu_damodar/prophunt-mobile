import { AppConfig } from "@app/config/app-config";

export class SearchDetails {
  propertySearchParams: PropertySearchParamModel = new PropertySearchParamModel();
}

export class PropertySearchParamModel {
  userID?: string = "";
  cityID: string = "";
  searchPropertyPurpose: "Sell" | "Rent" = "Sell";
  page?: number = 0;
  itemType?: "Property" = "Property";
  sortBy?: string = "date/orderby/desc";
  searchMinPrice?: string = "";
  searchMaxPrice?: string = "";
  searchPropertyTypeID?: string = "";
  searchCountryID: string = AppConfig.countryId;
  searchAttributesStr?: string = "";
  searchRegion?: string = "";
  possessionStatus?: string = "";
  searchRadius?: string = "";
  Coordinates?: string = "";
  center?: string = "";
  radius?: string = "";
  searchUserTypeID?: number = 1;
}
