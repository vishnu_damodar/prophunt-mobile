import {
  createActionWithPayload,
  IActionWithPayload,
} from "@app/utils/action-helper";
import { PropertySearchParamModel } from "./search-models";

export const SET_SEARCH_PROPERTY_PARAMS_ACTION_TYPE = "SET-SEARCH-PROPERTY-PARAMS-ACTION";

type SetPropertySearchParamAction = IActionWithPayload<
  PropertySearchParamModel,
  typeof SET_SEARCH_PROPERTY_PARAMS_ACTION_TYPE
>;

export type SearchActions = SetPropertySearchParamAction;



export const SetPropertySearchParamsAction = (
  params: PropertySearchParamModel
): SetPropertySearchParamAction => {
  return createActionWithPayload(SET_SEARCH_PROPERTY_PARAMS_ACTION_TYPE, params);
};
