import {
  SearchActions,
  SET_SEARCH_PROPERTY_PARAMS_ACTION_TYPE,
} from "./search-actions";
import { SearchDetails } from "./search-models";

export const SearchReducer = (
  searchState: SearchDetails = new SearchDetails(),
  action: SearchActions
): SearchDetails => {
  switch (action.type) {
    case SET_SEARCH_PROPERTY_PARAMS_ACTION_TYPE:
      return {
        ...searchState,
        propertySearchParams:action.payload
      };

    default:
      return searchState;
  }
};
