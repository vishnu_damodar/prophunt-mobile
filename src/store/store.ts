import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import rootReducer from "./combined-reducer";
import initialize from "./initializeStore";
import { rootSaga } from "./combined-saga";

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    whitelist: ["settings", "auth"],
    blacklist: []
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const persistor = persistStore(store, null, () => {
    initialize(store);
});
export { store, persistor };
