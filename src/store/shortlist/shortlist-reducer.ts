import { ShortlistDetails } from "./shortlist-models";
import { SHORTLISTED_COUNT_SUCCESS_ACTION_TYPE, ShortlistActions, SHORTLISTED_AGENT_SUCCESS_ACTION_TYPE, SHORTLISTED_DEVELOPER_SUCCESS_ACTION_TYPE, SHORTLISTED_EXPERT_SUCCESS_ACTION_TYPE, SHORTLISTED_PROJECT_SUCCESS_ACTION_TYPE, SHORTLISTED_PROPERTY_SUCCESS_ACTION_TYPE } from "./shortlist-actions";


export const ShortlistReducer = (
  propertyState: ShortlistDetails = new ShortlistDetails(),
  action: ShortlistActions
): ShortlistDetails => {
  switch (action.type) {
    case SHORTLISTED_COUNT_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        countResult: action.payload
      };
    case SHORTLISTED_PROPERTY_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        propertyResult: action.payload
      };
    case SHORTLISTED_PROJECT_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        projectResult: action.payload
      };
    case SHORTLISTED_AGENT_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        agentResult: action.payload
      };
    case SHORTLISTED_DEVELOPER_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        developerResult: action.payload
      };
    case SHORTLISTED_EXPERT_SUCCESS_ACTION_TYPE:
      return {
        ...propertyState,
        expertResult: action.payload
      };

    default:
      return propertyState;
  }
};
