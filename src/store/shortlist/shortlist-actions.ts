import { ApiResponse } from "@app/services/api/models";
import { ShortlistedAgentResult, ShortlistedCountResult, ShortlistedDeveloperResult, ShortlistedExpertResult, ShortlistedProjectResult, ShortlistedPropertyResult } from "@app/services/api/models/shortlist";
import {
  createActionWithPayload,
  IActionWithPayload,
} from "@app/utils/action-helper";
import { UnFavoriteParamModel } from "./shortlist-models";

export const GET_SHORTLISTED_COUNT_ACTION_TYPE = "GET-SHORTLISTED-COUNT-ACTION";
export const SHORTLISTED_COUNT_SUCCESS_ACTION_TYPE = "SHORTLISTED-SUCCESS-COUNT-ACTION";
export const SET_SHORTLISTED_PROPERTY_LIST_ACTION_TYPE = "SET-SHORTLISTED-PROPERTY-LIST-ACTION";
export const SHORTLISTED_PROPERTY_SUCCESS_ACTION_TYPE = "SHORTLISTED-PROPERTY-SUCCESS-ACTION";
export const SET_SHORTLISTED_PROJECT_LIST_ACTION_TYPE = "SET-SHORTLISTED-PROJECT-LIST-ACTION";
export const SHORTLISTED_PROJECT_SUCCESS_ACTION_TYPE = "SHORTLISTED-PROJECT-SUCCESS-ACTION";
export const SET_SHORTLISTED_AGENT_LIST_ACTION_TYPE = "SET-SHORTLISTED-AGENT-LIST-ACTION";
export const SHORTLISTED_AGENT_SUCCESS_ACTION_TYPE = "SHORTLISTED-AGENT-SUCCESS-ACTION";
export const SET_SHORTLISTED_DEVELOPER_LIST_ACTION_TYPE = "SET-SHORTLISTED-DEVELOPER-LIST-ACTION";
export const SHORTLISTED_DEVELOPER_SUCCESS_ACTION_TYPE = "SHORTLISTED-DEVELOPER-SUCCESS-ACTION";
export const SET_SHORTLISTED_EXPERT_LIST_ACTION_TYPE = "SET-SHORTLISTED-EXPERT-LIST-ACTION";
export const SHORTLISTED_EXPERT_SUCCESS_ACTION_TYPE = "SHORTLISTED-EXPERT-SUCCESS-ACTION";

export const SET_UNFAV_PROPERTY_ACTION_TYPE = "SET_UNFAV_PROPERTY_ACTION";
export const UNFAV_PROPERTY_SUCCESS_ACTION_TYPE = "UNFAV_PROPERTY-SUCCESS-ACTION";
export const SET_UNFAV_PROJECT_ACTION_TYPE = "SET_UNFAV_PROJECT_ACTION";
export const UNFAV_PROJECT_SUCCESS_ACTION_TYPE = "UNFAV_PROJECT-SUCCESS-ACTION";
export const SET_UNFAV_USER_ACTION_TYPE = "SET_UNFAV_USER_ACTION";
export const UNFAV_USER_SUCCESS_ACTION_TYPE = "UNFAV_USER-SUCCESS-ACTION";

type GetShortlistedCountAction = IActionWithPayload<
  string,
  typeof GET_SHORTLISTED_COUNT_ACTION_TYPE
>;

type ShortlistedCountSuccessAction = IActionWithPayload<
  ShortlistedCountResult
>;

type FetchShortlistedPropertyListAction = IActionWithPayload<
  string,
  typeof SET_SHORTLISTED_PROPERTY_LIST_ACTION_TYPE
>;

type ShortlistedPropertySuccessAction = IActionWithPayload<
  ShortlistedPropertyResult
>;

type FetchShortlistedProjectListAction = IActionWithPayload<
  string,
  typeof SET_SHORTLISTED_PROJECT_LIST_ACTION_TYPE
>;

type ShortlistedProjectSuccessAction = IActionWithPayload<
  ShortlistedProjectResult
>;

type FetchShortlistedAgentListAction = IActionWithPayload<
  string,
  typeof SET_SHORTLISTED_AGENT_LIST_ACTION_TYPE
>;

type ShortlistedAgentSuccessAction = IActionWithPayload<
  ShortlistedAgentResult
>;

type FetchShortlistedDeveloperListAction = IActionWithPayload<
  string,
  typeof SET_SHORTLISTED_DEVELOPER_LIST_ACTION_TYPE
>;

type ShortlistedDeveloperSuccessAction = IActionWithPayload<
  ShortlistedDeveloperResult
>;

type FetchShortlistedExpertListAction = IActionWithPayload<
  string,
  typeof SET_SHORTLISTED_EXPERT_LIST_ACTION_TYPE
>;

type ShortlistedExpertSuccessAction = IActionWithPayload<
  ShortlistedExpertResult
>;

type UnFavoritePropertyAction = IActionWithPayload<
  UnFavoriteParamModel,
  typeof SET_UNFAV_PROPERTY_ACTION_TYPE
>;

type UnFavoriteProjectAction = IActionWithPayload<
  UnFavoriteParamModel,
  typeof SET_UNFAV_PROJECT_ACTION_TYPE
>;

type UnFavoriteUserAction = IActionWithPayload<
  UnFavoriteParamModel,
  typeof SET_UNFAV_USER_ACTION_TYPE
>;

export type ShortlistActions =
  | GetShortlistedCountAction
  | FetchShortlistedPropertyListAction
  | ShortlistedPropertySuccessAction
  | FetchShortlistedProjectListAction
  | ShortlistedProjectSuccessAction
  | FetchShortlistedAgentListAction
  | ShortlistedAgentSuccessAction
  | FetchShortlistedDeveloperListAction
  | ShortlistedDeveloperSuccessAction
  | FetchShortlistedExpertListAction
  | ShortlistedExpertSuccessAction
  | UnFavoritePropertyAction
  | UnFavoriteProjectAction
  | UnFavoriteUserAction;

  export const GetShortlistedCountAction = (
    userId: string
  ): GetShortlistedCountAction => {
    return createActionWithPayload(GET_SHORTLISTED_COUNT_ACTION_TYPE, userId);
  };
  
  export const onShortlistedCountSuccessAction = (
    details: ShortlistedCountResult
  ): ShortlistedCountSuccessAction => {
    return createActionWithPayload(SHORTLISTED_COUNT_SUCCESS_ACTION_TYPE, details);
  };

export const SetShortlistedPropertyListAction = (
  userId: string
): FetchShortlistedPropertyListAction => {
  return createActionWithPayload(SET_SHORTLISTED_PROPERTY_LIST_ACTION_TYPE, userId);
};

export const onShortlistedPropertySuccessAction = (
  details: ShortlistedPropertyResult
): ShortlistedPropertySuccessAction => {
  return createActionWithPayload(SHORTLISTED_PROPERTY_SUCCESS_ACTION_TYPE, details);
};

export const SetShortlistedProjectListAction = (
  userId: string
): FetchShortlistedProjectListAction => {
  return createActionWithPayload(SET_SHORTLISTED_PROJECT_LIST_ACTION_TYPE, userId);
};

export const onShortlistedProjectSuccessAction = (
  details: ShortlistedProjectResult
): ShortlistedProjectSuccessAction => {
  return createActionWithPayload(SHORTLISTED_PROJECT_SUCCESS_ACTION_TYPE, details);
};

export const SetShortlistedAgentListAction = (
  userId: string
): FetchShortlistedAgentListAction => {
  return createActionWithPayload(SET_SHORTLISTED_AGENT_LIST_ACTION_TYPE, userId);
};

export const onShortlistedAgentSuccessAction = (
  details: ShortlistedAgentResult
): ShortlistedAgentSuccessAction => {
  return createActionWithPayload(SHORTLISTED_AGENT_SUCCESS_ACTION_TYPE, details);
};

export const SetShortlistedDeveloperListAction = (
  userId: string
): FetchShortlistedDeveloperListAction => {
  return createActionWithPayload(SET_SHORTLISTED_DEVELOPER_LIST_ACTION_TYPE, userId);
};

export const onShortlistedDeveloperSuccessAction = (
  details: ShortlistedDeveloperResult
): ShortlistedDeveloperSuccessAction => {
  return createActionWithPayload(SHORTLISTED_DEVELOPER_SUCCESS_ACTION_TYPE, details);
};

export const SetShortlistedExpertListAction = (
  userId: string
): FetchShortlistedExpertListAction => {
  return createActionWithPayload(SET_SHORTLISTED_EXPERT_LIST_ACTION_TYPE, userId);
};

export const onShortlistedExpertSuccessAction = (
  details: ShortlistedExpertResult
): ShortlistedExpertSuccessAction => {
  return createActionWithPayload(SHORTLISTED_EXPERT_SUCCESS_ACTION_TYPE, details);
};

export const UnFavoritePropertyAction = (
  params: UnFavoriteParamModel
): UnFavoritePropertyAction => {
  return createActionWithPayload(SET_UNFAV_PROPERTY_ACTION_TYPE, params);
};

export const UnFavoriteProjectAction = (
  params: UnFavoriteParamModel
): UnFavoritePropertyAction => {
  return createActionWithPayload(SET_UNFAV_PROPERTY_ACTION_TYPE, params);
};

export const UnFavoriteUserAction = (
  params: UnFavoriteParamModel
): UnFavoritePropertyAction => {
  return createActionWithPayload(SET_UNFAV_PROPERTY_ACTION_TYPE, params);
};

