import {ShortlistedAgentResult, ShortlistedCountResult, ShortlistedDeveloperResult, ShortlistedExpertResult, ShortlistedProjectResult, ShortlistedPropertyResult } from "@app/services/api/models/shortlist";

export class ShortlistDetails {
  countResult: ShortlistedCountResult = new ShortlistedCountResult();
  propertyResult: ShortlistedPropertyResult = new ShortlistedPropertyResult();
  projectResult: ShortlistedProjectResult = new ShortlistedProjectResult();
  agentResult: ShortlistedAgentResult = new ShortlistedAgentResult();
  developerResult: ShortlistedDeveloperResult = new ShortlistedDeveloperResult();
  expertResult: ShortlistedExpertResult = new ShortlistedExpertResult();
}

export class UnFavoriteParamModel {
  userID?: string = "";
  id: string = "";
  option?: number;
}