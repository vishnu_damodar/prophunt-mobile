import api from "@app/services/api";
import { Store } from "redux";
import { onLogoutAction } from "./auth/auth-actions";
import { onLoadCityListAction, onLoadCommonListingAction, onLoadLanguageListingAction } from "./settings/settings-actions";
import { IAppState } from "./state-props";
import useUnauthorizedApiResponseInterceptor from "@app/services/api/interceptors";
import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";

const initialize = (store: Store<IAppState>) => {
    const state = store.getState();
    useUnauthorizedApiResponseInterceptor(() => {
        // store.dispatch(onLogoutAction());
        // api.setToken(undefined);
       
        // NavigationService.navigate(AppRoutes.Home);
    });
    api.setToken(state.auth.token);
    api.setLanguageId(`${state.settings.locale.languageId}` ?? "1");
    if (state.auth.token) {
        api.setToken(state.auth.token);
      
        api.setLanguageId(`${state.settings.locale.languageId}` ?? "1");
        // store.dispatch(fetchProfileAction());
    }
    store.dispatch(onLoadLanguageListingAction());
    store.dispatch(onLoadCommonListingAction());
    store.dispatch(onLoadCityListAction());
};

export default initialize;
