import { combineReducers } from "redux";
import { IAppState } from "./state-props";
import { SettingsReducer } from "./settings/settings-reducer";
import { AuthReducer } from "./auth/auth-reducer";
import api from "@app/services/api";
import { IActionWithPayload } from "@app/utils/action-helper";
import { LOGOUT_ACTION_TYPE } from "./auth/auth-actions";
import { AuthDetails } from "./auth/auth-models";
import { NavigationService } from "@app/services/navigation-service";
import { SearchReducer } from "./search/search-reducer";
import { SearchDetails } from "./search/search-models";
import { PropertyReducer } from "./property/property-reducer";
import { PropertyDetails } from "./property/property-models";
import { ShortlistReducer } from "./shortlist/shortlist-reducer";
import { ShortlistDetails } from "./shortlist/shortlist-models";
import { DashboardReducer } from "./dashboard/dashboard-reducer";
import { DashboardDetails } from "./dashboard/dashboard-models";
const reducer = combineReducers<IAppState>({
  settings: SettingsReducer,
  auth: AuthReducer,
  search: SearchReducer,
  property: PropertyReducer,
  shortlist: ShortlistReducer,
  dashboard: DashboardReducer,
});

export default (
  appState: IAppState,
  action: IActionWithPayload<any>
): IAppState => {
  if (action.type === LOGOUT_ACTION_TYPE) {
    api.setToken("");
    api.setWebsiteId("");
    api.setSubsiteId("");
    NavigationService.navigate(action.payload);
    return {
      auth: new AuthDetails(),
      settings: appState.settings, // Don't clear settings when logging out
      search: new SearchDetails(),
      property: new PropertyDetails(),
      shortlist: new ShortlistDetails(),
      dashboard: new DashboardDetails(),
    };
  }
  return reducer(appState, action);
};
