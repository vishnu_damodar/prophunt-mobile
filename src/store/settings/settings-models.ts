import {
  BuyRentPrice,
  CityList,
  CommonListingResult,
  DefaultCity,
  LanguageList,
} from "@app/services/api/models";

export interface ILocale {
  languageId: number;
  languageName: string;
  code: string;
  isRtl: boolean;
  currencySymbol:string;
  currencyCode:string;
}
export class EnglishLocale implements ILocale {
  languageId: number = 1;
  languageName: string = "English";
  code: string = "en";
  currencySymbol:string ="$";
  currencyCode:string = "USD"
  isRtl: boolean = false;
}

export class FrenchLocale implements ILocale {
  languageId: number = 8;
  languageName: string = "French";
  code: string = "fr";
  currencySymbol:string ="€";
  currencyCode:string = "EUR";
  isRtl: boolean = false;
}

export class SettingsState {
  loading: boolean = false;
  locale: ILocale = new EnglishLocale();
  LanguageList: LanguageList[] = [];
  defaultCity: DefaultCity = new DefaultCity();
  currency: string = "GH₵";
  unit:string = "㎡"
  commonListings: CommonListingResult = new CommonListingResult();
  cityList: CityList[] = [];
}
