import api from "@app/services/api";
import { call, delay, put, takeEvery, takeLatest } from "redux-saga/effects";
import { IActionWithPayload } from "@app/utils/action-helper";
import {
  SetLocaleActionType,
  ChangeLanguageActionType,
  SetLayoutActionType,
  LoadLanguageListActionType,
  SetLanguageListActionType,
  LoadCommonListingActionType,
  SetCommonListingActionType,
  SetCityListActionType,
  GetCityListActionType,
} from "./settings-actions";
import { ILocale, EnglishLocale, FrenchLocale } from "./settings-models";
import {
  ApiResponse,
  CityList,
  CityListResponse,
  CommonListingResult,
  LanguageResult,
} from "@app/services/api/models";
import Img from "@app/assets/images";

function* fetchLanguagesAsync() {
  const response: ApiResponse<LanguageResult> = yield call(
    api.settings.getLanguageList
  );
  if (response.responseStatus == "Success") {
    yield put({
      type: SetLanguageListActionType,
      payload: {
        ...response.data,
      },
    });
  } else {
    // yield put({
    //   type: SetLanguageListActionType,
    //   payload: { ...response.data },
    // });
  }
}

export function* onLoadLanguageList() {
  yield takeLatest(LoadLanguageListActionType, fetchLanguagesAsync);
}

function* fetchCommonListingAsync() {
  const response: ApiResponse<CommonListingResult> = yield call(
    api.settings.getCommonListing
  );
  if (response.responseStatus == "Success") {
    yield put({
      type: SetCommonListingActionType,
      payload: {
        ...response.data,
      },
    });
  } else {
    // yield put({
    //   type: SetLanguageListActionType,
    //   payload: { ...response.data },
    // });
  }
}

export function* onLoadCommonListings() {
  yield takeLatest(LoadCommonListingActionType, fetchCommonListingAsync);
}

function* setLocale(data: IActionWithPayload<string>) {
  let locale: ILocale;
  switch (data.payload) {
    case "1":
      locale = new EnglishLocale();
      break;
    case "8":
      locale = new FrenchLocale();
      break;
    default:
      locale = new EnglishLocale();
  }

  yield put({ type: SetLocaleActionType, payload: locale });
  yield delay(100);
  yield put({ type: SetLayoutActionType, payload: locale });
}

export function* changeLanguage() {
  yield takeEvery(ChangeLanguageActionType, setLocale);
}

function* fetchCityListAsync() {
  const response: ApiResponse<CityListResponse> = yield call(
    api.settings.getCityList
  );
  if (response.responseStatus == "Success") {
    yield put({
      type: SetCityListActionType,
      payload: response.data?.cityList?? [],
    });
  } else {
    // yield put({
    //   type: SetLanguageListActionType,
    //   payload: { ...response.data },
    // });
  }
}

export function* onLoadCityList() {
  yield takeLatest(GetCityListActionType, fetchCityListAsync);
}
