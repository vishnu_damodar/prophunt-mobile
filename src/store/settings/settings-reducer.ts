import {
  SettingsActions,
  LoadCommonListingActionType,
  SetLayoutActionType,
  SetLocaleActionType,
  SetLanguageListActionType,
  SetCommonListingActionType,
  SetCityListActionType,
} from "./settings-actions";
import { SettingsState } from "./settings-models";
import { I18nManager } from "react-native";
import RNRestart from "react-native-restart";
import api from "@app/services/api";
import Img from "@app/assets/images";

export const SettingsReducer = (
  state: SettingsState = new SettingsState(),
  action: SettingsActions
): SettingsState => {
  switch (action.type) {
    case SetLanguageListActionType:
      return {
        ...state,
        LanguageList: action.payload.languageList,
      };
    case LoadCommonListingActionType:
      return {
        ...state,
        loading: true,
      };

    case SetCommonListingActionType:
      action.payload.buyMaxPrice.unshift({
        price: 0,
        priceValue: `Max Price`,
      });
      action.payload.buyMinPrice.unshift({
        price: 0,
        priceValue: `Min Price`,
      });
      action.payload.rentMaxPrice.unshift({
        price: 0,
        priceValue: `Max Price`,
      });
      action.payload.rentMinPrice.unshift({
        price: 0,
        priceValue: `Min Price`,
      });

      action.payload.propertyType.unshift({
        propertyTypeId: "-1",
        subCategory: [],
        propertyTypeName: "Property Type",
      });

      return {
        ...state,
        commonListings: action.payload,
        loading: false,
      };

    case SetLocaleActionType:
      return {
        ...state,
        locale: action.payload,
      };

    case SetCityListActionType:
      return {
        ...state,
        cityList: action.payload,
      };
    case SetLayoutActionType: {
      I18nManager.forceRTL(action.payload.isRtl);
      api.setLanguageId(`${action.payload.languageId}` ?? "1");
      RNRestart.Restart();
    }

    default:
      return (
        state || {
          locale: { isRtl: false, code: "en", languageId: 1 },
        }
      );
  }
};
