import { CityList, CommonListingResult, LanguageResult } from "@app/services/api/models";
import {
  IAction,
  IActionWithPayload,
  createActionWithPayload,
  createAction,
} from "@app/utils/action-helper";
import { ILocale } from "./settings-models";
export const LoadLanguageListActionType = "Load-Language-List-Action";
export const SetLanguageListActionType = "Set-Language-List-Action";
export const LoadCommonListingActionType = "Load-Common-Listing-Action";
export const SetCommonListingActionType = "Set-Common-Listing-Action";
export const ChangeLanguageActionType = "Change-Language-Action";
export const SetLocaleActionType = "Set-Locale-Action";
export const SetLayoutActionType = "Set-layout-Action";
export const GetCityListActionType = "Get-City-List";
export const SetCityListActionType = "Set-City-List";

type LoadLanguageListingAction = IAction<typeof LoadLanguageListActionType>;
type SetLanguageListAction = IActionWithPayload<
  LanguageResult,
  typeof SetLanguageListActionType
>;

type LoadCommonListingAction = IActionWithPayload<
  null,
  typeof LoadCommonListingActionType
>;

type SetCommonListingAction = IActionWithPayload<
  CommonListingResult,
  typeof SetCommonListingActionType
>;
type ChangeLanguageAction = IActionWithPayload<
  string,
  typeof ChangeLanguageActionType
>;
type SetLocaleAction = IActionWithPayload<ILocale, typeof SetLocaleActionType>;
type SetLayoutAction = IActionWithPayload<ILocale, typeof SetLayoutActionType>;

type GetCityListAction = IActionWithPayload<null, typeof GetCityListActionType>;

type SetCityListAction = IActionWithPayload<CityList[], typeof SetCityListActionType>;


export type SettingsActions =
  | LoadLanguageListingAction
  | SetLanguageListAction
  | LoadCommonListingAction
  | SetCommonListingAction
  | ChangeLanguageAction
  | SetLocaleAction
  | SetLayoutAction
  | GetCityListAction
  | SetCityListAction;

export const onLoadLanguageListingAction = (): LoadLanguageListingAction => {
  return createAction(LoadLanguageListActionType);
};

export const onLoadCommonListingAction = (): LoadCommonListingAction => {
  return createActionWithPayload(LoadCommonListingActionType, null);
};

export const changeLanguage = (languageCode: string): ChangeLanguageAction => {
  return createActionWithPayload(ChangeLanguageActionType, languageCode);
};

export const onLoadCityListAction = (): GetCityListAction => {
  return createActionWithPayload(GetCityListActionType, null);
};
