import { EnquiriesResult, MyPropertiesResult, ProfileResult, SavedSearchResult } from "@app/services/api/models/dashboard";

export class DashboardDetails {
  savedSearchResult: SavedSearchResult = new SavedSearchResult();
  profileResult: ProfileResult = new ProfileResult();
  myPropertiesResult: MyPropertiesResult = new MyPropertiesResult();
  enquiriesResult: EnquiriesResult = new EnquiriesResult();
}

export class ListParamModel {
  userID?: string = "";
  page?: number = 0;
}

export class PauseResumeDeleteParamModel {
  userID?: string = "";
  id?: string = "";
  status?: string = "";
}

export class ChangePasswordParamModel {
  userID?: string = "";
  userPassword?: string = "";
  userNewPassword?: string = "";
  userConfirmPassword?: string = "";
}

export class SaveProfileParamModel {
  userID?: string = "";
  userFirstName?: string = "";
  userLastName?: string = "";
  userGender?: string = "";
  userPhone?: string = "";
  googleCountryName?: string = "";
  googleStateName?: string = "";
  googleCityName?: string = "";
  googleLocality?: string = "";
  countryCode?: string = "";
  userAddress1?: string = "";
  userAddress2?: string = "";
  userEmail?: string = "";
  userZip?: string = "";
  userProfilePicture?: string = "";
}