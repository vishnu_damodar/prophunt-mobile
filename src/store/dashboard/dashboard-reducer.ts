import { DELETE_PROPERTY_SUCCESS_ACTION_TYPE, ENQUIRIES_SUCCESS_ACTION_TYPE, MY_PROPERTIES_SUCCESS_ACTION_TYPE, PROFILE_DATA_SUCCESS_ACTION_TYPE, SavedSearchActions, SAVED_SEARCH_LIST_SUCCESS_ACTION_TYPE } from "./dashboard-actions";
import { DashboardDetails } from "./dashboard-models";

export const DashboardReducer = (
  state: DashboardDetails = new DashboardDetails(),
  action: SavedSearchActions
): DashboardDetails => {
  switch (action.type) {
    case SAVED_SEARCH_LIST_SUCCESS_ACTION_TYPE:
      return {
        ...state,
        savedSearchResult: action.payload
      };

    case PROFILE_DATA_SUCCESS_ACTION_TYPE:
      return {
        ...state,
        profileResult: action.payload
      };
    case MY_PROPERTIES_SUCCESS_ACTION_TYPE:
      return {
        ...state,
        myPropertiesResult: action.payload
      };
    case ENQUIRIES_SUCCESS_ACTION_TYPE:
      return {
        ...state,
        enquiriesResult: action.payload
      };
    case DELETE_PROPERTY_SUCCESS_ACTION_TYPE:
      let listData = [...state.myPropertiesResult.myProperties];
      const result = listData.filter(item => item.propertyID != action.payload)
      return {
        ...state,
        myPropertiesResult: {
          totalCount: state.myPropertiesResult.totalCount,
          imgPath: state.myPropertiesResult.imgPath,
          myProperties: result
        }
      };
    default:
      return state;
  }
};
