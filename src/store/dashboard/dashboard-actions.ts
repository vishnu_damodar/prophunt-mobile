import { EnquiriesResult, MyPropertiesResult, ProfileResult, SavedSearchResult } from "@app/services/api/models/dashboard";
import {
  createActionWithPayload,
  IActionWithPayload,
} from "@app/utils/action-helper";

export const GET_SAVED_SEARCH_LIST_ACTION_TYPE = "GET-SAVED-SEARCH-LIST-ACTION";
export const SAVED_SEARCH_LIST_SUCCESS_ACTION_TYPE = "SAVED-SEARCH-LIST-SUCCESS-ACTION";
export const SET_UNFAV_PROPERTY_ACTION_TYPE = "SET_UNFAV_PROPERTY_ACTION";
export const UNFAV_PROPERTY_SUCCESS_ACTION_TYPE = "UNFAV_PROPERTY-SUCCESS-ACTION";

export const PROFILE_DATA_SUCCESS_ACTION_TYPE = "PROFILE-DATA-SUCCESS-ACTION";
export const MY_PROPERTIES_SUCCESS_ACTION_TYPE = "MY-PROPERTIES-SUCCESS-ACTION";
export const ENQUIRIES_SUCCESS_ACTION_TYPE = "ENQUIRIES-SUCCESS-ACTION";
export const DELETE_PROPERTY_SUCCESS_ACTION_TYPE = "DELETE-PROPERTY-SUCCESS-ACTION";

type GetSavedSearchListAction = IActionWithPayload<
  string,
  typeof GET_SAVED_SEARCH_LIST_ACTION_TYPE
>;

type SavedSearchListSuccessAction = IActionWithPayload<
  SavedSearchResult
>;

type ProfileDataSuccessAction = IActionWithPayload<
  ProfileResult
>;

type MyPropertiesSuccessAction = IActionWithPayload<
  MyPropertiesResult
>;

type EnquiriesSuccessAction = IActionWithPayload<
  EnquiriesResult
>;

type DeletePropertySuccessAction = IActionWithPayload<
  string,
  typeof DELETE_PROPERTY_SUCCESS_ACTION_TYPE
>;

export type SavedSearchActions =
  | GetSavedSearchListAction
  | SavedSearchListSuccessAction
  | ProfileDataSuccessAction
  | MyPropertiesSuccessAction
  | EnquiriesSuccessAction
  | DeletePropertySuccessAction

export const GetSavedSearchListAction = (
  userId: string
): GetSavedSearchListAction => {
  return createActionWithPayload(GET_SAVED_SEARCH_LIST_ACTION_TYPE, userId);
};

export const onSavedSearchSuccessAction = (
  details: SavedSearchResult
): SavedSearchListSuccessAction => {
  return createActionWithPayload(SAVED_SEARCH_LIST_SUCCESS_ACTION_TYPE, details);
};

export const onProfileDataSuccessAction = (
  details: ProfileResult
): ProfileDataSuccessAction => {
  return createActionWithPayload(PROFILE_DATA_SUCCESS_ACTION_TYPE, details);
};

export const onMyPropertiesSuccessAction = (
  details: MyPropertiesResult
): MyPropertiesSuccessAction => {
  return createActionWithPayload(MY_PROPERTIES_SUCCESS_ACTION_TYPE, details);
};

export const onEnquiriesSuccessAction = (
  details: EnquiriesResult
): EnquiriesSuccessAction => {
  return createActionWithPayload(ENQUIRIES_SUCCESS_ACTION_TYPE, details);
};

export const DeletePropertySuccessAction = (
  id: string
): DeletePropertySuccessAction => {
  return createActionWithPayload(DELETE_PROPERTY_SUCCESS_ACTION_TYPE, id);
};

