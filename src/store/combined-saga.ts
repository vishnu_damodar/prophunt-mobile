import { all, fork } from "redux-saga/effects";
import * as settingsSaga from "@app/store/settings/settings-saga";
import * as authSaga from "@app/store/auth/auth-saga";

export function* rootSaga() {
    yield all([...(Object as any).values(settingsSaga)].map(fork));
    yield all([...(Object as any).values(authSaga)].map(fork));
}
