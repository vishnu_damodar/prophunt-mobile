import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize";

import { I18nManager } from "react-native";

const translationGetters: any = {
  // lazy requires (metro bundler does not support symlinks)
  ar: () => require("./ar.json"),
  en: () => require("./en.json"),
  fr: () => require("./fr.json"),
};

export const translate = memoize(
  (key: any, config?: any) => i18n.t(key, config),
  (key: any, config: any) => (config ? key + JSON.stringify(config) : key)
);

export const setI18nConfig = (languageSettings: any) => {
  // fallback if no available language fits
  const fallback = { languageTag: "en", isRTL: false };
  let { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;
  if (languageSettings != null) {
    languageTag = languageSettings.code;
    isRTL = languageSettings.isRtl;
  }
  // clear translation cache
  translate.cache.clear();

  I18nManager.forceRTL(isRTL);

  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;
};
