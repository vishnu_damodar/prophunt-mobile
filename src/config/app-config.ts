export const Keys = {
  servingUrl: "servingUrl",
};
export const baseUrl = "";

//dev
export const OORJIT_API_DOMAIN = "https://stg70.zeek.in/prophunt/mobilapp/";

//staging
//export const OORJIT_API_DOMAIN = "";

//production
// export const OORJIT_API_DOMAIN = "";

export const AppConfig = {
  url: `${baseUrl}mobileapp/`,
  webviewUrl: baseUrl,
  keys: Keys,
  version: "1.0",
  mapKey: "AIzaSyBdmg8dV3kdQuJ0tw4vldzwMYjec2GfPes",
  countryId: '82'
};

export const GoogleConfigs = {
  webClientId: "",
};
