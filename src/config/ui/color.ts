export const AppColors = {
  primaryThemeColor: '#73C6B6', //7CBEAB
  primaryThemeLightColor: '#BAE3DB',
  primaryThemeDarkColor: '#589387',
  primaryThemeExLightColor: '#EBF6F1',
  headerColor:"#32A893",

  headingTextColor: '#1B1A21',
  bodyTextColor:"#697684",
  fadeTextColor: '#98999c',
  grayTextColor: '#697684',
  dangerTextColor: '#dc3545',
  

  borderColor: '#DDE0E8',
  pageBackgroundColor: '#E9EEF2',
  statusBarColor: '#73C6B6',  
  warningColor: '#F39C12',
  redColor:"#FF1717",
  
  lightColor: '#ffffff',
  lightGray: "#F4F4F4",
  darkColor: '#2F2F2F',
  yellow: '#E4B716',
};
