import { Dimensions, PixelRatio, Platform, Image } from 'react-native';

export const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  'window',
);

export const wp = (percentage: number): number => {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
};

export const hp = (percentage: number): number => {
  const value = (percentage * viewportHeight) / 100;
  return Math.round(value);
};
const scale = viewportWidth / 411;

export function normalizeFont(size: number) {
  const newSize = size * scale;
  if (Platform.OS === 'ios') {
    return Math.round(newSize);
  } else {
    return Math.round(newSize);
  }
}

export function getResizedHeight(
  url: string,
  resizedWidth: number,
  callBack: any,
) {
  var height = 0;
  Image.getSize(
    url,
    (width, height) => {
      let aspectRatio = height / width;
      height = resizedWidth * aspectRatio;
      callBack(height);
    },
    (error) => {},
  );
  return height;
}
