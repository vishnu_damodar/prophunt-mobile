export const Fonts = {
  ExtraBold:'SourceSansPro-Black',
  Bold:'SourceSansPro-Bold',
  SemiBold:'SourceSansPro-SemiBold',
  // Heavy:'',
  // Medium:'',
  Regular:'SourceSansPro-Regular',
  // Thin:'Gilroy-Thin',
  Light:'SourceSansPro-Light',
  UltraLight:'SourceSansPro-ExtraLight'
};
