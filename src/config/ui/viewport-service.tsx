import { Dimensions } from "react-native";
export class ViewportService {
  private static _small = 10;
  private static _normal = 14;
  private static _subHeading: number = 18;
  private static _mainHeading: number = 20;
  private static _padding: number = 16;
  private static _smallPadding: number = 10;

  public static smallFont: number;
  public static normalFont: number = ViewportService._normal;
  public static subHeading: number = ViewportService._subHeading;
  public static mainHeading: number = ViewportService._mainHeading;
  public static commonPadding: number = ViewportService._padding;
  public static smallPadding: number = ViewportService._smallPadding;
  public static width: number = 411;
  public static hight = Math.round(Dimensions.get("window").height);

  public static initialize(viewportWidth: number) {
    const scale = viewportWidth / 411;
    this.smallFont = scale * this._small;
    this.normalFont = scale * this._normal;
    this.subHeading = scale * this._subHeading;
    this.mainHeading = scale * this._mainHeading;
    this.commonPadding = scale * this._padding;
    this.smallPadding = scale * this._smallPadding;
    this.width = viewportWidth;
  }
}
