import React, {useState, useEffect} from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  Platform,
  ActivityIndicator,
} from 'react-native';
import MapView, {Region} from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import {ToasterReferences} from '@app/services/toast-service';
import {AppConfig} from '@app/config/app-config';
import {Fonts} from '@app/config/ui/font';
import {AppColors} from '@app/config/ui/color';
import Img from '@app/assets/images';
import GetLocation from 'react-native-get-location';
import { translate } from '@app/config/locale/i18n';
export const MapComponent = (props: {
  onSelect: any;
  defaultValue: string;
  defaultLocation: UserLocation;
  hideMap: boolean;
  isInPickLocationScreen?: boolean;
}) => {
  let GoogleplacesAutocomplete: any = GooglePlacesAutocomplete;
  const [hideMap, setHideMap] = useState(props.hideMap);
  // const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLocation(props.defaultValue);
  }, [props.defaultValue]);

  useEffect(() => {
    //  setLoading(false);
  }, []);
  //let isInitialLoad: boolean = true;
  const [isInitialLoad, setInitialLoad] = useState(true);
  const [isAutoCompleteProcessing, setisAutoCompleteProcessing] = useState(
    false,
  );
  const [loading, setLoading] = useState(false);
  const fetchLocation = async () => {
    try {
      setLoading(true);
      var location = await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 4000,
      });
      let userLocation: UserLocation = new UserLocation();
      userLocation.lat = location.latitude;
      userLocation.lng = location.longitude;
      let currentRegion = {...region};
      currentRegion.latitude = location.latitude;
      currentRegion.longitude = location.longitude;
      revertseGeoCode(currentRegion);
    } catch (error) {
      const {code, message} = error;
      if (code == 'UNAVAILABLE') {
        ToasterReferences.showErrorMsg('Please turn on the Location');
      }
    } finally {
      setLoading(false);
    }
  };

  let placesRef: any;
  let location = props.defaultLocation;
  const [margin, setMargin] = useState(1);
  const [region, setRegion] = useState({
    longitude: 0,
    latitude: 0,
    latitudeDelta: 0.0024996521899200985,
    longitudeDelta: 0.004818923771381378,
  });
  const GeoCoder = Geocoder as any;
  useEffect(() => {
    GeoCoder.init(AppConfig.mapKey, {});
  }, []);

  const onSelect = (details: any, selectedRegion?: Region) => {
    setMargin(0);
    let placeDetails: PlaceDetails = new PlaceDetails();
    placeDetails.formattedAddress = details.formatted_address;
    let location = details?.geometry?.location;
    if (location) {
      placeDetails.lat = location.lat;
      placeDetails.lng = location.lng;
    }

    parseAddressComponent(details.address_components, placeDetails);
    let mapLocation: any = {...region};
    mapLocation.latitude = placeDetails.lat as any;
    mapLocation.longitude = placeDetails.lng as any;
    if (selectedRegion != null) {
      mapLocation.latitude = selectedRegion.latitude;
      mapLocation.longitude = selectedRegion.longitude;
      mapLocation.latitudeDelta = selectedRegion.latitudeDelta;
      mapLocation.longitudeDelta = selectedRegion.longitudeDelta;
    }

    setRegion(mapLocation);

    props.onSelect(placeDetails);
  };

  const parseAddressComponent = (
    address_components: any,
    placeDetails: PlaceDetails,
  ) => {
    address_components.forEach((p: any) => {
      if (p.types.indexOf('country') != -1) {
        placeDetails.country = {
          shortName: p.short_name,
          longName: p.long_name,
        };
      } else if (p.types.indexOf('postal_code') != -1) {
        placeDetails.postalCode = p.short_name;
      } else if (p.types.indexOf('administrative_area_level_1') != -1) {
        placeDetails.state = {
          shortName: p.short_name,
          longName: p.long_name,
        };
      } else if (p.types.indexOf('administrative_area_level_2') != -1) {
        placeDetails.city = {
          shortName: p.short_name,
          longName: p.long_name,
        };
      } else if (p.types.indexOf('route') != -1) {
        placeDetails.route = p.long_name;
      } else if (p.types.indexOf('locality') != -1) {
        placeDetails.locality = p.long_name;
      } else if (p.types.indexOf('street_number') != -1) {
        placeDetails.streetNumber = p.long_name;
      }
    });
  };

  const setLocation = (text: string) => {
    placesRef && placesRef.setAddressText(text);
  };

  const revertseGeoCode = (selectedRegion: Region) => {
    //   setLoading(true);
    GeoCoder.from(selectedRegion.latitude, selectedRegion.longitude)
      .then((json: any) => {
        // setLoading(false);
        let results = json.results;
        if (results.length > 0) {
          let formattedAddress = results[0].formatted_address;
          setLocation(formattedAddress);
          onSelect(results[0], selectedRegion);
        }
      })
      .catch((error: any) => {
        //  setLoading(false);
        //    console.warn(error);
      });
  };

  const revertseGeoCodeForPin = (
    formattedAddress: string,
    lat: any,
    lng: any,
  ) => {
    GeoCoder.from(lat, lng)
      .then((json: any) => {
        let results = json.results;
        if (results.length > 0) {
          if (formattedAddress != null && formattedAddress != '') {
            results[0].formatted_address = formattedAddress;
          }
          onSelect(results[0]);
        }
        setTimeout(() => {
          setisAutoCompleteProcessing(false);
        }, 3000);
      })
      .catch((error: any) => {
        setisAutoCompleteProcessing(false);
        //    console.warn(error);
      });
  };

  return (
    <View style={{}}>
      <View style={{}}>
        <GoogleplacesAutocomplete
          ref={(ref: any) => {
            placesRef = ref;
          }}
          placeholder={translate('dashboard.enterLocation')}
          minLength={2} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'default'}
          keyboardAppearance={'light'}
          //  listViewDisplayed="false"
          fetchDetails={true}
          enablePoweredByContainer={false}
          renderDescription={(row: any) => row.description}
          onPress={(data: any, details = null as any) => {
            let location = details?.geometry?.location;
            if (location) {
              setisAutoCompleteProcessing(true);
              revertseGeoCodeForPin(
                details?.formatted_address as any,
                location.lat,
                location.lng,
              );
            }
            //  onSelect(details);
          }}
          getDefaultValue={() => props.defaultValue}
          query={{
            key: AppConfig.mapKey,
            //  components: "country:us",
            language: 'en', // language of the results
            //   types: '(cities)' // default: 'geocode'
          }}
          styles={{
            textInputContainer: {
              width: '100%',
              paddingRight: 20,
              backgroundColor: AppColors.lightColor,
              borderWidth: 1,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderTopColor: AppColors.borderColor,
              borderBottomColor: AppColors.borderColor,
              borderColor: AppColors.borderColor,
              borderRadius: 5,
              height: props.isInPickLocationScreen ? 60 : 50,
              paddingLeft: props.isInPickLocationScreen ? 30 : 0,
            },
            textInput: {
              minHeight: props.isInPickLocationScreen ? 40 : 30,
              fontFamily: Fonts.Regular,
              fontSize: 14,
            },
            description: {
              fontWeight: 'bold',
            },
            predefinedPlacesDescription: {
              color: '#1faadb',
            },
          }}
          // currentLocation={true}
          // currentLocationLabel="Current location"
          nearbyPlacesAPI="GooglePlacesSearch"
          GooglePlacesSearchQuery={{
            rankby: 'distance',
          }}
          GooglePlacesDetailsQuery={{
            fields: 'formatted_address,geometry,address_components',
          }}
          debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
        />
        <View
          style={{
            position: 'absolute',
            top: 2,
            bottom: 0,
            right: props.isInPickLocationScreen ? undefined : 0,
            left: props.isInPickLocationScreen ? 0 : undefined,
          }}>
          <TouchableOpacity
            onPress={() => {
              fetchLocation();
            }}>
            <View style={{padding: 10}}>
              <Image
                resizeMode="contain"
                style={{
                  height: props.isInPickLocationScreen ? 30 : 20,
                  width: 20,
                }}
                source={Img.location}
              />
              {loading && (
                <View style={{position: 'absolute', left: 0, right: 0, top: 0}}>
                  <ActivityIndicator size={'large'}></ActivityIndicator>
                </View>
              )}
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export class PlaceDetails {
  showPopup: boolean = false;
  lat: number = 0;
  lng: number = 0;
  formattedAddress: string = '';
  country: PlaceItem = new PlaceItem();
  state: PlaceItem = new PlaceItem();
  city: PlaceItem = new PlaceItem();
  locality: string = '';
  streetNumber: string = '';
  postalCode: string = '';
  route: string = '';
}
export class PlaceItem {
  shortName: string = '';
  longName: string = '';
}

export class UserLocation {
  lat: number = 37.78825;
  lng: number = -122.4324;
}
