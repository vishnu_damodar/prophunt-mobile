import React from "react";
import { View, Image, ImageBackground } from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import Img from "@app/assets/images";
import { AppColors } from "@app/config/ui/color";

export const LogoHeader: React.FunctionComponent = () => {
  return (
    <View style={{ backgroundColor: AppColors.primaryThemeColor }}>
      <ImageBackground
        source={Img.ellipseCornerLeft}
        imageStyle={{
          width: 80,
          height: 80,
          padding: ViewportService.commonPadding,
          paddingTop: ViewportService.commonPadding * 2,
          resizeMode: "contain",
          zIndex:1
        }}
        style={{}}
        resizeMode={"contain"}
      >
        <View
          style={{
            alignItems: "center",
            paddingVertical: ViewportService.commonPadding,
          }}
        >
          <Image
            resizeMode="contain"
            style={{ height: 60, width: 240}}
            source={Img.logo}
          />
        </View>
      </ImageBackground>
    </View>
  );
};
