import React, { useEffect } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';

// import {
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
//   LoginManager,
// } from 'react-native-fbsdk';
import { AppColors } from '@app/config/ui/color';
import { Fonts } from '@app/config/ui/font';
import Img from '@app/assets/images';

export const FacebookLoginComponent = (props: { onLogin: any }) => {
  const customFacebookLogout = () => {
    // var current_access_token = '';
    // AccessToken.getCurrentAccessToken()
    //   .then((data: any) => {
    //     current_access_token = data.accessToken.toString();
    //   })
    //   .then(() => {
    //     let logout = new GraphRequest(
    //       'me/permissions/',
    //       {
    //         accessToken: current_access_token,
    //         httpMethod: 'DELETE',
    //       },
    //       (error: any, result: any) => {
    //         if (error) {
    //         } else {
    //           LoginManager.logOut();
    //         }
    //       },
    //     );
    //     new GraphRequestManager().addRequest(logout).start();
    //   })
    //   .catch((error: any) => {});
  };

  const handleFacebookLogin = () => {
    // LoginManager.logInWithPermissions(['public_profile', 'email']).then(
    //   (result: any) => {
    //     if (result.isCancelled) {
    //     } else {
    //       AccessToken.getCurrentAccessToken().then((data: any) => {
    //         fetch(
    //           'https://graph.facebook.com/v2.5/me?fields=email,name,friends,first_name,last_name&access_token=' +
    //             data.accessToken.toString(),
    //         )
    //           .then((response) => response.json())
    //           .then((response) => {
    //             // Some user object has been set up somewhere, build that user here

    //             if (response !== null) {
    //               const loginDetails: IFaceBookUser = response;
    //               customFacebookLogout();
    //               props.onLogin(loginDetails);
    //             }
    //           })
    //           .catch((error) => {});
    //       });
    //     }
    //   },
    //   (error: any) => {},
    // );
  };

  return (
    <View>
      <TouchableOpacity onPress={handleFacebookLogin}>
        <Image
          resizeMode="contain"
          style={{ height: 50, width: 50 }}
          source={Img.facebook}
        />
      </TouchableOpacity>
    </View>
  );
};

export interface IFaceBookUser {
  email: string;
  name: string;
  first_name: string;
  last_name: string;
  id: string;
}
