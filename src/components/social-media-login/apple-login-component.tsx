import React, { useEffect } from "react";
import { View, TouchableOpacity, Image } from "react-native";
// import appleAuth, {
//   AppleAuthRequestOperation,
//   AppleAuthRequestScope,
//   AppleAuthCredentialState,
//   AppleAuthError,
// } from "@invertase/react-native-apple-authentication";
import { ToasterReferences } from "@app/services/toast-service";
import { translate } from "@app/config/locale/i18n";
import { AppleLoginModel } from "@app/store/auth/auth-models";
import Img from "@app/assets/images";
export const AppleLoginComponent = (props: { onLogin: any }) => {
  useEffect(() => {}, []);

  const onAppleButtonPress = async () => {
    // try {
    //   const appleAuthRequestResponse = await appleAuth.performRequest({
    //     requestedOperation: AppleAuthRequestOperation.LOGIN,
    //     requestedScopes: [
    //       AppleAuthRequestScope.EMAIL,
    //       AppleAuthRequestScope.FULL_NAME,
    //     ],
    //   });
    //   if (appleAuthRequestResponse) {
    //     let loginDetails: AppleLoginModel = new AppleLoginModel();
    //     if (appleAuthRequestResponse.email) {
    //       loginDetails.email = appleAuthRequestResponse?.email ?? "";
    //     }
    //     loginDetails.firstName =
    //       appleAuthRequestResponse?.fullName?.givenName ?? "";
    //     loginDetails.lastName =
    //       appleAuthRequestResponse?.fullName?.familyName ?? "";
    //     loginDetails.user = appleAuthRequestResponse?.user ?? "";
    //     props.onLogin(loginDetails);
    //   }
    // } catch (error) {
    //   if (error.code === AppleAuthError.CANCELED) {
    //     ToasterReferences.showErrorMsg(translate("login.cancelledAppleLogin"));
    //   } else {
    //     ToasterReferences.showErrorMsg(error?.message ?? "");
    //   }
    // }
  };

  const supportCheck = async () => {
    // if (!appleAuth.isSupported) {
    //   ToasterReferences.showErrorMsg(translate("login.notSupported"));
    // } else {
    //   onAppleButtonPress();
    // }
  };

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          supportCheck();
        }}
      >
        <Image
          resizeMode="contain"
          style={{ height: 50, width: 50 }}
          source={Img.apple}
        />
      </TouchableOpacity>
    </View>
  );
};

export interface IAppleUser {
  email: string;
  first_name: string;
  last_name: string;
  user: string;
}
