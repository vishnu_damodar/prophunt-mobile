import React, { useEffect } from 'react';
import { View, TouchableOpacity, Text, Image, Alert } from 'react-native';
// import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import { GoogleConfigs } from '@app/config/app-config';
import Img from '@app/assets/images';
export const GoogleLoginComponent = (props: { onLogin: any }) => {
  const configureGoogleSignIn = () => {
    // GoogleSignin.configure({
    //   webClientId: GoogleConfigs.webClientId,
    //   offlineAccess: false,
    // });
    // getCurrentUser();
  };

  useEffect(() => {
    // configureGoogleSignIn();
  }, []);

  const getCurrentUser = async () => {
    // try {
    //   const userInfo = await GoogleSignin.signInSilently();
    // } catch (error) {
    //   const errorMessage =
    //     error.code === statusCodes.SIGN_IN_REQUIRED
    //       ? 'Please sign in'
    //       : error.message;
    // }
  };

  const signIn = async () => {
    // try {
    //   await GoogleSignin.hasPlayServices();
    //   const userInfo: any = await GoogleSignin.signIn();
    //   if (userInfo !== null) {
    //     const loginDetails: IGoogleUser = userInfo.user;
    //     await signOut();
    //     props.onLogin(loginDetails);
    //   }
    // } catch (error) {
    //   switch (error.code) {
    //     case statusCodes.SIGN_IN_CANCELLED:
    //       // sign in was cancelled
    //       // Alert.alert('cancelled');
    //       break;
    //     case statusCodes.IN_PROGRESS:
    //       // operation (eg. sign in) already in progress
    //       // Alert.alert('in progress');
    //       break;
    //     case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
    //       // android only
    //       Alert.alert('play services not available or outdated');
    //       break;
    //     default:
    //       Alert.alert('Something went wrong', error.toString());
    //   }
    // }
  };

  const signOut = async () => {
    // try {
    //   await GoogleSignin.revokeAccess();
    //   await GoogleSignin.signOut();
    // } catch (error) {}
  };

  return (
    <View>
      <TouchableOpacity onPress={signIn}>
        <Image
          resizeMode="contain"
          style={{ height: 50, width: 50 }}
          source={Img.google}
        />
      </TouchableOpacity>
    </View>
  );
};

export interface IGoogleSignInResponseModel {
  scopes: string[];
  serverAuthCode?: any;
  idToken: string;
  user: IGoogleUser;
}
export interface IGoogleUser {
  photo: string;
  email: string;
  familyName: string;
  givenName: string;
  name: string;
  id: string;
}
