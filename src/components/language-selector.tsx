import Img from "@app/assets/images";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import useAppSettings from "@app/hooks/useAppSettings";
import React, { useEffect, useState } from "react";
import { View, ViewStyle, TouchableOpacity, Image } from "react-native";
import { BarIndicator } from "react-native-indicators";
import { NormalText } from "./common/text/normal-text";

interface Props {
  style?: ViewStyle;
  items: any[];
  valueKey: string;
  onSelect: (data: any) => void;
  defaultText: string;
}

const LanguageSelector: React.FunctionComponent<Props> = (props) => {
  const { locale } = useAppSettings();
  const [expand, setExpand] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>({
    [props.valueKey]: locale.languageName,
  });

  let flag: any = "";
  switch (locale.languageId) {
    case 1:
      flag = Img.usFlag;
      break;
    case 8:
      flag = Img.frenchFlag;
      break;
    default:
      flag = Img.usFlag;
  }
  //   useEffect(() => {
  //     if (props.items[0].id !== "-1") {
  //       props.items.unshift({
  //         id: "-1",
  //         [props.valueKey]: props.defaultText,
  //       });
  //     }
  //   }, [props.items]);
  return (
    <View style={[{ backgroundColor: AppColors.lightGray }, props?.style]}>
      <TouchableOpacity
        style={[
          {
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: ViewportService.commonPadding,
            paddingVertical: ViewportService.commonPadding * 0.5,
            alignItems: "center",
          },
        ]}
        onPress={() => {
          setExpand(!expand);
        }}
      >
        <View style={{ flex: 1 }}>
          <NormalText color={AppColors.fadeTextColor}>
            {" "}
            {selectedItem ? selectedItem[props.valueKey]?? props.defaultText : props.defaultText}
          </NormalText>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View
            style={{
              marginHorizontal: ViewportService.commonPadding,
              borderRadius: 20,
            }}
          >
            <Image
              resizeMode={"contain"}
              source={flag}
              style={{
                height: 15,
                width: 15,
              }}
            />
          </View>

          <Image
            resizeMode={"contain"}
            source={Img.dropdownArrow}
            style={[{ transform: [{ scaleY: expand ? -1 : 1 }] }]}
          />
        </View>
      </TouchableOpacity>
      {expand && (
        <View
          style={{
            backgroundColor: AppColors.lightColor,
            borderBottomColor: AppColors.borderColor,
            borderBottomWidth: 1,
          }}
        >
          {props.items.map((item, index) => {
            return (
              <TouchableOpacity
                key={`exList_${index}`}
                style={[
                  {
                    flexDirection: "row",
                    justifyContent: "space-between",
                    paddingHorizontal: ViewportService.commonPadding,
                    paddingVertical: ViewportService.commonPadding * 0.5,
                    alignItems: "center",
                  },
                ]}
                onPress={() => {
                  props.onSelect(item);
                  setSelectedItem(item);
                  setExpand(!expand);
                }}
              >
                <View style={{ flex: 1 }}>
                  <NormalText>
                    <NormalText>{item[props.valueKey] ?? ""}</NormalText>
                  </NormalText>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default LanguageSelector;
