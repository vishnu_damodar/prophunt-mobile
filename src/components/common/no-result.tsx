import React from 'react';
import {View, Image} from 'react-native';
import {AppColors} from '@app/config/ui/color';
import {translate} from '@app/config/locale/i18n';
import AppText from './text/AppText';
import {ViewportService} from '@app/config/ui/viewport-service';
interface Props {
  message?: string;
}
export const NoResult: React.FunctionComponent<Props> = (props) => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
      }}>
      <View style={{margin: ViewportService.commonPadding}}>
        <Image
          resizeMode="contain"
          style={{
            height: 70,
            width: 70,
            // tintColor: AppColors.primaryThemeColor,
          }}
          source={require('@app/assets/img/no-record.png')}
        />
      </View>
      <View style={{}}>
        <AppText
          style={{
            fontSize: ViewportService.subHeading,
            color: AppColors.fadeTextColor,
          }}>
          {props?.message ?? 'No Records'}
        </AppText>
      </View>
    </View>
  );
};

export default NoResult;
