import React from 'react';
import {View, Image, Platform, TouchableOpacity} from 'react-native';
import {styles} from './sidebar-styles';
import {Root, Text} from 'native-base';
import {changeLanguage} from '@app/store/settings/settings-actions';
import {useDispatch, useSelector} from 'react-redux';
// import {ISettings} from '@app/store/settings/settings-models';
// import {IAppState} from '@app/store/state-props';
import {AppRoutes} from '@app/route/route-keys';
import {translate} from '@app/config/locale/i18n';
import {NavigationService} from '@app/services/navigation-service';
import {AppColors} from '@app/config/ui/color';

// import {
//   createLogoutAction,
//   setPostLoginAction,
// } from '@app/store/auth/auth-actions';
import {AppConfig} from '@app/config/app-config';
import {Fonts} from '@app/config/ui/font';

export const SideBar = (_props: any) => {
  const dispatch = useDispatch();
  // const settings: ISettings = useSelector((state: IAppState) => state.settings);
  //   const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const sidebarItems: Array<ISideBarItem> = [
    {
      displayText: translate('sidebar.myOrders'),
      route: AppRoutes.Login,
    },
  ];

  const navigator = (item: ISideBarItem) => {};

  return (
    <View style={[styles.container]}>
      <Root>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Drawer</Text>
        </View>
      </Root>
    </View>
  );
};

interface ISideBarItem {
  displayText: string;
  route: string;
}
