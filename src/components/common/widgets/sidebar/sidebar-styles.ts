import {StyleSheet, Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
const hasNotch = DeviceInfo.hasNotch();

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    marginTop: Platform.OS === 'ios' && hasNotch ? 36 : 0,
  },
});
