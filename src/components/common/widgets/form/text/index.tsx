import React, { useState, useEffect } from "react";
import {
  View,
  TextInput,
  StyleProp,
  ViewStyle,
  Alert,
  Text,
  TextStyle,
} from "react-native";

import { validate } from "../validators";
import { AppColors } from "@app/config/ui/color";
import { Fonts } from "@app/config/ui/font";
import { commonStyles } from "@app/components/common/common-style";

export const useTextFormInput = (
  initialValue: any,
  validators: Array<any>,
  numberOfLines?: number,
  maxLength?: number
) => {
  const [value, setValue] = useState(initialValue);
  const [isValid, setIsValid] = useState(false);
  const [errorMesage, setErrorMesage] = useState("");
  const [showError, setShowError] = useState(false);
  useEffect(() => {
    let validator = validate(validators, value);
    setIsValid(validator.isValid);
    setErrorMesage(validator.message);
  }, []);
  useEffect(() => {
    let validator = validate(validators, value);
    setIsValid(validator.isValid);
    setShowError(false);
    setErrorMesage(validator.message);
  }, [value]);
  const onChange = (value: any) => {
    setValue(value);
  };
  const showErrorMessage = () => {
    setShowError(true);
  };
  const widget = (
    containerStyle: StyleProp<ViewStyle>,
    inputStyle: StyleProp<TextStyle>,
    props: any,
    onValueChange?: any,
    keyboardType?: any,
    disabled?: boolean,
    onBlur?: any
  ) => {
    let disable = disabled ?? false;
    const [isFocused, setIsFocused] = useState(false);
    return (
      <View>
        <View
          style={[
            containerStyle,
            {
              borderColor: isFocused
                ? AppColors.primaryThemeColor
                : AppColors.borderColor,
            },
          ]}
        >
          <TextInput
            placeholderTextColor="#BEBEBE"
            {...props}
            value={value}
            editable={!disable}
            maxLength={maxLength != null ? maxLength : 400}
            numberOfLines={numberOfLines != null ? numberOfLines : 1}
            multiline={numberOfLines != null ? true : false}
            onBlur={() => {
              setIsFocused(false);
              if (onBlur != null) {
                onBlur();
              }
            }}
            onFocus={() => {
              setIsFocused(true);
            }}
            onChangeText={(value) => {
              onChange(value);

              if (onValueChange) {
                onValueChange(value);
              }
            }}
            keyboardType={keyboardType ?? "default"}
            style={[
              { textAlignVertical: "center", fontFamily: Fonts.Regular },
              inputStyle,
            ]}
          ></TextInput>
        </View>
        <View>
          {showError && (
            <Text style={commonStyles.errorMessage}>{errorMesage}</Text>
          )}
        </View>
      </View>
    );
  };

  return {
    value,
    setValue,
    onChange: onChange,
    widget,
    valid: isValid,
    errorMesage: errorMesage,
    showErrorMessage: showErrorMessage,
  };
};
