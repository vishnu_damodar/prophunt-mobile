const isRequired = (value: string) => {
  if (value.length > 0) {
    return true;
  }
  return false;
};

const isValidEmail = (value: string) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(value) === false) {
    return false;
  }
  return true;
};

const isValidPassword = (value: string) => {
  let reg = /^[A-Za-z0-9!#_]{5,50}$/;
  if (reg.test(value) === false) {
    return false;
  }
  return true;
};

const requiredValidator = (message: string) => {
  const validate = (value: string) => {
    if (!isRequired(value)) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const dropdownValidator = (message: string) => {
  const validate = (value: any) => {
    if (value == -1) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const emailValidator = (message: string) => {
  const validate = (value: string) => {
    if (!isValidEmail(value)) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const passwordValidator = (message: string) => {
  const validate = (value: string) => {
    if (!isValidPassword(value)) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const minValueValidator = (minValue: number, message: string) => {
  const validate = (value: string) => {
    let originalValue = 0;
    try {
      if (value != null) {
        originalValue = parseFloat(value);
      }
    } catch (err) {}
    if (minValue > originalValue) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const maxValueValidator = (maxValue: number, message: string) => {
  const validate = (value: string) => {
    let originalValue = 0;
    try {
      if (value != null) {
        originalValue = parseFloat(value);
      }
    } catch (err) {}
    if (maxValue < originalValue) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const maxLengthValidator = (maxLength: number, message: string) => {
  const validate = (value: string) => {
    let originalValue = value.trim();

    if (maxLength < originalValue.length) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

const confirmValidator = (newPassword: string, message: string) => {
  const validate = (value: string) => {
    if (newPassword != value) {
      return {status: false, message: message};
    }
    return {status: true, message: ''};
  };
  return {validate};
};

export const validators = {
  requiredValidator,
  emailValidator,
  passwordValidator,
  dropdownValidator,
  minValueValidator,
  maxValueValidator,
  maxLengthValidator,
  confirmValidator
};

export const validate = (validators: Array<any>, value: any) => {
  let isValid = true;
  let message = '';
  for (var item of validators) {
    let validator = item.validate(value);
    isValid = validator.status;
    message = validator.message;
    if (!isValid) break;
  }
  return {isValid, message};
};
