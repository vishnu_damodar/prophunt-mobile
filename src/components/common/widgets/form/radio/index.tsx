import React, {useState} from 'react';
import {
  StyleProp,
  View,
  ViewStyle,
  Platform,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {validate} from '../validators';
export const Radio = (initialValue: any, validators: Array<any>) => {
  const [value, setValue] = useState(initialValue);
  const [isValid, setIsValid] = useState(false);
  const [errorMesage, setErrorMesage] = useState('');

  const onChange = (itemValue: any, itemIndex: any) => {
    if (itemValue != -1) {
      setValue(itemValue);
      let validator = validate(validators, itemValue);
      setIsValid(validator.isValid);
      setErrorMesage(validator.message);
    }
  };
  const renderPickerWithLabels = (
    items: Array<any>,
    valueField: string,
    titleField: string,
    onchangeValue: any,
  ) => {
    let pickerItems = items.map((item, index) => {
      return (
        <TouchableOpacity
          key={'radio-option' + index + item[valueField]}
          onPress={() => {
            if (onchangeValue) {
              onchangeValue(item[valueField]);
            }
            onChange(item[valueField], index);
          }}>
          <View
            style={{
              padding: 10,
              paddingLeft: 0,
              flexDirection: 'row',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            {value == item[valueField] && (
              <Image
                resizeMode="contain"
                style={{height: 25, width: 25}}
                source={require('project/src/assets/img/radio-active.png')}
              />
            )}
            {value != item[valueField] && (
              <Image
                resizeMode="contain"
                style={{height: 25, width: 25}}
                source={require('project/src/assets/img/radio.png')}
              />
            )}

            <Text style={{marginLeft: 10}}>{item[titleField]}</Text>
          </View>
        </TouchableOpacity>
      );
    });

    return pickerItems;
  };
  const widget = (
    items: Array<any>,
    valueField: string,
    titleField: string,
    pickerStyle: StyleProp<ViewStyle>,
    containerStyle: StyleProp<ViewStyle>,
    onchangeValue?: any,
    color = 'black',
    placeHolder = 'Select',
  ) => {
    return (
      <View style={containerStyle}>
        {renderPickerWithLabels(items, valueField, titleField, onchangeValue)}
      </View>
    );
  };
  return {
    value,
    setValue,
    onChange,
    widget,
    valid: isValid,
  };
};
