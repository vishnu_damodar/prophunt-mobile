import {
  View,
  Alert,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {CheckBox, Text} from 'native-base';
import React, {useEffect, useState} from 'react';
import {AppColors} from '@project/config/ui/color';
export interface CustomProps {
  options: Array<any>;
  titleField: string;
  valueField: string;
  selectedItems: Array<number>;
  onSubmit: any;
}
export const MultSelect = (props: CustomProps) => {
  const [items, setItems] = useState<Array<any>>([]);
  const [showList, setShowList] = useState<boolean>(false);
  const mapItems = () => {
    const mappedItems = props.options.map((item) => {
      if (items.find((x) => x == item[props.valueField])) {
        item.checked = true;
      } else {
        item.checked = false;
      }
      return item;
    });
    setItems(mappedItems);
  };
  useEffect(() => {
    mapItems();
  }, []);
  const onCheckChanged = (index: number, activeItem: any) => {
    setItems(
      items.map((item, index) => {
        if (item[props.valueField] == activeItem[props.valueField]) {
          item.checked = !item.checked;
        }
        return item;
      }),
    );
  };
  const onSubmit = () => {
    const selectedValues = items
      .filter((p) => p.checked)
      .map((x) => {
        return x[props.valueField];
      });
    props.onSubmit(selectedValues);
    setShowList(false);
    mapItems();
  };
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}}>
        <TextInput
          onFocus={() => {
            setShowList(true);
          }}
          style={{
            height: 45,
            backgroundColor: '#f1f1f1',
            borderRadius: 10,
          }}></TextInput>
      </View>
      {showList && (
        <View
          style={{
            position: 'absolute',
            height: 220,
            zIndex: 40,
            top: 40,
            width: '100%',
            backgroundColor: AppColors.lightColor,
          }}>
          <ScrollView
            style={{
              maxHeight: 180,
              // position: "absolute",
              flex: 1,
              backgroundColor: AppColors.lightColor,
              width: '100%',
            }}>
            {items.length == 0 && (
              <View>
                <Text>No Items Found</Text>
              </View>
            )}
            {items.map((item, index) => {
              return (
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginHorizontal: 20,
                    marginVertical: 10,
                  }}
                  key={index}>
                  <View style={{width: '90%'}}>
                    <Text>{item[props.titleField]}</Text>
                  </View>
                  <View>
                    <CheckBox
                      color={AppColors.primaryThemeColor}
                      style={{borderColor: AppColors.primaryThemeColor}}
                      onPress={() => {
                        onCheckChanged(index, item);
                      }}
                      checked={item.checked}
                    />
                  </View>
                </View>
              );
            })}
          </ScrollView>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 10,
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: AppColors.primaryThemeColor,
                width: 120,
                marginHorizontal: 10,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => {
                setShowList(false);
              }}>
              <Text style={{color: AppColors.lightColor}}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={onSubmit}
              style={{
                backgroundColor: AppColors.primaryThemeColor,
                width: 120,
                marginHorizontal: 10,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: AppColors.lightColor}}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};
