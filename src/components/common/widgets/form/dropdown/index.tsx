import React, {useState, useEffect} from 'react';
import {StyleProp, View, ViewStyle, Platform, Text, Image} from 'react-native';
import {Picker} from 'native-base';
import {validate} from '../validators';
import {AppColors} from '@project/config/ui/color';
import {Fonts} from '@project/config/ui/font';
import {commonStyles} from '@project/components/common/common-style';
export const DropDown = (initialValue: any, validators: Array<any>) => {
  const [value, setValue] = useState(initialValue);
  const [isValid, setIsValid] = useState(false);
  const [errorMesage, setErrorMesage] = useState('');
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    let validator = validate(validators, value);
    setIsValid(validator.isValid);
    setErrorMesage(validator.message);
  }, []);

  useEffect(() => {
    let validator = validate(validators, value);
    setIsValid(validator.isValid);
    setShowError(false);
    setErrorMesage(validator.message);
  }, [value]);

  const onChange = (itemValue: any, itemIndex: any) => {
    setValue(itemValue);
    setShowError(false);
    let validator = validate(validators, itemValue);
    setIsValid(validator.isValid);
    setErrorMesage(validator.message);
  };
  const showErrorMessage = () => {
    setShowError(true);
  };
  const renderPickerWithLabels = (
    items: Array<any>,
    showLabel: boolean,
    placeHolder: string,
    valueField: string,
    titleField: string,
  ) => {
    let pickerItems = items.map((item, index) => {
      return (
        <Picker.Item
          key={index}
          label={item[titleField]}
          value={item[valueField]}
        />
      );
    });
    if (showLabel) {
      pickerItems.unshift(
        <Picker.Item key="label" label={placeHolder} value={-1} />,
      );
    }
    return pickerItems;
  };
  const widget = (
    items: Array<any>,
    valueField: string,
    titleField: string,
    containerStyle: StyleProp<ViewStyle>,
    pickerStyle: StyleProp<ViewStyle>,
    onchangeValue?: any,
    color = 'black',
    placeHolder = 'Select',
  ) => {
    let showLabel = Platform.OS === 'android' && placeHolder != 'Select';
    return (
      <View>
        <View
          style={[
            containerStyle,
            {
              paddingLeft: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
            },
          ]}>
          <Picker
            selectedValue={value}
            style={[pickerStyle, {fontSize: 14}]}
            placeholderIconColor={AppColors.lightColor}
            placeholderStyle={{color: AppColors.lightColor}}
            textStyle={{
              color: color,
              fontSize: 14,
              fontFamily: Fonts.Medium,
            }}
            placeholder={placeHolder}
            itemStyle={{fontFamily: Fonts.Medium, fontSize: 14}}
            onValueChange={(itemValue: any, itemIndex: any) => {
              if (onchangeValue) {
                onchangeValue(itemValue);
              }
              onChange(itemValue, itemIndex);
            }}>
            {renderPickerWithLabels(
              items,
              showLabel,
              placeHolder,
              valueField,
              titleField,
            )}
          </Picker>
          {Platform.OS === 'ios' && (
            <Image
              resizeMode="contain"
              style={{height: 12, width: 12}}
              source={require('project/src/assets/img/down-arrow-line.png')}
            />
          )}
        </View>
        <View>
          {showError && (
            <Text style={commonStyles.errorMessage}>{errorMesage}</Text>
          )}
        </View>
      </View>
    );
  };
  return {
    value,
    setValue,
    onChange,
    widget,
    valid: isValid,
    showErrorMessage: showErrorMessage,
  };
};

export interface IKeyValuePair {
  key: any;
  value: string;
}
