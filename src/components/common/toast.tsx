import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Animated,
    Dimensions,
    Text,
    StyleProp,
    ViewStyle,
    TextStyle,
    Image
} from "react-native";

export const DURATION = {
    LENGTH_SHORT: 500,
    FOREVER: 0
};

const { height, width } = Dimensions.get("window");
interface Props {
    style: StyleProp<ViewStyle>;
    position: "top" | "center" | "bottom";
    textStyle: StyleProp<TextStyle>;
    positionValue: number;
    fadeInDuration: number;
    fadeOutDuration: number;
    opacity: number;
    defaultCloseDelay?: number;
}
export default class Toast extends Component<
    Props,
    { isShow: boolean; text: string; opacityValue: any; type: string }
> {
    constructor(props: any) {
        super(props);
        this.state = {
            isShow: false,
            text: "",
            opacityValue: new Animated.Value(this.props.opacity),
            type: ""
        };
    }
    isShow!: boolean;
    duration!: number;
    callback!: any;
    animation!: any;
    timer!: any;
    show(
        text: string,
        duration: number,
        type: "error" | "warning" | "success",
        callback: () => void
    ) {
        this.duration =
            typeof duration === "number" ? duration : DURATION.LENGTH_SHORT;
        this.callback = callback;
        this.setState({
            isShow: true,
            text: text,
            type: type
        });

        this.animation = Animated.timing(this.state.opacityValue, {
            toValue: this.props.opacity,
            duration: this.props.fadeInDuration,
            useNativeDriver: true
        });
        this.animation.start(() => {
            this.isShow = true;
            if (duration !== DURATION.FOREVER) this.close();
        });
    }

    close(duration: number | undefined) {
        let delay = typeof duration === "undefined" ? this.duration : duration;

        if (delay === DURATION.FOREVER)
            delay = this.props.defaultCloseDelay || 250;

        if (!this.isShow && !this.state.isShow) return;
        this.timer && clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.animation = Animated.timing(this.state.opacityValue, {
                toValue: 0.0,
                duration: this.props.fadeOutDuration,
                useNativeDriver: true
            });
            this.animation.start(() => {
                this.setState({
                    isShow: false
                });
                this.isShow = false;
                if (typeof this.callback === "function") {
                    this.callback();
                }
            });
        }, delay);
    }

    componentWillUnmount() {
        this.animation && this.animation.stop();
        this.timer && clearTimeout(this.timer);
    }

    render() {
        let pos;
        switch (this.props.position) {
            case "top":
                pos = this.props.positionValue;
                break;
            case "center":
                pos = height / 2;
                break;
            case "bottom":
                pos = height - this.props.positionValue;
                break;
        }

        const view = this.state.isShow ? (
            <View style={[styles.container, { top: pos }]} pointerEvents="none">
                <Animated.View
                    style={[
                        styles.content,
                        { opacity: this.state.opacityValue },
                        this.props.style
                    ]}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            flex: 1,
                            alignItems: "center"
                        }}
                    >
                        {/* <Image
                            source={
                                this.state.type === "success"
                                    ? require("@app/assets/icons/tick-default.png")
                                    : require("@app/assets/icons/alert.png")
                            }
                            style={{
                                height: 20,
                                width: 20,
                                tintColor: "#FFFFFF"
                            }}
                            resizeMode={"contain"}
                        /> */}
                        <View style={{ flex: 1 }}>
                            {React.isValidElement(this.state.text) ? (
                                this.state.text
                            ) : (
                                <Text style={this.props.textStyle}>
                                    {this.state.text}
                                </Text>
                            )}
                        </View>
                    </View>
                    {this.props.children}
                </Animated.View>
            </View>
        ) : null;
        return view;
    }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        left: 0,
        right: 0,
        elevation: 999,
        alignItems: "center",
        zIndex: 10000
    },
    content: {
        backgroundColor: "black",
        borderRadius: 5,
        padding: 10
    },
    text: {
        color: "white"
    }
});
