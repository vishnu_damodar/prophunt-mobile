import React, {useState} from 'react';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {View,TouchableOpacity } from 'react-native';
import AppButton from './AppButton';
import {ViewportService} from '@app/config/ui/viewport-service';
import {NormalText} from './text/normal-text';
import {AppColors} from '@app/config/ui/color';


interface Props {
  onSubmit: (otp: string) => void;
  onResent: (otp: string) => void;
  sendToAnother?: () => void;
  loading?: boolean;
}
export const OtpComponent: React.FunctionComponent<Props> = (props) => {
  const [otp, setOtp] = useState('');
  const [error, setError] = useState('');
  const onContinue = () => {
    if (otp.length !== 6) {
      setError('Please enter a valid OTP');
    } else {
      props.onSubmit(otp);
    }
  };
  return (
    <View>
      <View style={{flex: 1, marginBottom: ViewportService.commonPadding * 2}}>
        <View style={{paddingVertical: ViewportService.commonPadding}}>
          <OTPInputView
            pinCount={6}
            keyboardType="number-pad"
            autoFocusOnLoad
            codeInputFieldStyle={{color: 'black'}}
            // codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeChanged={(code: string) => {
              setError('');
              setOtp(code);
            }}
          />
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <NormalText color={AppColors.dangerTextColor}>{error}</NormalText>
          </View>
          <View style={{}}>
            <TouchableOpacity
              onPress={() => {
                props.onResent(otp);
              }}>
              <NormalText>Resend OTP</NormalText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {props.sendToAnother !== undefined && (
        <View
          style={{
            alignItems: 'flex-end',
            marginBottom: ViewportService.commonPadding,
          }}>
          <TouchableOpacity
            onPress={() => {
              if (props.sendToAnother !== undefined) {
                props.sendToAnother();
              }
            }}>
            <NormalText textDecorationLine={'underline'}>
              Send OTP to another number
            </NormalText>
          </TouchableOpacity>
        </View>
      )}
      <AppButton
        loading={props.loading ?? false}
        disabled={props.loading}
        title={'Continue'}
        onPress={() => {
          onContinue();
        }}></AppButton>
    </View>
  );
};

export interface IOTPDetails {}
