import Img from "@app/assets/images";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import React, { useEffect, useState } from "react";
import { View, ViewStyle, TouchableOpacity, Image } from "react-native";
import { BarIndicator } from "react-native-indicators";
import { NormalText } from "./text/normal-text";

interface Props {
  containerStyle?:ViewStyle;
  selectedBoxStyle?: ViewStyle;
  listBoxStyle?:ViewStyle;
  itemStyle?:ViewStyle;
  items: any[];
  valueKey: string;
  onSelect: (data: any) => void;
  defaultText: string;
}

const ExpandingList: React.FunctionComponent<Props> = (props) => {
  const [expand, setExpand] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>();
  //   useEffect(() => {
  //     if (props.items[0].id !== "-1") {
  //       props.items.unshift({
  //         id: "-1",
  //         [props.valueKey]: props.defaultText,
  //       });
  //     }
  //   }, [props.items]);
  return (
    <View style={[{ backgroundColor: AppColors.lightGray }, props?.containerStyle]}>
      <TouchableOpacity
        style={[
          {
            flexDirection: "row",
            justifyContent: "space-between",
            padding: ViewportService.commonPadding,
            alignItems: "center",
          },
          props?.selectedBoxStyle
        ]}
        onPress={() => {
          setExpand(!expand);
        }}
      >
        <View style={{ flex: 1 }}>
          <NormalText>
            {" "}
            {selectedItem ? selectedItem[props.valueKey] : props.defaultText}
          </NormalText>
        </View>
        <Image
          source={Img.dropdownArrow}
          style={[{ transform: [{ scaleY: expand ? -1 : 1 }] }]}
        />
      </TouchableOpacity>
      {expand && (
        <View style={[{ backgroundColor: AppColors.lightColor },props?.listBoxStyle]}>
          {props.items.map((item, index) => {
            return (
              <TouchableOpacity
                key={`exList_${index}`}
                style={[
                  {
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: ViewportService.commonPadding,
                    alignItems: "center",
                  },
                  props?.itemStyle
                ]}
                onPress={() => {
                  props.onSelect(item);
                  setSelectedItem(item);
                  setExpand(!expand);
                }}
              >
                <View style={{ flex: 1 }}>
                  <NormalText>
                    <NormalText>{item[props.valueKey] ?? ""}</NormalText>
                  </NormalText>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default ExpandingList;
