import React, { useState } from "react";
import {
  View,
  ViewProps,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  ViewStyle,
  SectionList,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "./text/normal-text";
import { SubHeading } from "./text/sub-heading";

export const BottomSectionLister: React.FunctionComponent<
  ViewProps & {
    items: any[];
    valueKey: string;
    sectionKey: string;
    sectionValuesKey: string;
    visibile?: boolean;
    height?: number;
    defaultText: string;
    defaultSelected?: any;
    onSelect: (data: any) => void;
    loading?: boolean;
    containerStyle?: ViewStyle;
  }
> = (props) => {
  // if (!props.visibile) {
  //     return null;
  // }
  const [isVisible, setListVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>(props.defaultSelected);
  let items = props.items.map((item) => {
    return {
      title: item[props.sectionKey],
      data: item[props.sectionValuesKey],
    };
  });
  const renderItem = (item, index) => {
    return (
      <TouchableOpacity
        key={`section_${index}`}
        onPress={() => {
          props.onSelect(item);
          setSelectedItem(item);
          setListVisible(false);
        }}
        style={{
          alignItems: "center",
          justifyContent: "center",
          padding: ViewportService.commonPadding - 6,
        }}
      >
        <NormalText>{item[props.valueKey] ?? ""}</NormalText>
      </TouchableOpacity>
    );
  };
  return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity
        style={[
          {
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            borderRadius: 3,
            borderColor: AppColors.borderColor,
            borderWidth: 0.7,
            padding: ViewportService.commonPadding,
            alignItems: "center",
          },
          props?.containerStyle,
        ]}
        onPress={() => {
          setListVisible(true);
        }}
      >
        <View style={{ flex: 1 }}>
          <NormalText>
            {selectedItem ? selectedItem[props.valueKey] : props.defaultText}
          </NormalText>
        </View>
        <Image source={Img.dropdownArrow} />
      </TouchableOpacity>

      <Modal
        transparent
        animationType="fade"
        visible={isVisible}
        onRequestClose={() => {
          setListVisible(false);
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "#00000050",
            justifyContent: "flex-end",
          }}
        >
          <View
            style={{
              minHeight: props?.height ?? 200,
              borderRadius: 20,
            }}
          >
            <View
              style={{
                alignItems: "flex-end",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  setListVisible(false);
                }}
                style={{
                  padding: ViewportService.commonPadding - 10,
                  marginRight: ViewportService.commonPadding - 10,
                }}
              >
                <Image
                  resizeMode={"contain"}
                  source={Img.cancel}
                  style={{ tintColor: AppColors.lightColor }}
                />
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
              <SectionList
                showsVerticalScrollIndicator={false}
                style={[
                  {
                    borderRadius: 20,
                    borderBottomRightRadius: 0,
                    borderBottomLeftRadius: 0,
                    backgroundColor: AppColors.lightColor,
                  },
                  props.style,
                ]}
                sections={items ?? []}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item, index }) => {
                  return renderItem(item, index);
                }}
                renderSectionHeader={({ section: { title } }) => (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      backgroundColor: AppColors.lightColor,
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: AppColors.borderColor,
                        height: 1,
                        alignSelf: "center",
                      }}
                    />
                    <SubHeading style={{ paddingHorizontal: 16 }} bold>
                      {title}
                    </SubHeading>
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: AppColors.borderColor,
                        height: 1,
                        alignSelf: "center",
                      }}
                    />
                  </View>
                )}
                ListHeaderComponent={
                  props.loading ? (
                    <ActivityIndicator
                      color={AppColors.primaryThemeColor}
                      size="small"
                    ></ActivityIndicator>
                  ) : (
                    <View></View>
                  )
                }
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default BottomSectionLister;
