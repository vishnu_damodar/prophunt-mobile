import {AppColors} from '@app/config/ui/color';
import {Fonts} from '@app/config/ui/font';
import React from 'react';
import {Text, TextProps} from 'react-native';

const AppText: React.FunctionComponent<TextProps & {bold?: boolean}> = (
  props,
) => {
  return (
    <Text
      {...props}
      style={[
        {
          fontFamily: props.bold ? Fonts.SemiBold : Fonts.Regular,
          fontWeight: props.bold ? '600' : 'normal',
          color: AppColors.darkColor,
        },
        props.style,
      ]}>
      {props.children}
    </Text>
  );
};

export default AppText;
