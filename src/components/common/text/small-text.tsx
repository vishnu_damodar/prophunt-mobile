import {Text} from 'react-native';
import React from 'react';
import {AppColors} from '@app/config/ui/color';
import {Fonts} from '@app/config/ui/font';
import {ViewportService} from '@app/config/ui/viewport-service';

export const SmallText = (props: {
  size?: number;
  color?: string;
  children?: any;
  fontFamily?: string;
}) => {
  return (
    <Text
      style={[
        {
          fontFamily: props.fontFamily ?? Fonts.Regular,
          fontSize: props.size ?? ViewportService.smallFont,
          color: props.color ?? AppColors.darkColor,
        },
      ]}>
      {props.children}
    </Text>
  );
};
