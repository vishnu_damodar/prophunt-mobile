import { Text, TextStyle } from "react-native";
import React from "react";
import { Fonts } from "@app/config/ui/font";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";

export const SubHeading = (props: {
  style?: TextStyle;
  size?: number;
  color?: string;
  children?: any;
  textDecorationLine?:
    | "none"
    | "underline"
    | "line-through"
    | "underline line-through";
  bold?: boolean;
}) => {
  return (
    <Text
      style={[
        {
          fontFamily: Fonts.SemiBold,
          fontSize: props.size ?? ViewportService.subHeading,
          color: props.color ?? AppColors.darkColor,
          textDecorationLine: props.textDecorationLine ?? "none",
          fontWeight: props.bold ? "600" : "normal",
        },
        props?.style,
      ]}
    >
      {props.children}
    </Text>
  );
};
