import React from "react";

import { AppColors } from "@app/config/ui/color";
import { Text, TextProps } from "react-native";

const ErrorText: React.FunctionComponent<TextProps> = (props) => {
    return (
        <Text
            {...props}
            style={[{ color: AppColors.dangerTextColor }, props.style]}
        >
            {props.children}
        </Text>
    );
};

export default ErrorText;
