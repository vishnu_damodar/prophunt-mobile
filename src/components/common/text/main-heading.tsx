import { View, Text, TouchableOpacity, Image, TextStyle } from "react-native";
import React from "react";
import { Fonts } from "@app/config/ui/font";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";

export const MainHeading = (props: {
  style?: TextStyle;
  size?: number;
  color?: string;
  children?: any;
}) => {
  return (
    <Text
      style={[
        {
          fontFamily: Fonts.Bold,
          fontSize: props.size ?? ViewportService.mainHeading,
          color: props.color ?? AppColors.darkColor,
          fontWeight: "bold",
        },
        props?.style,
      ]}
    >
      {props.children}
    </Text>
  );
};
