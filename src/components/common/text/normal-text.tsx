import {Text,TextStyle} from 'react-native';
import React from 'react';
import {Fonts} from '@app/config/ui/font';
import {AppColors} from '@app/config/ui/color';
import {ViewportService} from '@app/config/ui/viewport-service';

export const NormalText = (props: {
  style?: TextStyle;
  color?: string;
  children?: any;
  fontSize?: number;
  fontFamily?: string;
  textDecorationLine?:
    | 'none'
    | 'underline'
    | 'line-through'
    | 'underline line-through';
  numberOfLines?: number;
}) => {
  return (
    <Text
      numberOfLines={props.numberOfLines}
      style={[
        {
          fontFamily: props.fontFamily ?? Fonts.Regular,
          fontSize: props.fontSize ?? ViewportService.normalFont,
          color: props.color ?? AppColors.darkColor,
          textDecorationLine: props.textDecorationLine ?? 'none',
        },
        props?.style
      ]}>
      {props.children}
    </Text>
  );
};
