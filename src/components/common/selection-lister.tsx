import React, { useEffect, useState } from "react";
import {
  View,
  ViewProps,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  ViewStyle,
  TextProps,
  TextStyle,
  ImageStyle,
  Platform,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "./text/normal-text";
import AppButton from "./AppButton";
import { SmallText } from "./text/small-text";

export interface Props {
  selectionType: "single" | "multi";
  showSelected: boolean;
  label: string;
  items: any[];
  selectedItems?: any[];
  valueKey: string;
  idKey: string;
  height?: number;
  loading?: boolean;
  containerStyle?: ViewStyle;
  renderItem?: (item: any, index: number) => void;
  renderSelectedItem?: (item: any, index: number) => void;
  onSelect?: (data: any) => void;
  onApply?: (selectedItems: any[]) => void;
  disable?: boolean;
}

export const SelectionLister: React.FunctionComponent<Props> = (props) => {
  let selectionType = props.selectionType ?? "single";
  const [items, setItems] = useState<any[]>(props.items);
  const [showSelected, setShowSelected] = useState(props?.showSelected ?? true);
  const [isVisible, setListVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>();
  const [selectedItems, setSelectedItems] = useState<any[]>(
    props.selectedItems ?? []
  );
  useEffect(() => {
    setSelectedItems(props.selectedItems ?? []);
  }, [props.selectedItems]);

  useEffect(() => {
    if (props.items[props.idKey] !== "-1") {
      props.items.unshift({
        [props.idKey]: "-1",
        [props.valueKey]: props.label,
      });
    }
    setItems(props.items);
  }, [props.items]);
  const onItemSelect = (data: any) => {
    if (props.selectionType === "multi") {
      if (
        selectedItems.find((item) => item[props.idKey] == data[props.idKey])
      ) {
        let selected = selectedItems.filter((p) => {
          return p[props.idKey] !== data[props.idKey];
        });
        setSelectedItems(selected);
      } else {
        setSelectedItems([...selectedItems, data]);
      }
    } else {
      setSelectedItem(data);
      setListVisible(false);
      if (props.onSelect) {
        props.onSelect(data);
      }
    }
  };
  const itemComponent = (item: any, index: number) => {
    let isSelected: boolean = false;
    if (props.selectionType == "multi") {
      isSelected = selectedItems?.find(
        (p) => p[props.idKey] == item[props.idKey]
      )
        ? true
        : false;
    } else {
      if (selectedItem) {
        isSelected = selectedItem[props.idKey] === item[props.idKey];
      }
    }

    return (
      <TouchableOpacity
        disabled={props.disable}
        onPress={() => {
          onItemSelect(item);
        }}
        key={`${item[props.idKey]}_${index}`}
        style={{
          padding: 10,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {index > 0 && props.showSelected ? (
          <Image
            source={isSelected ? Img.radioButtonOn : Img.radioButton}
            resizeMode={"contain"}
            style={{ height: 15, width: 15 }}
          />
        ) : (
          <View
            style={{
              paddingHorizontal: ViewportService.smallPadding,
            }}
          />
        )}
        <View style={{ paddingHorizontal: ViewportService.smallPadding }}>
          <NormalText>{item[props.valueKey]}</NormalText>
        </View>
      </TouchableOpacity>
    );
  };

  const selectedItemComponent = (item: any, index: number) => {
    return (
      <View
        key={`${item[props.idKey]}_${index}`}
        style={{
          alignItems: "center",
          backgroundColor: AppColors.lightGray,
          margin: 5,
          flexDirection: "row",
          paddingLeft: 5,
          borderRadius: 2,
        }}
      >
        <SmallText>{item.value}</SmallText>
        <TouchableOpacity
          style={{ paddingVertical: 5, paddingHorizontal: 5 }}
          onPress={() => {
            onItemSelect(item);
          }}
        >
          <Image
            source={Img.cancel}
            style={{
              height: 7,
              width: 7,
              tintColor: AppColors.bodyTextColor,
            }}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flex: 1,
          borderRadius: 3,
          borderColor: AppColors.borderColor,
          borderWidth: 0.7,
        }}
      >
        <TouchableOpacity
          style={[
            {
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              padding: ViewportService.commonPadding,
              alignItems: "center",
            },
            props?.containerStyle,
          ]}
          onPress={() => {
            setListVisible(true);
          }}
        >
          <View style={{}}>
            {props.selectionType === "single" && (
              <NormalText>
                {selectedItem && showSelected
                  ? selectedItem[props.valueKey]
                  : props.label}
              </NormalText>
            )}
            {props.selectionType === "multi" && (
              <NormalText>{props.label}</NormalText>
            )}
          </View>
          <Image
            source={Img.dropdownArrow}
            resizeMode={"contain"}
            style={{ height: 10, width: 10 }}
          />
        </TouchableOpacity>
        {props.showSelected &&
          props.selectionType === "multi" &&
          selectedItems.length > 0 && (
            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                marginTop: 10,
                borderTopColor: AppColors.borderColor,
                borderTopWidth: 0.7,
              }}
            >
              {selectedItems.map((item, index) => {
                if (props?.renderSelectedItem) {
                  return props?.renderSelectedItem(item, index);
                } else {
                  return selectedItemComponent(item, index);
                }
              })}
            </View>
          )}
      </View>

      <Modal
        transparent
        animationType="fade"
        visible={isVisible}
        onRequestClose={() => {
          setListVisible(false);
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "#00000050",
            justifyContent: "flex-end",
          }}
        >
          <View
            style={{
              minHeight: props?.height ?? 200,
              borderRadius: 20,
            }}
          >
            <View
              style={{
                alignItems: "flex-end",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  setListVisible(false);
                }}
                style={{
                  padding: ViewportService.commonPadding - 10,
                  marginRight: ViewportService.commonPadding - 10,
                }}
              >
                <Image
                  resizeMode={"contain"}
                  source={Img.cancel}
                  style={{ tintColor: AppColors.lightColor }}
                />
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
              <ScrollView
                style={[
                  {
                    paddingTop: 20,
                    borderRadius: 20,
                    borderBottomRightRadius: 0,
                    borderBottomLeftRadius: 0,
                    backgroundColor: AppColors.lightColor,
                  },
                ]}
                contentContainerStyle={{
                  paddingBottom: ViewportService.commonPadding * 2,
                }}
              >
                {props.loading && (
                  <ActivityIndicator
                    color={AppColors.primaryThemeColor}
                    size="small"
                  ></ActivityIndicator>
                )}
                {!props.loading &&
                  items.map((item, index) => {
                    if (props.renderItem) {
                      return props.renderItem(item, index);
                    } else {
                      return itemComponent(item, index);
                    }
                  })}
              </ScrollView>
              {selectionType == "multi" && (
                <AppButton
                  title={"Apply"}
                  onPress={() => {
                    setListVisible(false);
                    if (props.onApply) {
                      props.onApply(selectedItems);
                    }
                  }}
                  style={{ marginBottom: Platform.OS === "ios" ? 10 : 0 }}
                />
              )}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default SelectionLister;
