import React from "react";
import { Text, Footer } from "native-base";
import { TouchableOpacity } from "react-native";
import {
  IRouteDetails,
  NavigationService,
} from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";
import { AppColors } from "@app/config/ui/color";
import { Image, ImageSourcePropType, View } from "react-native";
import { commonStyles } from "./common-style";
import Img from "@app/assets/images";
import { NormalText } from "./text/normal-text";
import { Fonts } from "@app/config/ui/font";
import { ViewportService } from "@app/config/ui/viewport-service";
import { IAppState } from "@app/store/state-props";
import { useSelector } from "react-redux";
import { AuthDetails } from "@app/store/auth/auth-models";

export const FooterComponent = () => {
  const currentRoute = NavigationService.getCurrentLocation();
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const footerItems: Array<IfooterItem> = [
    {
      title: "",
      icon: Img.home,
      routes: [AppRoutes.Landing],
      onTap: () => {
        NavigationService.navigate(AppRoutes.Landing);
      },
    },
    {
      title: "",
      icon: Img.search,
      routes: [AppRoutes.Search],
      onTap: () => {
        NavigationService.navigate(AppRoutes.Search, {
          purpose: "buy",
        });
      },
    },
    {
      title: "",
      icon: Img.add,
      isCenter: true,
      routes: [AppRoutes.Login],
      onTap: () => {
        if (auth.token) {
          NavigationService.navigate(AppRoutes.Dashboard);
        } else {
          NavigationService.navigate(AppRoutes.Login);
        }
      },
    },
    {
      title: "",
      icon: Img.user,
      routes: [AppRoutes.Login, AppRoutes.Dashboard],
      onTap: () => {
        if (auth.token) {
          NavigationService.navigate(AppRoutes.Dashboard);
        } else {
          NavigationService.navigate(AppRoutes.Login);
        }
      },
    },
    {
      title: "",
      icon: Img.footerMenu,
      routes: [""],
      onTap: () => {
        NavigationService.navigate("");
      },
    },
  ];

  return (
    <Footer style={commonStyles.footerContainer}>
      {footerItems.map((item, index) => (
        <FooterItem
          item={item}
          key={`footer-tab-${index}`}
          currentRoute={currentRoute}
        ></FooterItem>
      ))}
    </Footer>
  );
};

const FooterItem: React.FunctionComponent<{
  item: IfooterItem;
  currentRoute: IRouteDetails;
}> = (props) => {
  const { item, currentRoute } = props;

  const renderCount = () => {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          right: 0,
          height: 16,
          width: 16,
          backgroundColor: AppColors.primaryThemeColor,
          borderRadius: 30,
          alignContent: "center",
          alignItems: "center",
        }}
      >
        <NormalText>{item.count}</NormalText>
      </View>
    );
  };
  return (
    <TouchableOpacity
      onPress={() => {
        item.onTap();
      }}
    >
      <View style={[commonStyles.footerItem]}>
        <View style={{ paddingHorizontal: ViewportService.commonPadding }}>
          <FooterIcon
            path={item.icon}
            isCenter={item?.isCenter}
            active={item.routes.indexOf(currentRoute?.routeName) !== -1}
          />
          {item.count != null && item.count > 0 && renderCount()}
        </View>
        <View>
          {item.text == null && (
            <NormalText
              fontFamily={Fonts.SemiBold}
              color={
                item.routes.indexOf(currentRoute?.routeName) != -1
                  ? AppColors.primaryThemeColor
                  : AppColors.darkColor
              }
            >
              {item.title}
            </NormalText>
          )}
          {item.text != null && item.text != "" && (
            <NormalText>{item.text}</NormalText>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const FooterIcon: React.FunctionComponent<{
  active: boolean;
  isCenter?: boolean;
  path: ImageSourcePropType;
}> = (props) => {
  let dm = props.isCenter ? 30 : 20;
  return (
    <Image
      resizeMode={"contain"}
      style={{
        height: dm,
        width: dm,
        tintColor: !props.isCenter
          ? props.active
            ? AppColors.primaryThemeColor
            : AppColors.darkColor
          : undefined,
      }}
      source={props.path}
    />
  );
};

export interface IfooterItem {
  title: string;
  icon: any;
  activeIcon?: any;
  routes: Array<string>;
  onTap: any;
  text?: any;
  count?: number;
  isCenter?: boolean;
}
