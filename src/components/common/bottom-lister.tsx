import React, { useState } from "react";
import {
  View,
  ViewProps,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  ViewStyle,
  TextProps,
  TextStyle,
  ImageStyle,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "./text/normal-text";
import AppButton from "./AppButton";

export const BottomLister: React.FunctionComponent<
  ViewProps & {
    showSelected?: boolean;
    items: any[];
    valueKey: string;
    visibile?: boolean;
    height?: number;
    defaultText: string;
    defaultSelected: any;
    onSelect: (data: any) => void;
    loading?: boolean;
    containerStyle?: ViewStyle;
    leftIcon?: string;
    rightIcon?: string;
    iconStyle?: ImageStyle;
    labelStyle?: TextStyle;
    showRightIcon?: boolean;
    showLeftIcon?: boolean;
    itemContainerStyle?: ViewStyle;
    selectionType?: "single" | "multi";
    showSelection?: boolean;
    showIconSelectionLeft?: boolean;
    showIconSelectionRight?: boolean;
    itemPrefix?: string;
    itemSuffix?: string;
  }
> = (props) => {
  // if (!props.visibile) {
  //     return null;
  // }
  let showRightIcon = props.showRightIcon ?? props.showLeftIcon ? false : true;
  let showLeftIcon = props.showLeftIcon ?? false;
  let selectionType = props.selectionType ?? "single";
  let showSelection = props.showSelection ?? false;
  let showIconSelectionLeft = props.showIconSelectionLeft ?? false;
  let showIconSelectionRight = props.showIconSelectionRight ?? false;
  const [showSelected, setShowSelected] = useState(props?.showSelected ?? true);
  const [isVisible, setListVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>(props.defaultSelected);
  return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity
        style={[
          {
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            borderRadius: 3,
            borderColor: AppColors.borderColor,
            borderWidth: 0.7,
            padding: ViewportService.commonPadding,
            alignItems: "center",
          },
          props?.containerStyle,
        ]}
        onPress={() => {
          setListVisible(true);
        }}
      >
        {showLeftIcon && (
          <Image
            source={props.leftIcon ? props.leftIcon : Img.dropdownArrow}
            style={props.iconStyle}
          />
        )}
        <View style={{}}>
          <NormalText style={props.labelStyle}>
            {selectedItem && showSelected
              ? selectedItem[props.valueKey]
              : props.defaultText}
          </NormalText>
        </View>
        {showRightIcon && (
          <Image
            source={props.rightIcon ? props.rightIcon : Img.dropdownArrow}
            style={props.iconStyle}
          />
        )}
      </TouchableOpacity>

      <Modal
        transparent
        animationType="fade"
        visible={isVisible}
        onRequestClose={() => {
          setListVisible(false);
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "#00000050",
            justifyContent: "flex-end",
          }}
        >
          <View
            style={{
              minHeight: props?.height ?? 200,
              borderRadius: 20,
            }}
          >
            <View
              style={{
                alignItems: "flex-end",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  setListVisible(false);
                }}
                style={{
                  padding: ViewportService.commonPadding - 10,
                  marginRight: ViewportService.commonPadding - 10,
                }}
              >
                <Image
                  resizeMode={"contain"}
                  source={Img.cancel}
                  style={{ tintColor: AppColors.lightColor }}
                />
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
              <ScrollView
                {...props}
                style={[
                  {
                    paddingTop: 20,
                    borderRadius: 20,
                    borderBottomRightRadius: 0,
                    borderBottomLeftRadius: 0,
                    backgroundColor: AppColors.lightColor,
                  },
                  props.style,
                ]}
              >
                {props.loading && (
                  <ActivityIndicator
                    color={AppColors.primaryThemeColor}
                    size="small"
                  ></ActivityIndicator>
                )}
                {!props.loading &&
                  props.items.map((item, index) => {
                    return (
                      <TouchableOpacity
                        key={`bottomList_${index}_${item[props.valueKey]}`}
                        onPress={() => {
                          props.onSelect(item);
                          setSelectedItem(item);
                          setListVisible(false);
                        }}
                        style={[
                          {
                            alignItems: "center",
                            justifyContent: "center",
                            padding: ViewportService.commonPadding - 6,
                            flexDirection: "row",
                          },
                          props?.itemContainerStyle,
                        ]}
                      >
                        {showIconSelectionLeft && (
                          <View
                            style={{
                              paddingHorizontal: ViewportService.smallPadding,
                            }}
                          >
                            <Image
                              source={
                                selectedItem?.[props.valueKey] ===
                                item?.[props.valueKey]
                                  ? Img.radioButtonOn
                                  : Img.radio
                              }
                              style={{ height: 20, width: 20 }}
                              resizeMode={"contain"}
                            />
                          </View>
                        )}
                        <View style={{ flexDirection: "row" }}>
                          {props.itemPrefix && index >0 &&(
                            <NormalText>{props.itemPrefix ?? ""}</NormalText>
                          )}
                          <NormalText
                            style={{
                              paddingHorizontal: ViewportService.smallPadding,
                            }}
                          >
                            {item[props.valueKey] ?? ""}
                          </NormalText>
                          {props.itemSuffix && index >0(
                            <NormalText>{props.itemSuffix ?? ""}</NormalText>
                          )}
                        </View>

                        {showIconSelectionRight && (
                          <View
                            style={{
                              paddingHorizontal: ViewportService.smallPadding,
                            }}
                          >
                            <Image
                              source={
                                selectedItem?.[props.valueKey] ===
                                item?.[props.valueKey]
                                  ? Img.radioButtonOn
                                  : Img.radio
                              }
                              style={{ height: 20, width: 20 }}
                              resizeMode={"contain"}
                            />
                          </View>
                        )}
                      </TouchableOpacity>
                    );
                  })}
              </ScrollView>
              {selectionType == "multi" && (
                <AppButton
                  title={"Apply"}
                  onPress={() => {
                    setListVisible(false);
                  }}
                  style={{ marginBottom: 10 }}
                />
              )}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default BottomLister;
