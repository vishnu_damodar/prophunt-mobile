import React, {Fragment, useEffect, useState} from 'react';

import {
  ActivityIndicator,
  FlatList,
  Image,
  RefreshControl,
  View,
} from 'react-native';

import {AppColors} from '@app/config/ui/color';
import NoResult from './no-result';

export const InfiniteScroller = (props: {
  renderItem: (item: any, index: number) => JSX.Element;
  dataLoader: (page: number) => Promise<any[]>;
}) => {
  const [reachedLimit, setReachedLimit] = useState(false);

  const [data, setData] = useState<any>([]);

  const [page, setPage] = useState(1);

  const [requesting, setRequesting] = useState(false);
  useEffect(() => {
    loadData(1);
  }, []);

  const getNextPageData = () => {
    if (!reachedLimit && !requesting) {
      loadData(page + 1);
    }
  };
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    loadData(1).then(() => setRefreshing(false));
  }, []);

  const loadData = async (page: number) => {
    setRequesting(true);
    try {
      const response = await props.dataLoader(page);
      if (response?.length > 0) {
        const combinedData = [...data].concat(response);
        setData(combinedData);
        setPage(page);
      } else {
        setReachedLimit(true);
      }
    } catch (err) {
      setReachedLimit(true);
    } finally {
      setRequesting(false);
    }
  };

  const loader = () => {
    if (reachedLimit) {
      return <View></View>;
    }
    return (
      <View style={{alignItems: 'center'}}>
        <ActivityIndicator animating size="small"></ActivityIndicator>
      </View>
    );
  };

  return (
    <FlatList
      keyExtractor={(_item, index) => 'product-row' + index}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
      // ListHeaderComponent={totalCount}
      ListFooterComponent={loader}
      onEndReached={() => {
        getNextPageData();
      }}
      onEndReachedThreshold={0.5}
      style={{backgroundColor: AppColors.pageBackgroundColor}}
      data={data}
      contentContainerStyle={{paddingTop: 16}}
      renderItem={({item, index}: any) => {
        return props.renderItem(item, index);
      }}
      ListEmptyComponent={() => {
        if (!requesting && data.length == 0) {
          return <NoResult />;
        }
        return <View></View>;
      }}
    />
  );
};
