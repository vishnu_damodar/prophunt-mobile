import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { commonStyles } from "@app/components/common/common-style";
import CountryPicker from "react-native-country-picker-modal";
import { translate } from "@app/config/locale/i18n";

export interface ICountryCode {
  callingCode: string;
  code: any;
}
export interface IPhoneNumber {
  callingCode: string;
  code: any;
  phone: string;
}
export const PhoneWithCountryCode = (props: {
  phone?: string;
  defaultCode?: ICountryCode;
  trigger: boolean;
  onSubmit: (phoneDetails: IPhoneNumber) => void;
}) => {
  const phoneTextbox = useTextFormInput(props?.phone ?? "", [], undefined, 80);

  useEffect(() => {
    phoneTextbox.setValue(props.phone);
  }, [props.phone]);

  const [rendered, setRendered] = useState(false);

  const [code, setCode] = useState<ICountryCode>({
    callingCode: props?.defaultCode?.callingCode ?? "1242",
    code: props?.defaultCode?.code ?? "BS",
  });

  useEffect(() => {
    setRendered(true);
  }, []);

  const [error, setError] = useState("");

  useEffect(() => {
    if (rendered) {
      if (code?.callingCode && phoneTextbox.value.trim()) {
        props.onSubmit({
          phone: phoneTextbox.value.trim(),
          callingCode: code.callingCode,
          code: code.code,
        });
      } else {
        setError(translate('register.enterValidMobile'));
      }
    }
  }, [props.trigger]);

  return (
    <View>
      <View
        style={[
          { flexDirection: "row", alignItems: "center" },
          commonStyles.textBoxContainer,
        ]}
      >
        <View style={{ marginLeft: ViewportService.commonPadding }}>
          <CountryPicker
            withCallingCodeButton={true}
            withFilter={true}
            withCallingCode={true}
            onSelect={(code) => {
              let newCode: ICountryCode = {
                callingCode: code.callingCode[0],
                code: code.cca2,
              };
              setCode(newCode);
              setError("");
            }}
            countryCode={code.code}
          />
        </View>
        <View style={{ flex: 1 }}>
          {phoneTextbox.widget(
            [
              {
                height: 50,
              },
            ],
            [
              commonStyles.textBox,
              {
                // fontSize: ViewportService.normalFont,
                color: AppColors.grayTextColor,
              },
            ],
            { placeholder: translate('register.mobileNumber') },
            () => {
              setError("");
            },
            "phone-pad"
          )}
        </View>
      </View>
      <NormalText color={AppColors.dangerTextColor}>{error}</NormalText>
    </View>
  );
};
