import React from 'react';
import {Header, View, Button} from 'native-base';

import {commonStyles} from './common-style';

import {Image} from 'react-native';

import {AppColors} from '@app/config/ui/color';
import {NavigationService} from '@app/services/navigation-service';
import Img from '@app/assets/images';
import {ViewportService} from '@app/config/ui/viewport-service';
import {SubHeading} from './text/sub-heading';

export const NormalHeader = (props: any) => {
  const hideBackButton = props.hideBackButton ?? false;
  return (
    <Header
      style={commonStyles.primaryHeaderContainer}
      androidStatusBarColor={AppColors.headerColor}>
      <View
        style={{
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        {!hideBackButton && (
          <Button onPress={() => NavigationService.goBack()} transparent>
            <View style={{padding: ViewportService.commonPadding}}>
              <Image
                resizeMode="contain"
                style={{height: 20}}
                source={Img.back}
              />
            </View>
          </Button>
        )}
        <View
          style={{
            flex: 1,
            alignItems: 'flex-start',
            marginLeft: ViewportService.commonPadding,
          }}>
          <SubHeading color={AppColors.lightColor}>{props.title ?? ''}</SubHeading>
        </View>
      </View>
    </Header>
  );
};
