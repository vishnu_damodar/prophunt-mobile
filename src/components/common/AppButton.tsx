import {AppColors} from '@app/config/ui/color';
import {ViewportService} from '@app/config/ui/viewport-service';
import React from 'react';
import {
  TouchableOpacity,
  StyleProp,
  ViewStyle,
  StyleSheet,
  ActivityIndicator,
  TextStyle,
} from 'react-native';
import AppText from './text/AppText';

const styles = StyleSheet.create({
  appButton: {
    height: 50,
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
});

interface ButtonProps {
  title: string;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  color?: string;
  textColor?: string;
  fontSize?: number;
  disabled?: boolean;
  loading?: boolean;
  rightTitle?: string;
}

const AppButton: React.FunctionComponent<ButtonProps> = (props) => {
  return (
    <TouchableOpacity
      style={[
        styles.appButton,
        {
          backgroundColor: props.color || AppColors.primaryThemeColor,
        },
        props.style,
      ]}
      onPress={props.disabled ? undefined : props.onPress}
      disabled={props.disabled}>
      {props.loading && (
        <ActivityIndicator
          color={props.textColor || AppColors.lightColor}
          size="small"></ActivityIndicator>
      )}
      {!props.loading && (
        <AppText
          style={[
            {
              color: props.textColor || AppColors.lightColor,
              fontSize: props.fontSize ?? ViewportService.subHeading,
            },
            props.textStyle,
          ]}
          bold>
          {props.title}
        </AppText>
      )}
      {!props.loading && props.rightTitle && (
        <AppText
          style={[
            {
              color: props.textColor || AppColors.lightColor,
              fontSize: props.fontSize ?? ViewportService.subHeading,
            },
            props.textStyle,
          ]}
          bold>
          {props.rightTitle}
        </AppText>
      )}
    </TouchableOpacity>
  );
};

export default AppButton;
