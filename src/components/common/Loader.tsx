import {AppColors} from '@app/config/ui/color';
import React from 'react';
import {View} from 'react-native';
import {BarIndicator} from 'react-native-indicators';

interface Props {
  size?: number | 'small' | 'large';
  color?: string;
  transparent?: boolean;
  backgroundColor?: string;
}

const Loader = (props: Props) => {
  return (
    <View
      style={{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        zIndex: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:
          props.backgroundColor ||
          (props.transparent ? 'rgba(0, 0, 0,0.2)' : AppColors.lightColor),
      }}>
      <BarIndicator
        color={props.color || AppColors.primaryThemeColor}></BarIndicator>
    </View>
  );
};

export default Loader;
