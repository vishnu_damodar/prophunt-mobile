import {StyleSheet} from 'react-native';
import {AppColors} from '@app/config/ui/color';
import { ViewportService } from '@app/config/ui/viewport-service';
import { Fonts } from '@app/config/ui/font';

export const commonStyles = StyleSheet.create({
  footerContainer: {
    height: 65,
    backgroundColor: AppColors.lightColor,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderTopWidth: 0.5,
    borderColor: AppColors.borderColor,
  },
  footerItem: {
    backgroundColor: AppColors.lightColor,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },

  footerCenterIcon: {
    marginTop: 19,
    paddingTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: AppColors.borderColor,
    borderTopColor: AppColors.borderColor,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: AppColors.lightColor,
  },
  primaryHeaderContainer: {
    backgroundColor: AppColors.headerColor,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: ViewportService.commonPadding,
    height: 60,
    borderBottomColor: AppColors.borderColor,
    borderBottomWidth: 0.5,
  },
  secondaryHeaderContainer: {
    backgroundColor: AppColors.primaryThemeColor,
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
  },

  modalTransparentContainer: {
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  modalColoredContainer: {
    backgroundColor: AppColors.lightColor,
    flex: 1,
    padding: 20,
    borderRadius: 10,
  },

  fadedButton: {
    marginTop: 20,
    backgroundColor: AppColors.pageBackgroundColor,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 15,
    alignItems: 'center',
  },
  primaryButton: {
    marginTop: 20,
    backgroundColor: AppColors.primaryThemeColor,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 15,
    alignItems: 'center',
  },
  textBoxContainer: {
    borderWidth: 1,
    borderColor: AppColors.borderColor,
    height: 50,
    borderRadius: 3,
  },
  searchTextBoxContainer: {
    borderWidth: 0,
    borderColor: AppColors.borderColor,
    borderLeftWidth: 0,
    height: 50,
  },
  dropdown: {padding: 10, height: 50, color: AppColors.darkColor},
  textBox: {paddingHorizontal: 15, paddingVertical: 5, height: 50, flex: 1},

  subButton: {
    backgroundColor: AppColors.lightColor,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 25,
    borderColor: AppColors.primaryThemeLightColor,
    borderWidth: 1.5,
  },
  subCancelButton: {
    backgroundColor: AppColors.lightColor,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 25,
    borderColor: AppColors.pageBackgroundColor,
    borderWidth: 1.5,
  },
  textLeftAlign: {
    textAlign: 'left',
  },
  errorMessage: {
    color: AppColors.dangerTextColor,
    paddingVertical: 5,
    fontFamily: Fonts.Regular,
    fontSize: 13,
  },
  horizontalLine: {
    paddingVertical: 0.5,
    backgroundColor: AppColors.borderColor,
  },
  flexContainer: {
    flex: 1,
  },
});
