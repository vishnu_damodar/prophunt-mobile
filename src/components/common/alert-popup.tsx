import React, { useState } from "react";
import {
  View,
  ViewProps,
  Modal,
  Image,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "./text/normal-text";
import { SubHeading } from "./text/sub-heading";
import AppButton from "./AppButton";

export const AlertPopup: React.FunctionComponent<
  ViewProps & {
    title: string;
    message: string;
    positiveText: string;
    negativeText: string;
    onPositiveButtonClick?: any;
    onNegativeButtonClick?: any;
    loading?: boolean;
  }
> = (props) => {
  const [isVisible, setListVisible] = useState(true);
  return (
    <Modal
      transparent
      animationType="fade"
      visible={isVisible}
      onRequestClose={() => {
        setListVisible(false);
      }}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#00000050",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            alignItems: "center",
            margin: ViewportService.commonPadding,
            padding: ViewportService.commonPadding,
            backgroundColor: AppColors.lightColor,
            borderRadius: 3,
          }}
        >

          <SubHeading>{props?.title}</SubHeading>
          <Image
            style={{ marginVertical: ViewportService.commonPadding }}
            resizeMode={"contain"}
            source={Img.alertWarning}
          />
          <NormalText>{props?.message}</NormalText>
          <View style={{ flexDirection: "row", marginTop: ViewportService.commonPadding, }}>
            <AppButton
              title={props.positiveText}
              style={{
                marginEnd: 8
              }}
              onPress={() => {
                setListVisible(false)
                props.onPositiveButtonClick()
              }}
            />

            <AppButton
              title={props?.negativeText}
              textStyle={{ color: AppColors.headingTextColor }}
              style={{
                borderRadius: 3,
                marginStart: 8,
                backgroundColor: AppColors.lightColor,
                borderColor: AppColors.primaryThemeColor,
                borderWidth: 0.7,
              }}
              onPress={() => {
                setListVisible(false)
                props?.onNegativeButtonClick()
              }}
            />
          </View>

        </View>
      </View>
    </Modal>
  );
};

export default AlertPopup;
