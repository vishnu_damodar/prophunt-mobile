import React, {useState} from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {View} from 'react-native';
import {NormalText} from '@app/components/common/text/normal-text';
import Img from '@app/assets/images';
import {ViewportService} from '@app/config/ui/viewport-service';

export const RadioOptions = (props: {
  options: IRadioOption[];
  onChange: any;
  selectedKey: string;
}) => {
  const [selectedKey, setSelectedKey] = useState(props.selectedKey);
  return (
    <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
      {props.options.map((p, index) => {
        return (
          <TouchableOpacity
          
            key={'radio-option-' + index + p.key}
            onPress={() => {
              setSelectedKey(p.key);
              props.onChange(p.key);
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginRight: ViewportService.commonPadding,
                marginBottom: ViewportService.commonPadding,
                alignItems:'center'
              }}>
              <Image
              style={{height:20,width:20}}
              resizeMode={"contain"}
                source={
                  p.key == selectedKey ? Img.radioButtonOn : Img.radioButton
                }></Image>
              <View style={{marginLeft: 5}}>
                <NormalText>{p.value}</NormalText>
              </View>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export interface IRadioOption {
  key: string;
  value: string;
}
