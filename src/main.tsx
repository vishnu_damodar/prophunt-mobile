import React, { useEffect } from "react";
import 'react-native-gesture-handler';
import { AppContainer } from "./container/app/app-container";
import messaging from "@react-native-firebase/messaging";
var PushNotification = require("react-native-push-notification");
import DeviceInfo from "react-native-device-info";
import { useSelector, useDispatch } from "react-redux";
import { setI18nConfig } from "./config/locale/i18n";
import { IAppState } from "./store/state-props";
import { ILocale } from "./store/settings/settings-models";
// import { fetchCommonSettingsAction } from "./store/settings/settings-actions";
// import {AuthDetails, AuthActions} from './store/auth/auth-helper';
// import {
//   userInitAction,
//   setLoaderInLoginScreen,
// } from './store/auth/auth-actions';

import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";

export const Root = () => {
    const locale: ILocale = useSelector(
        (state: IAppState) => state.settings.locale
    );

    //   const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
    setI18nConfig(locale);
    // const dispatch = useDispatch();
    useEffect(() => {
        // NavigationService.navigate(AppRoutes.Landing)
        // dispatch(setLoaderInLoginScreen(false));
        // dispatch(fetchCommonSettingsAction());
        // if (auth.isAuthenticated) {
        //     dispatch(userInitAction(auth.cartId));
        // }
    }, []);
    PushNotification.localNotification({
        smallIcon: "notification",
        largeIcon: "ic_launcher"
    });
    const registerDevice = async () => {
        await messaging().registerDeviceForRemoteMessages();
    };
    const requestUserPermission = async () => {
        const settings = await messaging().requestPermission({
            alert: true,
            announcement: false,
            badge: true,
            carPlay: true,
            provisional: true,
            sound: true
        });

        if (settings) {
            // const token = await messaging().getToken()
        }
    };
    const checkPermission = async () => {
        const enabled = await messaging().hasPermission();
        if (enabled) {
            const token = await messaging().getToken();
        } else {
            requestUserPermission();
        }
    };

    useEffect(() => {
        // registerDevice();
        checkPermission();
    }, []);

    useEffect(() => {
        // Assume a message-notification contains a "type" property in the data payload of the screen to open

        messaging().onNotificationOpenedApp((remoteMessage) => {
            //'Notification caused app to open from background state:'
        });

        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then((remoteMessage) => {
                if (remoteMessage) {
                    // 'Notification caused app to open from quit state:',
                }
            });
        messaging().setBackgroundMessageHandler(async (remoteMessage) => {
            // 'Message handled in the background!'
        });
    }, []);

    // useEffect(() => {
    //   const unsubscribe = messaging().onMessage(async (remoteMessage) => {
    //     //  A new FCM message arrived!
    //   });

    //   return unsubscribe;
    // }, []);

    return <AppContainer />;
};
