export class AmountUtils {
  static getFormattedAmount(amount?: Number) {
    if (amount === undefined) {
      return 0;
    }
    return amount?.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$&,');
    // return (Math.round(amount * 100) / 100).toFixed(2);
  }
}
