import moment from "moment";

export class DateUtils {
    static generateDateMonth() {
        const currentYear = moment().year();
        moment.locale("en");
        const months = moment.monthsShort();
        const monthYearArray = [];
        const minYear = currentYear - 5;
        const maxYear = currentYear + 5;
        for (let i = minYear; i <= maxYear; i++) {
            for (const item of months) {
                monthYearArray.push(`${item} ${i}`);
            }
        }
        return monthYearArray;
    }

    static createMarkedDates(dates: Array<string>) {
        const dateObject: any = {};
        dates?.forEach((p) => {
            dateObject[p] = { selected: true };
        });
        return dateObject;
    }

    static displayFormatWithTime(date: any) {
        return moment(date).format("DD/MM/YYYY h:mm:ss");
    }

    static displayFormatWithOutTime(date: any, format?: string) {
        const customFormat: string = format ?? "DD/MM/YYYY";
        return moment(date).format(customFormat);
    }

    static toApplicateionFormat(date: any) {
        const customMoment = moment as any;
        return new Date(customMoment(date, "DD/MM/YYYY"));
    }

    static formatDate(date: Date | string, format?: string): string {
        return moment(date).format(format || "YYYY-MM-DD h:mm:ss");
    }

    static formatDisplayDate(date: Date): string {
        return moment(date).format("DD/MM/YYYY");
    }

    static formatDisplayDateForSubscription(date: Date): string {
        return moment(date).format("YYYY-MM-DD");
    }

    static formatSpecialDisplayDate(date: Date): string {
        const getDay = new Date(date).getDay();
        // let dayArray = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        return getDay.toString();
    }

    static parseDateString(dateString: any, format?: string): Date {
        const customFormat = format ?? "YYYY-MM-DD h:mm:ss";
        const customMoment = moment as any;
        return new Date(customMoment(dateString, customFormat));
    }

    static displayFormatWithTimeAMPM(date: any) {
        return moment(date).format("DD/MM/YYYY h:mm:ss A");
    }

    static isLessThanTommorrow(date: Date): boolean {
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        const selectedDate = moment(date).startOf("day");
        const momentTomorrow = moment(tomorrow).startOf("day");
        return selectedDate.diff(momentTomorrow, "days") < 0;
    }
}
