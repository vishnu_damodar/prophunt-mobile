import { Action } from "redux";

export interface IAction<T = string> extends Action<T> {}
export interface IActionWithPayload<T, U = string> extends IAction<U> {
    readonly payload: T;
}

interface IActionCreator<T, U = string> {
    readonly type: U;
    (payload: T): IActionWithPayload<T, U>;

    test(action: IAction<U>): action is IActionWithPayload<T, U>;
}

interface IActionCreatorVoid<T = string> {
    readonly type: string;
    (): IAction<T>;

    test(action: IAction<T>): action is IAction<T>;
}

export const actionCreator = <T>(type: string): IActionCreator<T> =>
    Object.assign((payload: T): any => ({ type, payload }), {
        type,
        test(action: IAction): action is IActionWithPayload<T> {
            return action.type === type;
        }
    });

export const actionCreatorVoid = (type: string): IActionCreatorVoid =>
    Object.assign((): any => ({ type }), {
        type,
        test(action: IAction): action is IAction {
            return action.type === type;
        }
    });
export const createActionWithPayload = <T, U = string>(
    type: U,
    payload: T
): IActionWithPayload<T, U> => {
    const actionData: IActionWithPayload<T, U> = {
        type,
        payload
    };
    return actionData;
};

export const createAction = <T = string>(type: T): IAction<T> => {
    const actionData: IAction<T> = {
        type
    };
    return actionData;
};
export function isNullOrEmpty(value: string) {
    return (
        value === undefined ||
        value == null ||
        value === "" ||
        value.length === 0
    );
}
