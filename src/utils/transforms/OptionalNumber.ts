export const OptionalNumberTransform = (value?: number) =>
  value === undefined ? value : Number(value);
