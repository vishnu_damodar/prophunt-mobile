export const StringToBooleanTransform = (trueValue: string = "true") => (
    value?: string
): boolean => {
    return value === trueValue;
};