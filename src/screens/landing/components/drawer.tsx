import React, { useState } from "react";
import {
  View,
  Image,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import Img from "@app/assets/images";
import { AppColors } from "@app/config/ui/color";
import { NormalText } from "@app/components/common/text/normal-text";
import LanguageSelector from "@app/components/language-selector";
import useAppSettings from "@app/hooks/useAppSettings";
import { LanguageList } from "@app/services/api/models";
import { useDispatch, useSelector } from "react-redux";
import { changeLanguage } from "@app/store/settings/settings-actions";
import { translate } from "@app/config/locale/i18n";
import { MenuItem } from "./menu-item";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { onLogoutAction } from "@app/store/auth/auth-actions";
import { AppRoutes } from "@app/route/route-keys";
import { NavigationService } from "@app/services/navigation-service";
export const DrawerScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dispatch = useDispatch();
  const [showLangList, setLangList] = useState(false);
  const { languageList } = useAppSettings();
  const onSelectLang = (data: LanguageList) => {
    dispatch(changeLanguage(data.languageID));
  };
  return (
    <SafeAreaView
      style={{
        flex: 0,
        backgroundColor: AppColors.primaryThemeColor,
      }}
    >
      <View>
        <ImageBackground
          source={Img.headerBg}
          style={{
            flexDirection: "row",
            backgroundColor: AppColors.primaryThemeColor,
            padding: ViewportService.commonPadding,
          }}
        >
          <View
            style={{
              backgroundColor: AppColors.lightColor,
              padding: ViewportService.commonPadding * 0.5,
              borderRadius: 30,
            }}
          >
            <Image
              resizeMode="cover"
              style={{ width: 30, height: 30 }}
              source={
                auth.userDetails?.imagePath &&
                auth.userDetails?.userProfilePicture
                  ? {
                      uri: `${auth.userDetails?.imagePath}${auth.userDetails?.userProfilePicture}`,
                    }
                  : Img.user
              }
            />
          </View>
          {auth.token && (
            <View
              style={{
                flex: 1,
                paddingLeft: ViewportService.commonPadding,
                justifyContent: "center",
              }}
            >
              <SubHeading bold color={AppColors.lightColor}>
                {auth.userDetails?.userFirstname ?? ""}{" "}
                {auth.userDetails?.userLastname ?? ""}
              </SubHeading>
              <NormalText color={AppColors.lightColor}>User</NormalText>
            </View>
          )}
        </ImageBackground>
        <LanguageSelector
          style={{ backgroundColor: AppColors.lightGray }}
          items={languageList}
          valueKey={"languageName"}
          onSelect={onSelectLang}
          defaultText={"Select Language"}
        />
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          showsVerticalScrollIndicator={false}
          style={{
            // flex: 1,
            backgroundColor: AppColors.lightColor,
          }}
        >
          <View style={{ flex: 1 }}>
            <MenuItem
              title={translate("drawer.buy")}
              onPress={() => {
                NavigationService.navigate(AppRoutes.Search, { purpose: "buy" });
              }}
            />
            <MenuItem
              title={translate("drawer.rent")}
              onPress={() => {
                NavigationService.navigate(AppRoutes.Search, { purpose: "rent" });
              }}
            />
            <MenuItem
              title={translate("drawer.developments")}
              onPress={() => {}}
            />
            <MenuItem title={translate("drawer.agents")} onPress={() => {}} />
            <MenuItem
              title={translate("drawer.developers")}
              onPress={() => {}}
            />
            {auth?.token && (
              <MenuItem
                title={translate("drawer.logout")}
                onPress={() => {
                  NavigationService.closeDrawer();
                  dispatch(onLogoutAction(AppRoutes.Landing));
                }}
              />
            )}
            {/* <MenuItem title={translate("drawer.resources")} onPress={() => {}} /> */}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
