import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { AppColors } from "@app/config/ui/color";
interface Props {
  image: any;
  title: string;
  onPress: () => void;
}
export const TileBox: React.FunctionComponent<Props> = (props) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: AppColors.lightColor,
        alignItems: "center",
        justifyContent: "center",
        margin: ViewportService.commonPadding * 0.5,
        padding: ViewportService.commonPadding,
        width: ViewportService.width / 2.7,
        borderRadius: 10,
      }}
      onPress={() => props.onPress()}
    >
      <Image
        resizeMode="contain"
        style={{ width: 100, height: 100 }}
        source={props.image}
      />
      <SubHeading style={{ textAlign: "center" }} bold>
        {props.title}
      </SubHeading>
    </TouchableOpacity>
  );
};
