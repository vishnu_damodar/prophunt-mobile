import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { NormalText } from "@app/components/common/text/normal-text";
interface Props {
  title: string;
  onPress: () => void;
}
export const MenuItem: React.FunctionComponent<Props> = (props) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: ViewportService.commonPadding,
        paddingVertical: ViewportService.commonPadding * 0.5,
        alignItems: "center",
      }}
      onPress={() => {props.onPress()}}
    >
      <View style={{ flex: 1 }}>
        <NormalText>
          <NormalText>{props.title}</NormalText>
        </NormalText>
      </View>
    </TouchableOpacity>
  );
};
