import React, { useState, useEffect, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigationParam } from "react-navigation-hooks";
import {
  View,
  Text,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  Platform,
  Dimensions,
  ImageBackground,
} from "react-native";
import AppText from "@app/components/common/text/AppText";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NavigationService } from "@app/services/navigation-service";
import Img from "@app/assets/images";
import { Fonts } from "@app/config/ui/font";
import { TileBox } from "./components/tile-box";
import { AppRoutes } from "@app/route/route-keys";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { HeaderType, RootLayout } from "@app/container/layout/root/root-layout";
export const LandingScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  return (
    <RootLayout showFooter={true} headerType={HeaderType.Custom}>
      <ScrollView
        keyboardShouldPersistTaps={"always"}
        showsVerticalScrollIndicator={false}
        style={{
          backgroundColor: AppColors.primaryThemeColor,
          flex: 1,
        }}
      >
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <ImageBackground
            source={Img.ellipseCornerLeft}
            imageStyle={{
              width: 100,
              height: 100,
              padding: ViewportService.commonPadding,
              paddingTop: ViewportService.commonPadding * 2,
              resizeMode: "contain",
            }}
            style={
              {
                // alignSelf: "flex-start",
                // width: 120,
                // height: 120,
                // padding: ViewportService.commonPadding,
                // paddingTop: ViewportService.commonPadding * 2,
              }
            }
            resizeMode={"contain"}
          >
            <TouchableOpacity
              style={{ padding: ViewportService.commonPadding }}
              onPress={() => NavigationService.openDrawer()}
            >
              <Image
                resizeMode="contain"
                style={{ height: 30 }}
                source={Img.menu}
              />
            </TouchableOpacity>
          </ImageBackground>
          <View
            style={{
              paddingTop: ViewportService.commonPadding * 3,
              paddingRight: ViewportService.commonPadding * 5,
            }}
          >
            <Image
              resizeMode="contain"
              style={{ width: 80, height: 80 }}
              source={Img.ellipse}
            />
          </View>
        </View>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <Image
            resizeMode="contain"
            style={{ width: 260, height: 67 }}
            source={Img.logo}
          />
        </View>
        <ImageBackground
          style={{
            flex: 1,
          }}
          source={Img.ellipse}
          imageStyle={{
            width: ViewportService.width / 1.5,
            height: ViewportService.hight / 2,
            resizeMode: "contain",
            position: "absolute",
            left: -60,
            right: undefined,
            bottom: undefined,
            top: 0,
          }}
        >
          <ImageBackground
            source={Img.ellipseHalf}
            style={{
              flex: 1,
              paddingVertical: ViewportService.commonPadding,
              paddingHorizontal: ViewportService.commonPadding * 3,
              alignItems: "center",
              justifyContent: "center",
            }}
            imageStyle={{
              width: ViewportService.width / 2,
              height: ViewportService.hight / 4,
              resizeMode: "contain",
              position: "absolute",
              left: undefined,
              bottom: undefined,
              right: -70,
              top: -20,
              padding: 0,
              margin: 0,
            }}
            // resizeMode={"cover"}
          >
            <AppText
              style={{
                textAlign: "center",
                color: AppColors.lightColor,
                fontFamily: Fonts.Bold,
                fontSize: 18,
                fontWeight: "600",
              }}
            >
              Welcome to Ganah's larget property marketplace
            </AppText>
          </ImageBackground>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              paddingHorizontal: ViewportService.commonPadding * 2,
            }}
          >
            <TileBox
              image={Img.propertyBuy}
              title={` \n Buy`}
              onPress={() => {
                NavigationService.navigate(AppRoutes.Search, { purpose: "buy" });
              }}
            />
            <TileBox
              image={Img.propertyrent}
              title={"\n Rent"}
              onPress={() => {
                NavigationService.navigate(AppRoutes.Search, { purpose: "rent" });
              }}
            />
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              paddingHorizontal: ViewportService.commonPadding * 2,
            }}
          >
            <TileBox
              image={Img.propertyPost}
              title={"Post \n Property"}
              onPress={() => {
                if (auth.token) {
                  NavigationService.navigate(AppRoutes.Login);
                } else {
                  NavigationService.navigate(AppRoutes.Login);
                }
              }}
            />
            <TileBox
              image={Img.handShake}
              title={"Local professional"}
              onPress={() => {}}
            />
          </View>
        </ImageBackground>
      </ScrollView>
    </RootLayout>
  );
};
