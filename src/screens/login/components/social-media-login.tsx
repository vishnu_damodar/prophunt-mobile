import React from 'react';
import { View, Text, Platform } from 'react-native';
import {
  FacebookLoginComponent,
  IFaceBookUser,
} from '@app/components/social-media-login/facebook-login-component';
import { GoogleLoginComponent, IGoogleUser } from '@app/components/social-media-login/google-login-component';
import {
  AppleLoginComponent,
  IAppleUser,
} from '@app/components/social-media-login/apple-login-component';
import { LoginComponent, ILoginModel } from './login-component';
import {
  LoginModel,
  GoogleLoginModel,
  FaceBookLoginModel,
  AppleLoginModel,
  AuthDetails,
} from '@app/store/auth/auth-models';
import { useDispatch, useSelector } from 'react-redux';
import { IAppState } from '@app/store/state-props';
import {
  onPressLoginAction,
  onPressGoogleLoginAction,
  onPressFaceBookLoginAction,
  onPressAppleLoginAction,
} from '@app/store/auth/auth-actions';
import DeviceInfo from 'react-native-device-info';
import { ISettings } from '@app/store/settings/settings-models';

export const SocialMediaLogin = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const settings: ISettings = useSelector((state: IAppState) => state.settings);
  const dispatch = useDispatch();
  const onGoogleLogin = (details: IGoogleUser) => {
    let googleLoginModel: GoogleLoginModel = new GoogleLoginModel();
    googleLoginModel.givenName = details.givenName;
    googleLoginModel.familyName = details.familyName;
    googleLoginModel.email = details.email;
    googleLoginModel.name = details.name;
    googleLoginModel.id = details.id;
    googleLoginModel.photo = details.photo;
    googleLoginModel.deviceType = Platform.OS;
    googleLoginModel.deviceUniqueId = DeviceInfo.getUniqueId();
    googleLoginModel.forceLogout = 'yes';
    googleLoginModel.deviceToken = settings.firebaseToken;
    dispatch(onPressGoogleLoginAction(googleLoginModel));
  };

  const onFaceBookLogin = (details: IFaceBookUser) => {
    let faceBookLoginModel: FaceBookLoginModel = new FaceBookLoginModel();
    faceBookLoginModel.firstName = details.first_name;
    faceBookLoginModel.lastName = details.last_name;
    faceBookLoginModel.email = details.email;
    faceBookLoginModel.deviceType = Platform.OS;
    faceBookLoginModel.deviceUniqueId = DeviceInfo.getUniqueId();
    faceBookLoginModel.forceLogout = 'yes';
    faceBookLoginModel.deviceToken = settings.firebaseToken;
    dispatch(onPressFaceBookLoginAction(faceBookLoginModel));
  };

  const onAppleLogin = (details: AppleLoginModel) => {
    details.deviceType = Platform.OS;
    details.deviceUniqueId = DeviceInfo.getUniqueId();
    details.forceLogout = 'yes';
    details.deviceToken = settings.firebaseToken;
    dispatch(onPressAppleLoginAction(details));
  };

  return (
    <View
      style={{
        paddingHorizontal: 20,
        marginVertical: 30,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
      }}>
      <View style={{ marginHorizontal: 10 }}>
        <FacebookLoginComponent
          onLogin={onFaceBookLogin}></FacebookLoginComponent>
      </View>
      <View style={{ marginHorizontal: 10 }}>
        <GoogleLoginComponent onLogin={onGoogleLogin}></GoogleLoginComponent>
      </View>
      {Platform.OS === 'ios' && (
        <View style={{ marginHorizontal: 10 }}>
          <AppleLoginComponent onLogin={onAppleLogin}></AppleLoginComponent>
        </View>
      )}
    </View>
  );
};
