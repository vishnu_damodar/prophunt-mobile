import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, TouchableWithoutFeedback, Keyboard } from "react-native";
// import { AuthDetails } from '@project/store/auth/auth-helper';
import DeviceInfo from "react-native-device-info";
import { ScrollView } from "react-native-gesture-handler";
import { styles } from "./style";
import { AppColors } from "@app/config/ui/color";
import { LogoHeader } from "@app/components/logo-header";
import {
  AuthDetails,
  ILoginModel,
  LoginModel,
} from "@app/store/auth/auth-models";
// import {loginAction} from '@app/store/auth/auth-actions';
import { RootLayout, HeaderType } from "@app/container/layout/root/root-layout";
import { ViewportService } from "@app/config/ui/viewport-service";
import Img from "@app/assets/images";
import AppButton from "@app/components/common/AppButton";
import { SocialMediaLogin } from "./components/social-media-login";
import { NormalText } from "@app/components/common/text/normal-text";
import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";
import { translate } from "@app/config/locale/i18n";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { validators } from "@app/components/common/widgets/form/validators";
import { commonStyles } from "@app/components/common/common-style";
import { onPressLoginAction } from "@app/store/auth/auth-actions";
import { IAppState } from "@app/store/state-props";
const hasNotch = DeviceInfo.hasNotch();
export const LoginScreen: React.FunctionComponent = (props: any) => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dispatch = useDispatch();
  const userName = useTextFormInput("", [
    validators.requiredValidator(translate("login.enterEmail")),
    validators.emailValidator(translate("register.enterValidEmail")),
  ]);
  const password = useTextFormInput("", [
    validators.requiredValidator(translate("login.enterPassword")),
  ]);
  const formItems = [userName, password];
  const isValid = () => {
    let isValid: boolean = true;
    formItems.forEach((p) => {
      if (!p.valid) {
        p.showErrorMessage();
        isValid = false;
      }
    });
    return isValid;
  };

  const onLogin = () => {
    if (isValid()) {
      let loginDetails: ILoginModel = {
        username: userName.value,
        password: password.value,
      };
      dispatch(onPressLoginAction(loginDetails));
    }
  };

  return (
    <RootLayout showFooter={true} headerType={HeaderType.Custom}>
      <LogoHeader />
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          style={{ flex: 1 }}
          contentContainerStyle={{
            paddingBottom: ViewportService.commonPadding * 2,
          }}
        >
          <SocialMediaLogin />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: ViewportService.commonPadding,
              marginBottom: ViewportService.commonPadding,
            }}
          >
            <View
              style={{
                flex: 1,
                borderBottomColor: AppColors.borderColor,
                borderBottomWidth: 0.7,
                marginHorizontal: ViewportService.commonPadding,
              }}
            ></View>
            <NormalText color={AppColors.grayTextColor}>
              {translate("common.or")}
            </NormalText>
            <View
              style={{
                flex: 1,
                borderBottomColor: AppColors.borderColor,
                borderBottomWidth: 0.7,
                marginHorizontal: ViewportService.commonPadding,
              }}
            ></View>
          </View>
          <View>
            <View style={[styles.loginBox]}>
              {/* <View style={{marginBottom: 20}}>
          <SubHeading>Email</SubHeading>
        </View> */}
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("login.email")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {userName.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  { placeholder: translate("login.email") }
                )}
              </View>
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("login.password")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {password.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  {
                    placeholder: translate("login.password"),
                    secureTextEntry: true,
                  }
                )}
              </View>
            </View>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
      <View style={{ flexDirection: "row" }}>
        <AppButton
          title={translate("common.submit")}
          disabled={auth.isLoading}
          loading={auth.isLoading}
          style={{
            flex: 1,
            borderRadius: 0,
            borderBottomColor: AppColors.borderColor,
            borderBottomWidth: 0.7,
          }}
          onPress={() => {
            onLogin();
          }}
        />
        <AppButton
          title={translate("login.haveAccount")}
          style={{
            flex: 1,
            borderRadius: 0,
            backgroundColor: AppColors.lightColor,
            borderTopColor: AppColors.borderColor,
            borderTopWidth: 0.7,
          }}
          textColor={AppColors.grayTextColor}
          textStyle={{ textDecorationLine: "underline" }}
          onPress={() => {
            NavigationService.navigate(AppRoutes.Register);
          }}
        />
      </View>
    </RootLayout>
  );
};
