import {StyleSheet, PixelRatio} from 'react-native';
import {Fonts} from '@app/config/ui/font';
import { ViewportService } from '@app/config/ui/viewport-service';


export const styles = StyleSheet.create({
  loginBox: {
    marginHorizontal: ViewportService.commonPadding,
    borderRadius: 10,
  },
  normalText: {fontFamily: Fonts.Regular, fontSize: 14},
});
