import React, { useEffect, useState, useRef } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  FlatListProps,
} from "react-native";
import Img from "@app/assets/images";
import AppButton from "@app/components/common/AppButton";
import { MainHeading } from "@app/components/common/text/main-heading";
import { NormalText } from "@app/components/common/text/normal-text";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { AppColors } from "@app/config/ui/color";
import { Fonts } from "@app/config/ui/font";
import { ViewportService } from "@app/config/ui/viewport-service";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";
export interface ISliderData {
  headingText: string;
  subHeadingText?: string;
  bodyText?: string;
  image: any;
}
interface Props {
  data?: ISliderData[];
}
export const SliderComponent: React.FunctionComponent<Props> = (props) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const items: ISliderData[] = [
    {
      headingText: "Dream House",
      subHeadingText: "Find your",
      bodyText:
        "If you sometimes feel like you are spinning your wheels and wandering aimlessly from property to property, we get it.",
      image: Img.sliderImageOne,
    },
    {
      headingText: "From the best",
      subHeadingText: "Choose",
      bodyText:
        "Buying a house or a flat is one of the biggest financial decisions you’ll make, so it’s important to get it right.",
      image: Img.sliderImageTwo,
    },
  ];
  const carouselRef = useRef<Carousel<ISliderData>>(null);
  const paginationRef = useRef<FlatListProps<any>>(null);
  const pagination = () => {
    return (
      <Pagination
        carouselRef={carouselRef}
        dotsLength={items.length}
        activeDotIndex={activeSlide}
        containerStyle={{}}
        dotStyle={{
          width: 20,
          height: 5,
          borderRadius: 5,
          // marginHorizontal: 8,
          backgroundColor: AppColors.primaryThemeDarkColor,
        }}
        inactiveDotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          // marginHorizontal: 8,
          padding: 5,
          backgroundColor: AppColors.primaryThemeDarkColor,
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
        tappableDots
      />
    );
  };

  const renderItem = ({ item, index }: any) => {
    return (
      <View style={{ flex: 1 }} key={`${index}_swipeitem`}>
        <View
          style={{
            flex: 1,
            borderBottomColor: AppColors.primaryThemeColor,
            borderBottomWidth: 0.7,
            backgroundColor: AppColors.primaryThemeExLightColor,
          }}
        >
          <Image
            resizeMode="contain"
            style={{
              width: ViewportService.width,
              height: ViewportService.width * 0.7,
              // tintColor: AppColors.primaryThemeColor,
            }}
            source={item.image}
          />
        </View>
        <View style={{ flex: 1, padding: ViewportService.commonPadding * 2 }}>
          <SubHeading
            color={AppColors.grayTextColor}
            // bold
            size={ViewportService.mainHeading + 12}
          >
            {item?.subHeadingText ?? ""}
          </SubHeading>
          <MainHeading
            color={AppColors.headingTextColor}
            size={ViewportService.mainHeading + 12}
            // style={{ fontWeight: "700" }}
          >
            {item?.headingText ?? ""}
          </MainHeading>
          <View
            style={{
              flex: 1,
              paddingVertical: ViewportService.commonPadding,
            }}
          >
            <NormalText
              color={AppColors.grayTextColor}
              fontSize={15}
              style={{ textAlign: "left", letterSpacing: 1 }}
            >
              {item?.bodyText ?? ""}
            </NormalText>
          </View>
        </View>
      </View>
    );
  };
  return (
    <View style={{ flex: 1, backgroundColor: AppColors.lightColor }}>
      <Carousel
        ref={carouselRef}
        data={items}
        renderItem={renderItem}
        onSnapToItem={(index) => setActiveSlide(index)}
        sliderWidth={ViewportService.width}
        itemWidth={ViewportService.width}
      />
      <View
        style={{
          position: "absolute",
          bottom: 0,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: "flex-start",
            justifyContent: "center",
          }}
        >
          {pagination()}
        </View>
        <View
          style={{
            flex: 1,
            alignItems: "flex-end",
            justifyContent: "center",
            paddingHorizontal: ViewportService.commonPadding,
          }}
        >
          <AppButton
            style={{ backgroundColor: AppColors.lightColor }}
            title={activeSlide === items.length - 1 ? "Finish" : "Next"}
            textColor={AppColors.primaryThemeDarkColor}
            onPress={() => {
              if (activeSlide === items.length - 1) {
                NavigationService.navigate(AppRoutes.Landing);
              } else {
                carouselRef?.current?.snapToNext();
              }
            }}
          />
        </View>
      </View>
    </View>
  );
};
