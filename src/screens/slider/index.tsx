import React, { useState, useEffect, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  View,
  Text,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  Platform,
  Dimensions,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import { SliderComponent } from "./components/slider";

export const SlidingScreen: React.FunctionComponent = () => {
  return (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: AppColors.primaryThemeColor,
        }}
      />
      <StatusBar barStyle="dark-content" />
      <SliderComponent />
    </Fragment>
  );
};
