import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  ActivityIndicator,
} from "react-native";
// import { AuthDetails } from '@project/store/auth/auth-helper';
import DeviceInfo from "react-native-device-info";
import { ScrollView } from "react-native-gesture-handler";
import { styles } from "../login/style";
import { AppColors } from "@app/config/ui/color";
import { LogoHeader } from "@app/components/logo-header";
import {
  AuthDetails,
  IRegisterModel,
} from "@app/store/auth/auth-models";
import { translate } from "@app/config/locale/i18n";
import { RootLayout, HeaderType } from "@app/container/layout/root/root-layout";
import { ViewportService } from "@app/config/ui/viewport-service";
import Img from "@app/assets/images";
import AppButton from "@app/components/common/AppButton";
import { SocialMediaLogin } from "../login/components/social-media-login";
import { NormalText } from "@app/components/common/text/normal-text";
import { NavigationService } from "@app/services/navigation-service";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { commonStyles } from "@app/components/common/common-style";
import { validators } from "@app/components/common/widgets/form/validators";
import ExpandingList from "@app/components/common/expanding-list";
import {
  PhoneWithCountryCode,
  IPhoneNumber,
} from "@app/components/common/phone-with-country-code";
import {
  MapComponent,
  PlaceDetails,
  UserLocation,
} from "@app/components/map-component";
import ErrorText from "@app/components/common/text/ErrorText";
import { setAuthLoader, signupAction } from "@app/store/auth/auth-actions";
import { IAppState } from "@app/store/state-props";
const hasNotch = DeviceInfo.hasNotch();
export const RegisterScreen: React.FunctionComponent = (props: any) => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const [userTypes, setUserTypes] = useState([
    { id: "1", type: "Individual" },
    // { id: "12", type: "LocalExperts" },
  ]);

  const dispatch = useDispatch();
  const [showError, setShowError] = useState(false);
  const [selectedUserType, setUserType] = useState(userTypes[0]);
  const [phone, setPhone] = useState("");
  const [callingCode, setCallingCode] = useState("");
  const [countryCode, setCountryCode] = useState("");
  const [selectedAddress, setSelectedAddress] = useState("");
  const [trigger, setTrigger] = useState(false);
  const [loader, setLoader] = useState(false);
  const companyName = useTextFormInput("", [
    validators.requiredValidator(translate("register.companyName")),
  ]);
  const firstName = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterFName")),
  ]);
  const lastName = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterLName")),
  ]);
  const address = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterAddress")),
  ]);
  const email = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterEmail")),
    validators.emailValidator(translate("register.enterValidEmail")),
  ]);
  const password = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterPassword")),
  ]);
  const confirmPassword = useTextFormInput("", [
    validators.requiredValidator(translate("register.enterPassword")),
  ]);
  const formItems = [firstName, lastName, email, password, confirmPassword];

  const onPickAddress = (place: PlaceDetails) => {
    let userLocation = new UserLocation();
    userLocation.lat = place.lat;
    userLocation.lng = place.lng;
    setSelectedAddress(place.formattedAddress);
    // dispatch(setLocationInFilter(place));
    // NavigationService.goBack();
  };
  useEffect(() => {
    dispatch(setAuthLoader(false));
  }, []);

  const verifyPhoneNumber = async (phoneDetails: IPhoneNumber) => {
    setPhone(phoneDetails.phone);
    setCallingCode(phoneDetails.callingCode);
    setCountryCode(phoneDetails.code);
    onRegister();
  };
  const isValid = () => {
    let isValid: boolean = true;
    setShowError(true);
    formItems.forEach((p) => {
      if (!p.valid) {
        p.showErrorMessage();
        isValid = false;
      }
    });
    return isValid && phone && password.value === confirmPassword.value;
  };

  const onRegister = () => {
    if (isValid()) {
      let regDetails: IRegisterModel = {
        userEmail: email.value,
        userPassword: password.value,
        // cPassword: confirmPassword.value,
        userTypeID: selectedUserType.id,
        userFirstName: firstName.value,
        userLastname: lastName.value,
        userPhone: `${callingCode}${phone}`,
        userStatus: "Active",
        userNewsletterSubscribeStatus: "Yes",
        stateSelectedType: "state",
        individualUserType: "",
        userCompanyName: "",
        countryId: "",
        countryCode: countryCode,
        googleStateName: "",
        googleCityName: "",
        propertyLatitude: "",
        propertyLongitude: "",
        googleLocality: "",
        userAddress1: "",
      };
      dispatch(signupAction(regDetails));
    }
  };

  const checkAvailability = async (email: string) => {
    setLoader(true);
    setLoader(false);
  };

  return (
    <RootLayout showFooter={false} headerType={HeaderType.Custom}>
      <LogoHeader />
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          contentContainerStyle={{
            paddingBottom: ViewportService.commonPadding * 2,
          }}
          style={{ flex: 1 }}
        >
          <SocialMediaLogin />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: ViewportService.commonPadding,
              marginBottom: ViewportService.commonPadding,
            }}
          >
            <View
              style={{
                flex: 1,
                borderBottomColor: AppColors.borderColor,
                borderBottomWidth: 0.7,
                marginHorizontal: ViewportService.commonPadding,
              }}
            ></View>
            <NormalText color={AppColors.grayTextColor}>
              {translate("common.or")}
            </NormalText>
            <View
              style={{
                flex: 1,
                borderBottomColor: AppColors.borderColor,
                borderBottomWidth: 0.7,
                marginHorizontal: ViewportService.commonPadding,
              }}
            ></View>
          </View>
          <View>
            <View style={[styles.loginBox]}>
              <View style={{ marginTop: 20 }}>
                <ExpandingList
                  selectedBoxStyle={{
                    backgroundColor: AppColors.lightColor,
                    borderColor: AppColors.borderColor,
                    borderWidth: 1,
                    borderRadius: 3,
                  }}
                  listBoxStyle={{
                    borderColor: AppColors.borderColor,
                    borderWidth: 1,
                  }}
                  items={userTypes}
                  valueKey={"type"}
                  defaultText={selectedUserType.type ?? ""}
                  onSelect={(uType) => {
                    setUserType(uType);
                  }}
                />
              </View>
              {/* {selectedUserType?.id == "12" && (
                <View style={{ marginTop: 20 }}>
                  <NormalText> {translate("register.companyName")}</NormalText>
                  <View style={{ marginVertical: 3 }}></View>
                  {firstName.widget(
                    [commonStyles.textBoxContainer],
                    [commonStyles.textBox, { color: AppColors.grayTextColor }],
                    { placeholder: translate("register.companyName") }
                  )}
                </View>
              )} */}
              <View style={{ marginTop: 20 }}>
                <NormalText> {translate("register.fname")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {firstName.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  { placeholder: translate("register.fname") }
                )}
              </View>
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("register.lname")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {lastName.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  { placeholder: translate("register.lname") }
                )}
              </View>
              {/* {
                <View>
                  <View style={{ marginTop: 20 }}>
                    <NormalText>{translate("register.location")}</NormalText>
                    <MapComponent
                      hideMap={false}
                      defaultLocation={new UserLocation()}
                      defaultValue={selectedAddress}
                      onSelect={onPickAddress}
                    />
                  </View>
                  <View style={{ marginTop: 20 }}>
                    <NormalText>{translate("register.address")}</NormalText>
                    <View style={{ marginVertical: 3 }}></View>
                    {address.widget(
                      [commonStyles.textBoxContainer],
                      [
                        commonStyles.textBox,
                        { color: AppColors.grayTextColor },
                      ],
                      { placeholder: translate("register.address") }
                    )}
                  </View>
                </View>
              } */}
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("register.email")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {email.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  { placeholder: translate("register.email") },
                  undefined,
                  undefined,
                  undefined,
                  () => {
                    checkAvailability(email.value);
                  }
                )}
                {loader && (
                  <View style={{ position: "absolute", right: 16, top: 10 }}>
                    <ActivityIndicator
                      size="small"
                      color={AppColors.primaryThemeColor}
                    />
                  </View>
                )}
              </View>
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("register.mobileNumber")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                <PhoneWithCountryCode
                  trigger={trigger}
                  onSubmit={(phoneDetails: IPhoneNumber) => {
                    verifyPhoneNumber(phoneDetails);
                  }}
                />
              </View>
              <View style={{ marginTop: 20 }}>
                <NormalText>{translate("register.password")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {password.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  {
                    placeholder: translate("register.password"),
                    secureTextEntry: true,
                  }
                )}
              </View>

              <View style={{ marginTop: 20 }}>
                <NormalText> {translate("register.cPassword")}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                {confirmPassword.widget(
                  [commonStyles.textBoxContainer],
                  [commonStyles.textBox, { color: AppColors.grayTextColor }],
                  {
                    placeholder: translate("register.password"),
                    secureTextEntry: true,
                  },
                  (value: any) => {
                    setShowError(false);
                  }
                )}
                {showError &&
                  confirmPassword.value !== "" &&
                  confirmPassword.value !== password.value && (
                    <ErrorText style={[commonStyles.errorMessage]}>
                      {translate("register.pswdMismatch")}
                    </ErrorText>
                  )}
              </View>
            </View>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
      <View style={{ flexDirection: "row" }}>
        <AppButton
          title={translate("common.submit")}
          loading={auth.isLoading}
          disabled={auth.isLoading}
          style={{
            flex: 1,
            borderRadius: 0,
            borderBottomColor: AppColors.borderColor,
            borderBottomWidth: 0.7,
          }}
          onPress={() => {
            if (phone) {
              setTrigger(!trigger);
            } else {
              setTrigger(!trigger);
              onRegister();
            }
          }}
        />
        <AppButton
          title={translate("register.isRegistered")}
          style={{
            flex: 1,
            borderRadius: 0,
            backgroundColor: AppColors.lightColor,
            borderTopColor: AppColors.borderColor,
            borderTopWidth: 0.7,
          }}
          textColor={AppColors.grayTextColor}
          textStyle={{ textDecorationLine: "underline" }}
          onPress={() => {
            NavigationService.goBack();
          }}
        />
      </View>
    </RootLayout>
  );
};
