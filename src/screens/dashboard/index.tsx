import React, { useState } from "react";
import { useSelector } from "react-redux";
import {
  ScrollView, View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { HeaderType, RootLayout } from "@app/container/layout/root/root-layout";
import { translate } from "@app/config/locale/i18n";
import { DashboardTabsComponent } from "./components/dashboard-tabs-component";
import { ShortlistScreen } from "./components/shortlist/shortlist-screen";
import { SavedSearchesScreen } from "./components/saved-search/saved-searches-screen";
import { ProfileScreen } from "./components/edit-profile";
import { MyPropertiesScreen } from "./components/my-properties";
import { EnquiriesScreen } from "./components/enquiries";
export const DashboardScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const [activeOption, setOption] = useState(1);
  const [title, setTitle] = useState(translate('dashboard.editProfile'));

  const onTabClick = (active: any) => {
    setOption(active);
    active === 1
      ? setTitle(translate('dashboard.editProfile'))
      : active === 2
        ? setTitle(translate('dashboard.properties'))
        : active === 3
          ? setTitle(translate('dashboard.publishProperty'))
          : active === 4
            ? setTitle(translate('dashboard.shortlist'))
            : active === 5
              ? setTitle(translate('dashboard.savedSearches'))
              : active === 6
                ? setTitle(translate('dashboard.publishedRequiements'))
                : active === 7
                  ? setTitle(translate('dashboard.queries'))
                  : active === 8
                    ? setTitle(translate('dashboard.settings'))
                    : '';
  };

  return (
    <RootLayout showFooter={true} headerType={HeaderType.Normal} title={title}>
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <View>
          <DashboardTabsComponent onPress={(option: number) => {
            onTabClick(option);
          }} />
        </View>
        {activeOption == 1 && (
          <ProfileScreen />
        )}
        {activeOption == 2 && (
          <MyPropertiesScreen />
        )}
        {activeOption == 4 && (
          <ShortlistScreen />
        )}

        {activeOption == 5 && (
          <SavedSearchesScreen />
        )}
        {activeOption == 7 && (
          <EnquiriesScreen />
        )}
      </ScrollView>
    </RootLayout>
  );
};
