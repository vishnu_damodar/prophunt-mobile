import { translate } from '@app/config/locale/i18n';
import { AppColors } from '@app/config/ui/color';
import { Fonts } from '@app/config/ui/font';
import { ViewportService } from '@app/config/ui/viewport-service';
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
export const DashboardTabsComponent = (props: {
  onPress: any;
}) => {
  const [activeOption, setOption] = useState(1);

  const onSelect = (option: number) => {
    setOption(option);
    props.onPress(option);
  };

  return (
    <ScrollView horizontal={true}>
      <TouchableOpacity
        onPress={() => onSelect(1)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 1 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.editProfile')}`}
          </Text>
          {activeOption === 1 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(2)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 2 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.properties')}`}
          </Text>
          {activeOption === 2 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(3)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 3 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.publishProperty')}`}
          </Text>
          {activeOption === 3 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(4)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 4 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.shortlist')}`}
          </Text>
          {activeOption === 4 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(5)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 5 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.savedSearches')}`}
          </Text>
          {activeOption === 5 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(6)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 6 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.publishedRequiements')}`}
          </Text>
          {activeOption === 6 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(7)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 7 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.queries')}`}
          </Text>
          {activeOption === 7 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => onSelect(8)}
        style={[styles.tabItem]}>
        <View>
          <Text
            style={[
              styles.tabItemText,
              activeOption === 8 ? styles.selectedTextStyle : {},
            ]}>
            {`${translate('dashboard.settings')}`}
          </Text>
          {activeOption === 8 ? (
            <View style={styles.selectedViewStyle} />
          ) : null}
        </View>
      </TouchableOpacity>
    </ScrollView>
  );
};

export const styles = StyleSheet.create({
  tabItem: {
    marginHorizontal: ViewportService.commonPadding,
    paddingTop: ViewportService.commonPadding,
  },
  tabItemText: {
    color: '#818181',
    paddingBottom: ViewportService.normalFont,
    fontSize: ViewportService. commonPadding,
    fontFamily: Fonts.Regular,
    alignSelf: 'center',
  },
  selectedTextStyle: {
    color: '#010101',
    fontFamily: Fonts.Regular,
    alignSelf: 'center',
  },
  selectedViewStyle: {
    paddingVertical: 2,
    backgroundColor: AppColors.primaryThemeColor,
  },
});
