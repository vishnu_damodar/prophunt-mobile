import React, { useState } from "react";
import { View, Image, TouchableOpacity, Alert, ColorValue } from "react-native";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { translate } from "@app/config/locale/i18n";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { DateUtils } from "@app/utils/date-utils";
import { ViewStats } from "./view-stats";
import { PropertyOptions } from "./property-options";
import useProperty from "@app/hooks/useProperty";
import { PauseResumeDeleteParamModel } from "@app/store/dashboard/dashboard-models";
import { AuthDetails } from "@app/store/auth/auth-models";
import { useSelector } from "react-redux";
import { IAppState } from "@app/store/state-props";
import AlertPopup from "@app/components/common/alert-popup";

export const MyPropertyItem: React.FunctionComponent<any> = (props) => {
  const [showStatus, setShowStatus] = useState(false);
  const [showAlertPopup, setAlertPopup] = useState(false);
  const [isImageError, setImageError] = useState(false);
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const { deleteProperty } = useProperty();

  const onViewStatsClick = () => {
    setShowStatus(!showStatus)
  };

  const getStatus = (propertyStatus: string): String => {
    const value = (propertyStatus === undefined || propertyStatus == '') ? ''
      : (propertyStatus === 'Active') ? translate('dashboard.approved')
        : (propertyStatus === 'Request') ? translate('dashboard.waitingForApproval')
          : (propertyStatus === 'Inactive') ? translate('dashboard.inactive')
            : '';
    return value
  };

  const getStatusColor = (propertyStatus: string): ColorValue => {
    const value = (propertyStatus === undefined || propertyStatus == '') ? ''
      : (propertyStatus === 'Active') ? AppColors.primaryThemeColor
        : (propertyStatus === 'Request') ? AppColors.yellow
          : (propertyStatus === 'Inactive') ? AppColors.dangerTextColor
            : '';
    return value
  };

  const items: Array<IStatsItem> = [
    {
      title: translate('dashboard.lastDaysViewCount'),
      value: props.data?.reslastDayViews,
    },
    {
      title: translate('dashboard.totalViews'),
      value: props.data?.restotalViews,
    },
    {
      title: translate('dashboard.listViews'),
      value: props.data?.restotalListViews,
    },
    {
      title: translate('dashboard.detailedViews'),
      value: props.data?.restotalDetailViews,
    },
    {
      title: translate('dashboard.favoriteAdded'),
      value: props.data?.restotalfavor,
    },
    {
      title: translate('dashboard.enquiryCount'),
      value: props.data?.restotalEmailViews,
    },
  ];

  const options: Array<IOption> = [
    {
      id: 1,
      title: translate('dashboard.markAsSoldOut'),
      type: 'markAsSoldOut',
      show: (props.data.propertySoldOutStatus === 'No') ? true : false
    },
    {
      id: 2,
      title: translate('dashboard.preview'),
      type: 'preview',
      show: (props.data.propertyStatus === 'Active') ? true : false
    },
    {
      id: 3,
      title: translate('dashboard.edit'),
      type: 'edit',
      show: true
    },
    {
      id: 4,
      title: translate('common.delete'),
      type: 'delete',
      show: true
    },
    {
      id: 5,
      title: translate('dashboard.upgradePlan'),
      type: 'upgrade',
      show: true
    },
  ];

  const onItemClick = (option: IOption) => {

    if (option.type = 'delete') {
      setShowStatus(false)
      setAlertPopup(true)
    }
  };

  const onAlertPopupCancel = () => {
    setAlertPopup(false)
  };

  const onDeleteClick = () => {
    setAlertPopup(false)
    const params = new PauseResumeDeleteParamModel();
    params.id = props?.data?.propertyID
    params.userID = auth.token
    params.status = 'Deleted'
    deleteProperty(params)
   
  }

  return (
    <TouchableOpacity
      style={{
        backgroundColor: AppColors.lightColor,
        marginBottom: ViewportService.commonPadding,
      }}>

      <PropertyOptions
        onItemClick={(item: IOption) => {
          onItemClick(item)
        }}
        items={options}
        titleKey={"title"}
        valueKey={"type"} />
      <View style={{ paddingHorizontal: ViewportService.commonPadding, paddingBottom: ViewportService.commonPadding, flex: 1 }}>
        <View style={{ flexDirection: "row" }}>
          <Image onError={() => setImageError(true)} source={isImageError ? Img.placeholder : { uri: props?.data?.propertyCoverImage ? props?.imagePath + props?.data?.propertyCoverImage : Img.placeholder }} style={{ height: 70, width: 80, marginBottom: ViewportService.commonPadding, marginRight: ViewportService.commonPadding, borderWidth: 1, borderColor: AppColors.borderColor, borderRadius: 3, backgroundColor: AppColors.pageBackgroundColor, resizeMode: "contain" }} />
          <View style={{ flex: 1 }}>
            <SubHeading style={{ flexWrap: 'wrap' }}>{props.data.propertyName}</SubHeading>
            <NormalText style={{ color: AppColors.grayTextColor }}>{`${props.data?.propertyLocality}, ${props.data?.stateName}`}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{`${translate('dashboard.postedOn')} ${DateUtils.formatDate(props.data.propertyAddedDate, "DD MMM, yyyy")}`}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{`${translate('dashboard.plan')}: ${props.data.planTitle}`}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{`${props.data?.currencySymbolLeft} ${props.data.propertyPrice}`}</NormalText>
          </View>
        </View>
        <View style={{ marginTop: ViewportService.smallPadding, flexDirection: "row" }}>
          <View style={{ flexDirection: "row", flex: 1 }}>
            <View style={{
              width: 15,
              height: 15,
              borderRadius: 15 / 2,
              backgroundColor: getStatusColor(props.data.propertyStatus),
              marginEnd: ViewportService.smallPadding
            }}></View>
            <NormalText style={{ color: AppColors.grayTextColor, }}>{getStatus(props.data.propertyStatus)}</NormalText>
          </View>
          <TouchableOpacity style={{ alignSelf: "flex-end", flexDirection: "row", alignItems: "center" }} onPress={() => onViewStatsClick()}>
            <NormalText style={{ color: AppColors.primaryThemeColor, marginEnd: 5 }}>{translate('dashboard.viewStats')}</NormalText>
            <Image source={showStatus ? Img.arrowUpOutline : Img.arrowDownOutline}></Image>
          </TouchableOpacity>
        </View>

        {showStatus && (
          <ViewStats
            items={items}
            titleKey={"title"}
            valueKey={"value"} />
        )}
      </View>

      {showAlertPopup && (
        <AlertPopup
          title={translate('dashboard.alert')}
          message={translate('dashboard.alertPropertyDelete')}
          positiveText={translate('common.ok')}
          negativeText={translate('common.cancel')}
          onNegativeButtonClick={() => onAlertPopupCancel()}
          onPositiveButtonClick={() => onDeleteClick()} />
      )}
    </TouchableOpacity>
  );
};

export interface IStatsItem {
  title: string;
  value?: number;
}

export interface IOption {
  id: number;
  title: string;
  type?: string;
  show?: boolean;
}
