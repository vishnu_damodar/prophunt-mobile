import React from "react";
import { View, ViewProps, ViewStyle } from "react-native";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";

export const ViewStats: React.FunctionComponent<
  ViewProps & {
    items: any[];
    valueKey: string;
    titleKey: string;
    containerStyle?: ViewStyle;
  }
> = (props) => {

  return (
    <View style={{ flexDirection: "row", flexWrap: 'wrap', }}>
      {props.items.map((item, index) => {
        return (
          <View key={index} style={{
            borderColor: AppColors.borderColor, borderRadius: 3, borderWidth: 1,
            padding: ViewportService.smallPadding, marginEnd: ViewportService.smallPadding, marginTop: ViewportService.smallPadding
          }}>
            <NormalText>
              {`${item[props.valueKey]} `}
              <NormalText style={{ color: AppColors.grayTextColor }}>{`${item[props.titleKey]}`}</NormalText>
            </NormalText>
          </View>
        );
      })}
    </View>

  );
};
