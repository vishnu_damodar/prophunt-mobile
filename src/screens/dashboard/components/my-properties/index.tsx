import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  FlatList,
  View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { MyPropertyItem } from "./my-properties-item";
import { DashboardDetails, ListParamModel, PauseResumeDeleteParamModel } from "@app/store/dashboard/dashboard-models";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import Loader from "@app/components/common/Loader";
import NoResult from "@app/components/common/no-result";
import useDashboard from "@app/hooks/useDashboard";
export const MyPropertiesScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dashboardDetails: DashboardDetails = useSelector((state: IAppState) => state.dashboard);

  const { getMyPropertiesList, isLoading } = useDashboard();

  useEffect(() => {
    getList(0)
  }, []);

  const getList = (page: number) => {
    const params: ListParamModel = new ListParamModel();
    params.userID = auth.token;
    params.page = page;
    getMyPropertiesList(params)
  };

  const onPauseResumeClick = (id: string, status: string) => {
    const params: PauseResumeDeleteParamModel = new PauseResumeDeleteParamModel();
    params.userID = auth.token;
    params.id = id;
    params.status = status;

  };

  const onDeleteClick = (id: string) => {
    const params: PauseResumeDeleteParamModel = new PauseResumeDeleteParamModel();
    params.userID = auth.token;
    params.id = id;
  };

  return (
    <View style={{ flex: 1 }}>
      {!isLoading && ((dashboardDetails?.myPropertiesResult?.myProperties?.length ?? 0) === 0) && (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <NoResult />
        </View>
      )}

      {isLoading && (
        <View style={{ flex: 1 }}>
          <Loader size="small"></Loader>
        </View>
      )}
      {!isLoading && dashboardDetails?.myPropertiesResult?.myProperties?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={dashboardDetails?.myPropertiesResult?.myProperties ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.propertyID}`}
          renderItem={({ item }) => {
            return (
              <MyPropertyItem key={`propertyItem_${item.propertyID}`}
                data={item}
                imagePath={dashboardDetails?.myPropertiesResult?.imgPath}
                onPress={(id: string) => {
                }}
                onDeleteClick={(id: string) => {
                  onDeleteClick(id)
                }}
                onPauseResumeClick={(id: string, status: string) => {
                  onPauseResumeClick(id, status);
                }}
              />
            );
          }}
        />
      )}
    </View>
  );
};
