import React, { useState } from "react";
import { Image, TouchableOpacity, View, ViewProps, ViewStyle } from "react-native";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import Img from "@app/assets/images";

export const PropertyOptions: React.FunctionComponent<
  ViewProps & {
    items: any[];
    valueKey: string;
    titleKey: string;
    containerStyle?: ViewStyle;
    onItemClick?: any
  }
> = (props) => {

  const [showOptions, setShowOptions] = useState(false);

  const onOptionsClick = () => {
    setShowOptions(!showOptions)
  };

  return (
    <View style={{ flexWrap: "wrap", alignSelf: "flex-end" }}>
      <TouchableOpacity style={{ padding: ViewportService.commonPadding }} onPress={() => onOptionsClick()}>
        <Image style={{ alignSelf: "flex-end", resizeMode: "contain" }}
          source={Img.menuDots}></Image>
      </TouchableOpacity>
      {showOptions && (
        <View style={{
          backgroundColor: AppColors.lightColor,
          borderColor: AppColors.borderColor, borderRadius: 3, borderWidth: 1, 
          padding: 5,
          position: 'absolute',
          right: ViewportService.commonPadding,
          top: 24,
          zIndex:100
        }}>
          {props.items.map((item, index) => {
            if (!item.show) {
              return null
            }
            return (
              <TouchableOpacity key={item.id} onPress={() => props.onItemClick(item)} style={{ padding: 3 }}>
                <NormalText style={{ color: AppColors.grayTextColor }}>{`${item[props.titleKey]}`}</NormalText>
              </TouchableOpacity>
            );
          })}
        </View>
      )}
    </View>

  );
};
