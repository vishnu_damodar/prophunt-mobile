import { translate } from '@app/config/locale/i18n';
import { AppColors } from '@app/config/ui/color';
import { Fonts } from '@app/config/ui/font';
import { ViewportService } from '@app/config/ui/viewport-service';
import { ShortlistedCountResult } from '@app/services/api/models/shortlist';
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
export const ShortlistTabsComponent = (props: {
  onPress: any;
  countResult: ShortlistedCountResult
}) => {
  const [activeOption, setOption] = useState(1);

  const onSelect = (option: number) => {
    setOption(option);
    props.onPress(option);
  };

  return (
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        <TouchableOpacity
          onPress={() => onSelect(1)}
          style={[styles.tabItem]}>
          <View>
            <Text
              style={[
                styles.tabItemText,
                activeOption === 1 ? styles.selectedTextStyle : {},
              ]}>
              {`${translate('dashboard.properties')} (${props.countResult.favPropertyCount ?? 0})`}
            </Text>
            {activeOption === 1 ? (
              <View style={styles.selectedViewStyle} />
            ) : null}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onSelect(2)}
          style={[styles.tabItem]}>
          <View>
            <Text
              style={[
                styles.tabItemText,
                activeOption === 2 ? styles.selectedTextStyle : {},
              ]}>
              {`${translate('dashboard.projects')} (${props.countResult.favProjectCount ?? 0})`}
            </Text>
            {activeOption === 2 ? (
              <View style={styles.selectedViewStyle} />
            ) : null}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onSelect(3)}
          style={[styles.tabItem]}>
          <View>
            <Text
              style={[
                styles.tabItemText,
                activeOption === 3 ? styles.selectedTextStyle : {},
              ]}>
              {`${translate('dashboard.agents')} (${props.countResult.favAgentCount ?? 0})`}
            </Text>
            {activeOption === 3 ? (
              <View style={styles.selectedViewStyle} />
            ) : null}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onSelect(4)}
          style={[styles.tabItem]}>
          <View>
            <Text
              style={[
                styles.tabItemText,
                activeOption === 4 ? styles.selectedTextStyle : {},
              ]}>
              {`${translate('dashboard.developers')} (${props.countResult.favBuilderCount ?? 0})`}
            </Text>
            {activeOption === 4 ? (
              <View style={styles.selectedViewStyle} />
            ) : null}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onSelect(5)}
          style={[styles.tabItem]}>
          <View>
            <Text
              style={[
                styles.tabItemText,
                activeOption === 5 ? styles.selectedTextStyle : {},
              ]}>
              {`${translate('dashboard.localExperts')} (${props.countResult.favExpertCount ?? 0})`}
            </Text>
            {activeOption === 5 ? (
              <View style={styles.selectedViewStyle} />
            ) : null}
          </View>
        </TouchableOpacity>
      </View>
  );
};

export const styles = StyleSheet.create({
  tabItem: {
    marginHorizontal: ViewportService.commonPadding,
    paddingTop: ViewportService.commonPadding,
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  tabItemText: {
    color: '#818181',
    paddingBottom: ViewportService.normalFont,
    fontSize: ViewportService.commonPadding,
    fontFamily: Fonts.Regular,
    alignSelf: 'center',
  },
  selectedTextStyle: {
    color: '#010101',
    fontFamily: Fonts.Regular,
    alignSelf: 'center',
  },
  selectedViewStyle: {
    paddingVertical: 2,
    backgroundColor: AppColors.primaryThemeColor,
  },
});
