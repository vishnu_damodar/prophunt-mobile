import React from "react";
import { View, Image, TouchableOpacity, Dimensions } from "react-native";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import { MainHeading } from "@app/components/common/text/main-heading";
import Img from "@app/assets/images";
import { IAppState } from "@app/store/state-props";
import { useSelector } from "react-redux";
import { SettingsState } from "@app/store/settings/settings-models";
import { AuthDetails } from "@app/store/auth/auth-models";
import { SubHeading } from "@app/components/common/text/sub-heading";

export const ShortlistItem: React.FunctionComponent<any> = (props) => {
  const displayImageWidth =
    Math.round(Dimensions.get('window').width) - ViewportService.commonPadding;
  const settings: SettingsState = useSelector((state: IAppState) => state.settings);
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);

  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: AppColors.lightColor,
        marginBottom: ViewportService.commonPadding,
        alignItems: "center",
      }}>
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Image
            resizeMode={'cover'}
            style={{ height: 210, width: displayImageWidth }}
            source={
              props.image !== ''
                ? { uri: props.image }
                : Img.logo
            }
          />
          <TouchableOpacity style={{
            position: 'absolute',
            right: 0,
            top: 0,
          }} onPress={() => props.onPress(props.id)}>
            <Image
              source={Img.delete}
              style={{
                tintColor: AppColors.lightColor,
                marginTop: ViewportService.smallFont,
                marginRight: ViewportService.smallFont
              }}></Image>
          </TouchableOpacity>

          <MainHeading style={{
            color: AppColors.lightColor,
            position: 'absolute',
            left: 0,
            bottom: 0,
            margin: ViewportService.commonPadding
          }}>{props.price ? `${settings.currency} ${props.price}` : ''}</MainHeading>
        </View>

        <View style={{ padding: ViewportService.commonPadding, }}>
          <SubHeading>{props.name}</SubHeading>
          <NormalText>{props.location}</NormalText>
        </View>

      </View>
    </TouchableOpacity>
  );
};
