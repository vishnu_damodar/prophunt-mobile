import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  FlatList, View,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import useShortlist from "@app/hooks/useShortlist";
import { ShortlistDetails, UnFavoriteParamModel } from "@app/store/shortlist/shortlist-models";
import { translate } from "@app/config/locale/i18n";
import { ShortlistTabsComponent as ShortlistTabsComponent } from "./shortlist-tabs-component";
import NoResult from "@app/components/common/no-result";
import Loader from "@app/components/common/Loader";
import { commonStyles } from "@app/components/common/common-style";
import { ShortlistItem } from "./shortlist-item";
export const ShortlistScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const shortlistDetails: ShortlistDetails = useSelector((state: IAppState) => state.shortlist);
  const { getShortlistedPropertyList, getShortlistedCount, getShortlistedProjectList,
    getShortlistedAgentList, getShortlistedDeveloperList, getShortlistedExpertList,
    unFavoriteProperty, unFavoriteProject, unFavoriteUser, isLoading } = useShortlist();
  const [activeOption, setOption] = useState(1);

  useEffect(() => {
    getShortlistedCount(auth.token)
    getShortlistedPropertyList(auth.token)
  }, []);

  const onTabClick = (active: any) => {
    active === 1
      ? getShortlistedPropertyList(auth.token)
      : active === 2
        ? getShortlistedProjectList(auth.token)
        : active === 3
          ? getShortlistedAgentList(auth.token)
          : active === 4
            ? getShortlistedDeveloperList(auth.token)
            : active === 5
              ? getShortlistedExpertList(auth.token)
              : '';
  };

  const onUnFavoriteClick = (id: string, option: number) => {
    const params: UnFavoriteParamModel = new UnFavoriteParamModel();
    params.userID = auth.token;
    params.id = id;
    params.option = option;
    (option == 1) ?
      unFavoriteProperty(params)
      : option == 2 ?
        unFavoriteProject(params)
        :
        unFavoriteUser(params)
  };

  return (
    <View style={{ flex: 1 }}>
      <ShortlistTabsComponent
        countResult={shortlistDetails.countResult}
        onPress={(option: number) => {
          setOption(option);
          onTabClick(option);
        }} />
      <View style={commonStyles.horizontalLine} />


      {!isLoading &&
        (activeOption === 1
          ? (shortlistDetails?.propertyResult?.propertyList?.length ?? 0) === 0
          : activeOption === 2
            ? (shortlistDetails?.projectResult?.projectList?.length ?? 0) === 0
            : activeOption === 3
              ? (shortlistDetails?.agentResult?.agentList?.length ?? 0) === 0
              : activeOption === 4
                ? (shortlistDetails?.developerResult?.developerList?.length ?? 0) === 0
                : activeOption === 5
                  ? (shortlistDetails?.expertResult?.expertList?.length ?? 0) === 0
                  : true) && (
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <NoResult />
          </View>
        )}

      {isLoading && (
        <View style={{ flex: 1 }}>
          <Loader size="small"></Loader>
        </View>
      )}
      {activeOption === 1 && !isLoading && shortlistDetails?.propertyResult?.propertyList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={shortlistDetails?.propertyResult?.propertyList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.propertyID}`}
          renderItem={({ item }) => {
            return (
              <ShortlistItem key={`propertyItem_${item.propertyID}`}
                id={item.propertyID} name={item.propertyName}
                image={shortlistDetails.propertyResult.imgPath + item.propCoverImage}
                location={item.cityLocName + ", " + item.stateName}
                onPress={(id: string) => {
                  onUnFavoriteClick(id, 1);
                }}
                price={item.propertyPrice ?? translate('dashboard.priceOnRequest')} />
            );
          }}
        />
      )}

      {activeOption === 2 && !isLoading && shortlistDetails?.projectResult?.projectList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={shortlistDetails?.projectResult?.projectList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.projectID}`}
          renderItem={({ item }) => {
            return (
              <ShortlistItem key={`propertyItem_${item.projectID}`}
                id={item.projectID} name={item.projectName}
                image={shortlistDetails.projectResult.imgPath + item.projCoverImage}
                location={item.cityName + ", " + item.stateName}
                price={item.propertyUnitPriceRange ?? translate('dashboard.priceOnRequest')}
                onPress={(id: string) => {
                  onUnFavoriteClick(id, 2);
                }} />
            );
          }}
        />
      )}

      {activeOption === 3 && !isLoading && shortlistDetails?.agentResult?.agentList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={shortlistDetails?.agentResult?.agentList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.favouriteID}`}
          renderItem={({ item }) => {
            return (
              <ShortlistItem key={`propertyItem_${item.favouriteID}`}
                id={item.favouriteID} name={item.userFirstName}
                image={shortlistDetails.agentResult.imgPath + item.userProfilePicture}
                location={item.cityName + ", " + item.stateName}
                price={''}
                onPress={(id: string) => {
                  onUnFavoriteClick(id, 3);
                }} />
            );
          }}
        />
      )}

      {activeOption === 4 && !isLoading && shortlistDetails?.developerResult?.developerList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={shortlistDetails?.developerResult?.developerList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.favouriteID}`}
          renderItem={({ item }) => {
            return (
              <ShortlistItem key={`propertyItem_${item.favouriteID}`}
                id={item.favouriteID} name={item.userFirstName}
                image={shortlistDetails.developerResult.imgPath + item.userProfilePicture}
                location={item.cityName + ", " + item.stateName}
                price={''}
                onPress={(id: string) => {
                  onUnFavoriteClick(id, 4);
                }} />
            );
          }}
        />
      )}

      {activeOption === 5 && !isLoading && shortlistDetails?.expertResult?.expertList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={shortlistDetails?.expertResult?.expertList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.favouriteID}`}
          renderItem={({ item }) => {
            return (
              <ShortlistItem key={`propertyItem_${item.favouriteID}`}
                id={item.favouriteID}
                name={item.userFirstName}
                image={shortlistDetails.expertResult.imgPath + item.userProfilePicture}
                location={item.cityName + ", " + item.stateName}
                price={''}
                onPress={(id: string) => {
                  onUnFavoriteClick(id, 5);
                }} />
            );
          }}
        />
      )}
    </View>
  );
};
