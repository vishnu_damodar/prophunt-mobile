import React from "react";
import { View } from "react-native";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import { translate } from "@app/config/locale/i18n";
import { DateUtils } from "@app/utils/date-utils";

export const EnquiryItem: React.FunctionComponent<any> = (props) => {

  return (
    <View
      style={{
        backgroundColor: AppColors.lightColor,
        marginBottom: ViewportService.commonPadding,
      }}>
      <View style={{ paddingHorizontal: ViewportService.commonPadding, paddingVertical: ViewportService.commonPadding, flex: 1 }}>
        <View style={{ flexDirection: "row", marginBottom: ViewportService.smallPadding }}>
          <View style={{ flex: 1 }}>
            <NormalText>{translate('dashboard.date')}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{DateUtils.formatDate(props?.data?.enquiryPostedDate, "DD/MM/yyyy")}</NormalText>
          </View>
          <View style={{ flex: 1 }}>
            <NormalText>{translate('dashboard.name')}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{props?.data?.enquiryName}</NormalText>
          </View>
        </View>

        <View style={{ flexDirection: "row", marginBottom: ViewportService.smallPadding }}>
          <View style={{ flex: 1 }}>
            <NormalText>{translate('dashboard.emailId')}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{props?.data?.enquiryEmail}</NormalText>
          </View>
          <View style={{ flex: 1 }}>
            <NormalText>{translate('dashboard.phoneNo')}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{`+${props?.data?.callingCode} ${props?.data?.enquiryPhone}`}</NormalText>
          </View>
        </View>

        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <NormalText>{translate('dashboard.message')}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{props?.data?.enquiryContent}</NormalText>
          </View>
        </View>
      </View>
    </View>
  );
};