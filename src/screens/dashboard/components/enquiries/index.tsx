import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  FlatList,
  View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { DashboardDetails, ListParamModel } from "@app/store/dashboard/dashboard-models";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import Loader from "@app/components/common/Loader";
import NoResult from "@app/components/common/no-result";
import useDashboard from "@app/hooks/useDashboard";
import { EnquiryItem } from "./enquiry-item";
export const EnquiriesScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dashboardDetails: DashboardDetails = useSelector((state: IAppState) => state.dashboard);

  const { getEnquiriesList, isLoading } = useDashboard();

  useEffect(() => {
    getList(0)
  }, []);

  const getList = (page: number) => {
    const params: ListParamModel = new ListParamModel();
    params.userID = auth.token;
    params.page = page;
    getEnquiriesList(params)
  };

  return (
    <View style={{ flex: 1 }}>
      {!isLoading && ((dashboardDetails?.enquiriesResult?.enquiries?.length ?? 0) === 0) && (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <NoResult />
        </View>
      )}

      {isLoading && (
        <View style={{ flex: 1 }}>
          <Loader size="small"></Loader>
        </View>
      )}
      {!isLoading && dashboardDetails?.enquiriesResult?.enquiries?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={dashboardDetails?.enquiriesResult?.enquiries ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `enquiry_${item.enquiryID}`}
          renderItem={({ item }) => {
            return (
              <EnquiryItem key={`enquiryItem_${item.enquiryID}`}
                data={item}
              />
            );
          }}
        />
      )}
    </View>
  );
};
