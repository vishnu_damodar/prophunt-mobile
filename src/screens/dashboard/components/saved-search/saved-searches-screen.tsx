import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  FlatList,
  View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { UnFavoriteParamModel } from "@app/store/shortlist/shortlist-models";
import { SavedSearchItem } from "./saved-search-item";
import { DashboardDetails, ListParamModel, PauseResumeDeleteParamModel } from "@app/store/dashboard/dashboard-models";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import Loader from "@app/components/common/Loader";
import NoResult from "@app/components/common/no-result";
import useDashboard from "@app/hooks/useDashboard";
export const SavedSearchesScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dashboardDetails: DashboardDetails = useSelector((state: IAppState) => state.dashboard);

  const { getSavedSearchList, pauseResumeSavedSearch, isLoading, deleteSavedSearch } = useDashboard();

  useEffect(() => {
    getList(0)
  }, []);

  const getList = (page: number) => {
    const params: ListParamModel = new ListParamModel();
    params.userID = auth.token;
    params.page = page;
    getSavedSearchList(params)
  };

  const onPauseResumeClick = (id: string, status: string) => {
    const params: PauseResumeDeleteParamModel = new PauseResumeDeleteParamModel();
    params.userID = auth.token;
    params.id = id;
    params.status = status;
    pauseResumeSavedSearch(params)
  };

  const onDeleteClick = (id: string) => {
    const params: PauseResumeDeleteParamModel = new PauseResumeDeleteParamModel();
    params.userID = auth.token;
    params.id = id;
    deleteSavedSearch(params)
  };

  return (
    <View style={{ flex: 1 }}>
      {!isLoading && ((dashboardDetails?.savedSearchResult?.savedSearchList?.length ?? 0) === 0) && (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <NoResult />
        </View>
      )}

      {isLoading && (
        <View style={{ flex: 1 }}>
          <Loader size="small"></Loader>
        </View>
      )}
      {!isLoading && dashboardDetails?.savedSearchResult?.savedSearchList?.length > 0 && (
        <FlatList
          style={{
            backgroundColor: AppColors.lightGray,
            padding: ViewportService.commonPadding,
            flex: 1
          }}
          data={dashboardDetails?.savedSearchResult?.savedSearchList ?? []}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => `property_${item.searchID}`}
          renderItem={({ item }) => {
            return (
              <SavedSearchItem key={`propertyItem_${item.searchID}`}
                data={item}
                onPress={(id: string) => {
                }}
                onDeleteClick={(id: string) => {
                  onDeleteClick(id)
                }}
                onPauseResumeClick={(id: string, status: string) => {
                  onPauseResumeClick(id, status);
                }}
              />
            );
          }}
        />
      )}
    </View>
  );
};
