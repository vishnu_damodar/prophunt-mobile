import React from "react";
import { View, Image, TouchableOpacity, Alert } from "react-native";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NormalText } from "@app/components/common/text/normal-text";
import { AppColors } from "@app/config/ui/color";
import Img from "@app/assets/images";
import { translate } from "@app/config/locale/i18n";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { DateUtils } from "@app/utils/date-utils";
import { IAppState } from "@app/store/state-props";
import { useSelector } from "react-redux";
import { SettingsState } from "@app/store/settings/settings-models";

export const SavedSearchItem: React.FunctionComponent<any> = (props) => {
  const settings: SettingsState = useSelector((state: IAppState) => state.settings);
  const getPriceData = (minPrice: string, maxPrice: string): String => {
    const value =
      (minPrice == "0" && maxPrice != "0")
        ? (`| ${settings.currency} ${maxPrice} ${translate('dashboard.below')}`)
        : (minPrice != "0" && maxPrice == "0")
          ? (`| ${settings.currency} ${minPrice} ${translate('dashboard.above')}`)
          : (minPrice != "0" && maxPrice != "0")
            ? (`| ${settings.currency} ${minPrice} - ${settings.currency} ${maxPrice}`)
            : (minPrice != "0" && maxPrice != "0") ? ''
              : ''

    return value
  };

  const getFurnishedData = (furnished: string): String => {
    const value = (furnished === undefined || furnished == '') ? ''
      : ` | ${translate('dashboard.furnished')} : ${furnished}`;
    return value
  };

  const getPurposeData = (purpose: string): String => {
    const value = (purpose === undefined) ? ''
      : (purpose == 'Sell')
        ? ` ${translate('dashboard.for')} ${translate('dashboard.buy')}`
        : (purpose == 'Rent')
          ? ` ${translate('dashboard.for')} ${translate('dashboard.rent')}`
          : '';
    return value
  };

  const getLocationData = (location: string): String => {
    const value = (location !== undefined && location != '')
      ? `| ${location}`
      : '';
    return value
  };

  const notify = () => {
    Alert.alert(
      translate('dashboard.alert'),
      translate('dashboard.alertDelete'),
      [
        {
          text: translate('common.cancel'),
          onPress: () => { },
          style: 'cancel',
        },
        { text: translate('common.ok'), onPress: () => props.onDeleteClick(props.data.searchID) },
      ],
      { cancelable: false },
    );
  };

  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: AppColors.lightColor,
        marginBottom: ViewportService.commonPadding,
      }}>

      <View style={{ padding: ViewportService.commonPadding }}>
        <SubHeading>{props.data.searchTitle}</SubHeading>

        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginVertical: ViewportService.smallPadding }}>
          <NormalText>{`${props.data.saveSearchParameter.propertyTypeName}${getPurposeData(props.data.searchParameters.searchPropertyPurpose)}${props.data.bhkRes ? ` | ${props.data.bhkRes} ${translate('dashboard.bedrooms')}` : ''} ${getPriceData(props.data.searchMinPriceValue, props.data.searchMaxPriceValue)} ${getFurnishedData(props.data.saveSearchParameter?.attrDetails?.Furnished)} ${getLocationData(props.data.saveSearchParameter?.searchLocation)}`}</NormalText>
        </View>
        <NormalText style={{ color: AppColors.grayTextColor }}>{`${translate('dashboard.savedOn')} ${DateUtils.formatDate(props.data.searchDate, "DD MMM, yyyy")}`}</NormalText>
        <View style={{ flexDirection: "row", marginTop: ViewportService.smallPadding }}>
          <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginRight: ViewportService.smallPadding }}
            onPress={notify}>
            <Image
              source={Img.delete}
              style={{
                marginRight: ViewportService.smallPadding
              }}></Image>
            <NormalText style={{ color: AppColors.primaryThemeColor }}>{translate('common.delete')}</NormalText>
          </TouchableOpacity>

          <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}
            onPress={() => props.onPauseResumeClick(props.data.searchID, props.data.searchStatus == 'Inactive' ? "Active" : "Inactive")}>
            <Image
              source={props.data.searchStatus == 'Inactive' ? Img.resume : Img.pause}
              style={{
                marginRight: ViewportService.smallPadding
              }}></Image>
            <NormalText style={{ color: AppColors.primaryThemeColor }}>{props.data.searchStatus == 'Inactive' ? translate('dashboard.resume') : translate('dashboard.pause')}</NormalText>
          </TouchableOpacity>
        </View>

      </View>
    </TouchableOpacity>
  );
};
