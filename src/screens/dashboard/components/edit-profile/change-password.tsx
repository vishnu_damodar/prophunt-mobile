import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { ChangePasswordParamModel, } from "@app/store/dashboard/dashboard-models";
import { NormalText } from "@app/components/common/text/normal-text";
import { commonStyles } from "@app/components/common/common-style";
import { translate } from "@app/config/locale/i18n";
import { AppColors } from "@app/config/ui/color";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { validators } from "@app/components/common/widgets/form/validators";
import { ViewportService } from "@app/config/ui/viewport-service";
import AppButton from "@app/components/common/AppButton";
import useDashboard from "@app/hooks/useDashboard";
import { SubHeading } from "@app/components/common/text/sub-heading";
export const ChangePassword = (props: {
  cancel: () => void;
}) => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const [showError, setShowError] = useState(false);
  const { isLoading, changePassword, passwordSuccess } = useDashboard();

  useEffect(() => {
    if (passwordSuccess) {
      currentPassword.setValue('')
      newPassword.setValue('')
      confirmPassword.setValue('')
    }
  }, [passwordSuccess]);


  const currentPassword = useTextFormInput('', [
    validators.requiredValidator(translate("dashboard.enterCurrentPassword")),
  ]);

  const newPassword = useTextFormInput('', [
    validators.requiredValidator(translate("dashboard.enterNewPassword")),
    validators.passwordValidator(translate("dashboard.passwordHint")),
  ]);

  const confirmPassword = useTextFormInput('', [
    validators.requiredValidator(translate("dashboard.enterConfirmPassword")),
    validators.confirmValidator(newPassword.value, translate("dashboard.passwordMismatch"))
  ]);

  const formItems = [currentPassword, newPassword, confirmPassword];

  const isValid = () => {
    let isValid: boolean = true;
    setShowError(true);
    formItems.forEach((p) => {
      if (!p.valid) {
        p.showErrorMessage();
        isValid = false;
      }
    });
    return isValid;
  };

  const onSaveProfileClick = () => {
    if (isValid()) {
      let params = new ChangePasswordParamModel();
      params.userID = auth.token
      params.userPassword = currentPassword.value
      params.userNewPassword = newPassword.value
      params.userConfirmPassword = confirmPassword.value
      changePassword(params)
    }
  };

  const onCancel = () => {
    props.cancel();
  };

  return (
    <View style={{
      flex: 1,
    }}>
      <ScrollView
        keyboardShouldPersistTaps={"always"}
        style={{ flex: 1 }}
        contentContainerStyle={{
          paddingBottom: ViewportService.commonPadding * 2,
        }}>

        <View
          style={{
            flex: 1,
            backgroundColor: AppColors.lightColor,
            marginHorizontal: ViewportService.commonPadding,
            padding: ViewportService.commonPadding,
            marginVertical: ViewportService.commonPadding,
          }}
        >

          <TouchableOpacity
            onPress={onCancel}
            style={{ alignSelf: 'flex-end' }}>
             <SubHeading style={{ textDecorationLine: 'underline', color: AppColors.primaryThemeColor }}>{translate("common.back")}</SubHeading>
          </TouchableOpacity>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.currentPassword")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {currentPassword.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox],
              {
                placeholder: translate("dashboard.currentPassword"),
                secureTextEntry: true,
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.newPassword")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {newPassword.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox],
              {
                placeholder: translate("dashboard.newPassword"),
                secureTextEntry: true,
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.confirmPassword")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>

            {confirmPassword.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.confirmPassword"),
                secureTextEntry: true,
              }
            )}
          </View>

          <AppButton
            title={translate("dashboard.save")}
            loading={isLoading}
            disabled={isLoading}
            style={{
              alignSelf: 'baseline',
              paddingHorizontal: 50,
              borderRadius: 3,
              marginTop: ViewportService.commonPadding,
              borderBottomColor: AppColors.borderColor,
              borderBottomWidth: 0.7,
            }}
            onPress={() => {
              onSaveProfileClick()
            }}
          />
        </View>

      </ScrollView>
    </View >
  );
};

