import React, { useState } from "react";
import {
  ScrollView,
  View,
} from "react-native";
import useDashboard from "@app/hooks/useDashboard";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import { ImagePickerResponse } from "react-native-image-picker";
import { ChangePassword } from "./change-password";
import Loader from "@app/components/common/Loader";
import { ProfileComponent } from "./profile-component";
export const ProfileScreen: React.FunctionComponent = () => {
  const [showChangePassword, setShowChangePassword] = useState<boolean>(false);

  const { isLoading } = useDashboard();

  const onClickCancel = () => {
    setShowChangePassword(false);
  };

  const [imageDetails, setImageDetails] = useState<ImagePickerResponse>();

  return (
    <View style={{ flex: 1, backgroundColor: AppColors.lightGray }}>
      <ScrollView
        keyboardShouldPersistTaps={"always"}
        style={{ flex: 1 }}
        contentContainerStyle={{
          paddingBottom: ViewportService.commonPadding * 2,
        }}>

        {showChangePassword ? (
          <ChangePassword
            cancel={onClickCancel}
          />
        ) : <ProfileComponent showChangePassword={() => { setShowChangePassword(true) }} />}

        {isLoading && (
          <View style={{ flex: 1 }}>
            <Loader size="small" />
          </View>

        )}

      </ScrollView>
    </View >
  );
};

