import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Alert,
  Image,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { DashboardDetails, PauseResumeDeleteParamModel, SaveProfileParamModel } from "@app/store/dashboard/dashboard-models";
import useDashboard from "@app/hooks/useDashboard";
import { NormalText } from "@app/components/common/text/normal-text";
import { commonStyles } from "@app/components/common/common-style";
import { translate } from "@app/config/locale/i18n";
import { AppColors } from "@app/config/ui/color";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { validators } from "@app/components/common/widgets/form/validators";
import { ViewportService } from "@app/config/ui/viewport-service";
import { ICountryCode, IPhoneNumber, PhoneWithCountryCode } from "@app/components/common/phone-with-country-code";
import AppButton from "@app/components/common/AppButton";
import Img from "@app/assets/images";
import { MapComponent, PlaceDetails, UserLocation } from "@app/components/map-component";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { Fonts } from "@app/config/ui/font";
import { ImagePickerResponse } from "react-native-image-picker";
export const ProfileComponent = (props: { showChangePassword: any; }) => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const dashboardDetails: DashboardDetails = useSelector((state: IAppState) => state.dashboard);
  const [showError, setShowError] = useState(false);
  const [phone, setPhone] = useState("");
  const [callingCode, setCallingCode] = useState('91');
  const [countryCode, setCountryCode] = useState(auth.userDetails?.countryCode?.toUpperCase());
  const [trigger, setTrigger] = useState(false);
  const [gender, setGender] = useState(dashboardDetails?.profileResult?.profileDetails?.userGender ?? "Male");
  const [selectedAddress, setSelectedAddress] = useState("");

  const { getProfileData, isLoading, saveProfile } = useDashboard();

  const firstName = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.userFirstName ?? '', [
    validators.requiredValidator(translate("register.enterFName")),
  ]);

  const lastName = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.userLastName ?? '', [
    validators.requiredValidator(translate("register.enterLName")),
  ]);

  const emailAddress = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.userEmail ?? '', [
    validators.requiredValidator(translate("register.enterEmail")),
  ]);

  const country = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.googleCountryName ?? '', [
    validators.requiredValidator(translate("dashboard.enterCountry")),
  ]);

  const region = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.googleStateName ?? '', [
    validators.requiredValidator(translate("dashboard.enterState")),
  ]);

  const city = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.googleCityName ?? '', [
    validators.requiredValidator(translate("dashboard.enterCity")),
  ]);

  const locality = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.googleLocality ?? '', [
    validators.requiredValidator(translate("dashboard.enterLocality")),
  ]);

  const postalCode = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.userZip ?? '', []);

  const address = useTextFormInput(dashboardDetails?.profileResult?.profileDetails?.userAddress1 ?? '', [
    validators.requiredValidator(translate("register.enterAddress")),
  ], 5);

  useEffect(() => {
    getProfileData(auth.token)
  }, []);

  useEffect(() => {
    let item = dashboardDetails.profileResult.profileDetails;
    firstName.setValue(item?.userFirstName ?? '');
    lastName.setValue(item?.userLastName ?? '');
    emailAddress.setValue(item?.userEmail ?? '');
    country.setValue(item?.googleCountryName ?? '');
    city.setValue(item?.googleCityName ?? '');
    region.setValue(item?.googleStateName ?? '');
    locality.setValue(item?.googleLocality ?? '');
    address.setValue(item?.userAddress1 ?? '');
    postalCode.setValue(item?.userZip ?? '');
  }, [dashboardDetails?.profileResult?.profileDetails]);

  const formItems = [firstName, lastName, emailAddress, country, region, city, locality, address];

  const verifyPhoneNumber = async (phoneDetails: IPhoneNumber) => {
    setPhone(phoneDetails.phone);
    setCallingCode(phoneDetails.callingCode);
    setCountryCode(phoneDetails.code);
  };

  const isValid = () => {
    let isValid: boolean = true;
    setShowError(true);
    formItems.forEach((p) => {
      if (!p.valid) {
        p.showErrorMessage();
        isValid = false;
      }
    });
    return isValid && phone;
  };

  const onSaveProfileClick = () => {
    if (isValid()) {
      const params = new SaveProfileParamModel()
      params.userID = auth.token
      params.userFirstName = firstName.value
      params.userLastName = lastName.value
      params.userGender = gender
      params.googleCountryName = country.value
      params.googleStateName = region.value
      params.googleCityName = city.value
      params.googleLocality = locality.value
      params.userZip = postalCode.value
      params.userPhone = phone
      params.countryCode = countryCode
      params.userEmail = emailAddress.value
      params.userAddress1 = address.value
      saveProfile(params)
    }
  };

  const onPickAddress = (place: PlaceDetails) => {
    let userLocation = new UserLocation();
    userLocation.lat = place.lat;
    userLocation.lng = place.lng;
    setSelectedAddress(place.formattedAddress);
    country.setValue(place.country)
    region.setValue(place.state)
    city.setValue(place.city)
    locality.setValue(place.locality)
    postalCode.setValue(place.postalCode)
  };

  const selectFile = () => {

  };

  const [imageDetails, setImageDetails] = useState<ImagePickerResponse>();

  return (
    <View style={{ flex: 1, backgroundColor: AppColors.lightGray }}>
      <ScrollView
        keyboardShouldPersistTaps={"always"}
        style={{ flex: 1 }}
        contentContainerStyle={{
          paddingBottom: ViewportService.commonPadding * 2,
        }}>

        <TouchableOpacity onPress={() => { props?.showChangePassword() }} style={{ marginHorizontal: ViewportService.commonPadding, marginTop: ViewportService.commonPadding, alignSelf: 'flex-end' }}>
          <SubHeading style={{ textDecorationLine: 'underline', color: AppColors.primaryThemeColor }}>{translate("dashboard.changePassword")}</SubHeading>
        </TouchableOpacity>

        <View
          style={{
            flex: 1,
            backgroundColor: AppColors.lightColor,
            marginHorizontal: ViewportService.commonPadding,
            padding: ViewportService.commonPadding,
            marginTop: ViewportService.commonPadding,
          }}>
          <SubHeading style={{ fontFamily: Fonts.Regular }}>{translate("dashboard.profilePicture")}</SubHeading>
          <View style={{ marginTop: ViewportService.commonPadding, alignSelf: 'baseline' }}>
            {(dashboardDetails?.profileResult?.profileDetails?.userProfilePicture === undefined || dashboardDetails?.profileResult?.profileDetails?.userProfilePicture === '')
              ? <TouchableOpacity onPress={() => selectFile()} style={{ borderRadius: 3, borderWidth: 1, borderColor: AppColors.borderColor, borderStyle: 'dashed', backgroundColor: AppColors.lightGray, marginBottom: ViewportService.commonPadding }}>
                <SubHeading style={{ fontFamily: Fonts.Regular, borderRadius: 3, borderWidth: 1, borderColor: AppColors.borderColor, margin: ViewportService.commonPadding, padding: 3, textAlign: "center", backgroundColor: AppColors.lightColor }}>{translate("dashboard.chooseProfilePhoto")}</SubHeading>
              </TouchableOpacity>
              : <TouchableOpacity onPress={() => selectFile()}>
                <Image source={{ uri: dashboardDetails?.profileResult?.imgPath + dashboardDetails?.profileResult?.profileDetails.userProfilePicture }} style={{ height: 70, width: 80, marginBottom: ViewportService.commonPadding, borderWidth: 1, borderColor: AppColors.borderColor, borderRadius: 3 }} />
              </TouchableOpacity>}

            <NormalText style={{ color: AppColors.grayTextColor }}>{translate("dashboard.maxFileSize")}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{translate("dashboard.allowedFileTypes")}</NormalText>
            <NormalText style={{ color: AppColors.grayTextColor }}>{translate("dashboard.preferedSize")}</NormalText>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            backgroundColor: AppColors.lightColor,
            marginHorizontal: ViewportService.commonPadding,
            padding: ViewportService.commonPadding,
            marginVertical: ViewportService.commonPadding,
          }}
        >

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("register.fname")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {firstName.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("register.fname"),
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("register.lname")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {lastName.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("register.lname"),
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("register.email")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {emailAddress.widget(
              [commonStyles.textBoxContainer, { backgroundColor: AppColors.lightGray }],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("register.email"),
              },
              null,
              null,
              true
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("register.mobileNumber")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            <PhoneWithCountryCode
              phone={dashboardDetails?.profileResult?.profileDetails?.userPhone}
              defaultCode={{
                callingCode: callingCode,
                code: countryCode,
              }}
              trigger={trigger}
              onSubmit={(phoneDetails: IPhoneNumber) => {
                verifyPhoneNumber(phoneDetails);
              }}
            />
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.localityInfo")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            <MapComponent
              hideMap={false}
              defaultLocation={new UserLocation()}
              defaultValue={selectedAddress}
              onSelect={() => {
                onPickAddress
              }}
            />
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.country")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {country.widget(
              [commonStyles.textBoxContainer, { backgroundColor: AppColors.lightGray }],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.country"),
              },
              null,
              null,
              true
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.region")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {region.widget(
              [commonStyles.textBoxContainer, { backgroundColor: AppColors.lightGray }],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.region"),
              },
              null,
              null,
              true
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.city")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {city.widget(
              [commonStyles.textBoxContainer, { backgroundColor: AppColors.lightGray }],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.city"),
              },
              null,
              null,
              true
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.locality")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {locality.widget(
              [commonStyles.textBoxContainer, { backgroundColor: AppColors.lightGray }],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.locality"),
              },
              null,
              null,
              true
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.postalCode")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {postalCode.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("dashboard.postalCode"),
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("register.address")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            {address.widget(
              [commonStyles.textBoxContainer, { height: 100 }],
              [commonStyles.textBox, { color: AppColors.grayTextColor, textAlignVertical: "top" }],
              {
                placeholder: translate("register.address"),
              }
            )}
          </View>

          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("dashboard.gender")}</NormalText>

            <View style={{ flexDirection: "row", paddingVertical: 5 }}>
              <TouchableOpacity
                onPress={() => {
                  setGender(gender == "Male" ? "Female" : "Male")
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginRight: ViewportService.smallPadding
                }}>
                <Image style={{ marginRight: ViewportService.smallPadding }} source={gender == "Male" ? Img.radioButtonOn : Img.radioButton}></Image>
                <NormalText>{translate("dashboard.male")}</NormalText>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setGender(gender == "Male" ? "Female" : "Male")
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}>
                <Image style={{ marginRight: ViewportService.smallPadding }} source={gender == "Female" ? Img.radioButtonOn : Img.radioButton}></Image>
                <NormalText>{translate("dashboard.female")}</NormalText>
              </TouchableOpacity>
            </View>

          </View>

          <AppButton
            title={translate("dashboard.save")}
            loading={isLoading}
            disabled={isLoading}
            style={{
              alignSelf: 'baseline',
              paddingHorizontal: 50,
              borderRadius: 3,
              marginTop: ViewportService.commonPadding,
              borderBottomColor: AppColors.borderColor,
              borderBottomWidth: 0.7,
            }}
            onPress={() => {
                setTrigger(!trigger);
                onSaveProfileClick()
            }}
          />
        </View>

      </ScrollView>
    </View >
  );
};

