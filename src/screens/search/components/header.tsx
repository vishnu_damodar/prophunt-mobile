import React from "react";
import { Header, View, Button } from "native-base";
import { Image } from "react-native";
import { AppColors } from "@app/config/ui/color";
import { NavigationService } from "@app/services/navigation-service";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { commonStyles } from "@app/components/common/common-style";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { TouchableOpacity } from "react-native-gesture-handler";
import { NormalText } from "@app/components/common/text/normal-text";
interface Props {
  title: string;
  reset: () => void;
}
export const FilterHeader: React.FunctionComponent<Props> = (props) => {
  return (
    <Header
      style={commonStyles.primaryHeaderContainer}
      androidStatusBarColor={AppColors.headerColor}
    >
      <View
        style={{
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
        }}
      >
        <Button onPress={() => NavigationService.goBack()} transparent>
          <View style={{ padding: ViewportService.smallPadding }}>
            <Image
              resizeMode="contain"
              style={{ height: 20 }}
              source={Img.back}
            />
          </View>
        </Button>

        <View
          style={{
            flex: 1,
            alignItems: "flex-start",
            marginLeft: ViewportService.smallPadding,
          }}
        >
          <SubHeading color={AppColors.lightColor}>
            {props.title ?? ""}
          </SubHeading>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              props.reset();
            }}
          >
            <NormalText
              style={{ fontWeight: "700" }}
              color={AppColors.lightColor}
            >
              Reset
            </NormalText>
          </TouchableOpacity>
        </View>
      </View>
    </Header>
  );
};
