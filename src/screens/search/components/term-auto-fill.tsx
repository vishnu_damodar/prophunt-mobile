import React, { useCallback, useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Modal,
  Text,
  Image,
  StyleSheet,
  FlatList,
  RefreshControl,
} from "react-native";
import { AppColors } from "@app/config/ui/color";
import { translate } from "@app/config/locale/i18n";
import Img from "@app/assets/images";
import { commonStyles } from "@app/components/common/common-style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { RegionLocation } from "@app/services/api/models";
import useSearch from "@app/hooks/useSearch";
import { NormalText } from "@app/components/common/text/normal-text";
import NoResult from "@app/components/common/no-result";
interface Props {
  term: string;
  status: boolean;
  onCancel: () => void;
  onSelect: (location: RegionLocation) => void;
}
export const TermSearchModel: React.FunctionComponent<Props> = (props) => {
  const searchTerm = useTextFormInput(props.term ?? "", []);
  const { isLoading, locations, projects, getRegions } = useSearch();
  useEffect(() => {
    searchTerm.setValue(props.term ?? "");
  }, [props.term]);

  const onCancel = () => {
    searchTerm.setValue("");
    props.onCancel();
  };

  const debounce = (func: () => void, wait: number) => {
    let timeout;
    return function (...args) {
      const context = this;
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => {
        timeout = null;
        func.apply(context, args);
      }, wait);
    };
  };

  const debounceOnChange = useCallback(debounce(getRegions, 400), []);

  const renderItem = (item: RegionLocation) => {
    return (
      <TouchableOpacity
        onPress={() => {
          props.onSelect(item);
        }}
        style={{ paddingVertical: ViewportService.commonPadding / 2 }}
      >
        <NormalText>{item.text}</NormalText>
      </TouchableOpacity>
    );
  };

  return (
    <Modal animationType="fade" transparent={true} visible={props.status}>
      <View style={{ flex: 1, backgroundColor: "#00000050" }}>
        <View
          style={{
            flex: 1,
            backgroundColor: AppColors.lightColor,
            // margin: ViewportService.commonPadding,
            borderRadius: 6,
          }}
        >
          <View
            style={{
              alignItems: "flex-end",
              backgroundColor: AppColors.headerColor,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                props.onCancel();
              }}
              style={{
                padding: ViewportService.commonPadding,
              }}
            >
              <Image
                resizeMode={"contain"}
                source={Img.cancel}
                style={{
                  tintColor: AppColors.lightColor,
                  height: 15,
                  width: 15,
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={{ margin: ViewportService.commonPadding }}>
            {searchTerm.widget(
              [commonStyles.textBoxContainer],
              [commonStyles.textBox, { color: AppColors.grayTextColor }],
              {
                placeholder: translate("search.searchByLocation"),
                autoFocus: true,
              },
              (value: any) => {
                debounceOnChange(value);
                // getRegions();
              }
            )}
          </View>
          <FlatList
            contentContainerStyle={{
              padding: ViewportService.commonPadding,
            }}
            refreshControl={<RefreshControl refreshing={isLoading} />}
            showsVerticalScrollIndicator={false}
            data={locations}
            // contentContainerStyle={{ paddingBottom: 24 }}
            renderItem={({ item }) => renderItem(item)}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={
              !isLoading && locations.length == 0 ? <NoResult /> : <View />
            }
          />
        </View>
      </View>
    </Modal>
  );
};

export interface ISort {
  id: number;
  title: string;
  isSelected: boolean;
}
