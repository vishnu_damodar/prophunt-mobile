import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigationParam } from "react-navigation-hooks";
import {
  View,
  Text,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  Platform,
  Dimensions,
  ImageBackground,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import { AppColors } from "@app/config/ui/color";
import { ViewportService } from "@app/config/ui/viewport-service";
import { NavigationService } from "@app/services/navigation-service";
import Img from "@app/assets/images";
import { Fonts } from "@app/config/ui/font";
import { translate } from "@app/config/locale/i18n";
import { AppRoutes } from "@app/route/route-keys";
import { AuthDetails } from "@app/store/auth/auth-models";
import { IAppState } from "@app/store/state-props";
import { HeaderType, RootLayout } from "@app/container/layout/root/root-layout";
import AppButton from "@app/components/common/AppButton";
import BottomLister from "@app/components/common/bottom-lister";
import BottomSectionLister from "@app/components/common/bottom-section-lister";
import { NormalText } from "@app/components/common/text/normal-text";
import { MapComponent, UserLocation } from "@app/components/map-component";
import useAppSettings from "@app/hooks/useAppSettings";
import {
  BuyRentPrice,
  PropertyType,
  PropertyTypeSubCategory,
  RegionLocation,
} from "@app/services/api/models";
import { wp } from "@app/config/ui/ui-utils";
import { commonStyles } from "@app/components/common/common-style";
import { useTextFormInput } from "@app/components/common/widgets/form/text";
import { TermSearchModel } from "./components/term-auto-fill";
import { SetPropertySearchParamsAction } from "@app/store/search/search-actions";
import { PropertySearchParamModel } from "@app/store/search/search-models";
import { AppConfig } from "@app/config/app-config";
import { ToasterReferences } from "@app/services/toast-service";
import {
  IRadioOption,
  RadioOptions,
} from "@app/components/common/radio-options";
import SelectionLister from "@app/components/common/selection-lister";
import { IKeyValuePair } from "@app/components/common/widgets/form/dropdown";
import { SmallText } from "@app/components/common/text/small-text";
import useSearch from "@app/hooks/useSearch";
import { FilterHeader } from "./components/header";
export const SearchScreen: React.FunctionComponent = () => {
  const { currency } = useAppSettings();
  const { getAttributes, attributes, getBedRooms, bedRooms } = useSearch();
  const dispatch = useDispatch();
  const isFilter = useNavigationParam("isFilter");
  const [selectedPurpose, setPurpose] = useState<
    "rent" | "buy" | string | undefined
  >(useNavigationParam("purpose"));
  const [selectedSoldBy, setSoldBy] = useState<"owner" | "agent" | string>(
    "owner"
  );
  const purposeOptions: IRadioOption[] = [
    {
      key: "buy",
      value: translate("search.buy"),
    },
    {
      key: "rent",
      value: translate("search.rent"),
    },
  ];
  const soldByOptions: IRadioOption[] = [
    {
      key: "owner",
      value: "Owner",
    },
    {
      key: "Agent",
      value: "Agent",
    },
  ];
  // let bedRooms: IKeyValuePair[] = [];
  // for (let i = 0; i <= 10; i++) {
  //   bedRooms.push({
  //     key: i,
  //     value: `${i > 9 ? "+" : ""} ${i > 0 ? i : ""} ${translate(
  //       "search.bedrooms"
  //     )}`,
  //   });
  // }
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const ref = useRef(null);
  const {
    buyMaxPrices,
    buyMinPrices,
    rentMaxPrices,
    rentMinPrices,
    cityList,
    propertyTypes,
  } = useAppSettings();
  const [snappedItem, setSnappedItem] = useState(0);
  const [termSearchModel, setTermSearchModel] = useState(false);

  const [selectedCity, setCity] = useState(cityList[0]);
  const locationTerm = useTextFormInput("", []);
  const [locationId, setLocationId] = useState("");
  const [propertyTypeId, setType] = useState("");
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(0);
  const [selectedBedRooms, setSelectedBedrooms] = useState<IKeyValuePair[]>([]);
  const [attributesStr, setAttributesStr] = useState<string[]>([]);
  const [reset, setFilterReset] = useState(false);
  useEffect(() => {
    if (propertyTypeId) {
      getAttributes(propertyTypeId);
      getBedRooms(propertyTypeId);
    }
  }, [propertyTypeId]);

  const onSearch = () => {
    let bedrooms = "";
    let attrStr = attributesStr.toString();
    attrStr = attrStr.replace(/,/g, ":");
    selectedBedRooms.map((br, index) => {
      if (index == 0) {
        bedrooms += `${br.key},`;
      } else {
        bedrooms += `${br.key.split("~")[1]},`;
      }
    });
    attrStr += `:${bedrooms}`;
    attrStr = attrStr.replace(/,\s*$/, "");

    let params: PropertySearchParamModel = {
      cityID: selectedCity.cityId,
      searchPropertyPurpose: selectedPurpose == "buy" ? "Sell" : "Rent",
      itemType: "Property",
      sortBy: "date/orderby/desc",
      searchPropertyTypeID: propertyTypeId !== "-1" ? propertyTypeId : "",
      searchCountryID: AppConfig.countryId,
      searchRegion: locationId ? locationId : "",
      searchMinPrice: `${minPrice}`,
      searchMaxPrice: `${maxPrice}`,
      searchAttributesStr: attrStr,
    };
    dispatch(SetPropertySearchParamsAction(params));
    if (minPrice > maxPrice) {
      ToasterReferences.showErrorMsg("Min Price should be less than Max Price");
    } else {
      NavigationService.navigate(AppRoutes.PropertyListing, {
        locationTerm: locationTerm.value,
      });
    }
  };

  const onFilterReset = () => {
    setSelectedBedrooms([]);
    setAttributesStr([]);
    setMinPrice(0);
    setMaxPrice(0);
    setType("");
    setLocationId("");
    let params: PropertySearchParamModel = {
      cityID: selectedCity.cityId,
      searchPropertyPurpose: selectedPurpose == "buy" ? "Sell" : "Rent",
      itemType: "Property",
      sortBy: "date/orderby/desc",
      searchPropertyTypeID: "",
      searchCountryID: AppConfig.countryId,
      searchRegion: "",
      searchMinPrice: "",
      searchMaxPrice: "",
      searchAttributesStr: "",
    };
    dispatch(SetPropertySearchParamsAction(params));
  };

  return (
    <RootLayout showFooter={true} headerType={HeaderType.Custom}>
      <FilterHeader
        title={
          isFilter
            ? "Filter"
            : selectedPurpose === "buy"
            ? translate("search.buy")
            : translate("search.rent")
        }
        reset={onFilterReset}
      />
      <ScrollView
        keyboardShouldPersistTaps={"always"}
        showsVerticalScrollIndicator={false}
        style={{
          backgroundColor: AppColors.lightColor,
          padding: ViewportService.commonPadding,
          flex: 1,
        }}
        contentContainerStyle={{
          paddingBottom: ViewportService.commonPadding * 2,
        }}
      >
        <View style={{ marginTop: ViewportService.commonPadding }}>
          <NormalText>{translate("search.selectCity")}</NormalText>
          <View style={{ marginVertical: 3 }}></View>
          <BottomLister
            height={300}
            items={cityList}
            defaultSelected={cityList[0]}
            valueKey={"cityName"}
            onSelect={(city) => {
              setCity(city);
            }}
            defaultText={"Select City"}
          />
        </View>
        <View style={{ marginTop: ViewportService.commonPadding }}>
          <NormalText>{translate("search.searchByLocation")}</NormalText>
          <View style={{ marginVertical: 3 }}></View>
          {locationTerm.widget(
            [commonStyles.textBoxContainer],
            [commonStyles.textBox, { color: AppColors.grayTextColor }],
            { placeholder: translate("search.searchByLocation") },
            (value: any) => {
              setTermSearchModel(true);
            }
          )}
        </View>
        {isFilter && (
          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("search.purpose")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            <RadioOptions
              onChange={(value: string) => {
                setPurpose(value);
              }}
              selectedKey={selectedPurpose ?? ""}
              options={purposeOptions}
            />
          </View>
        )}
        <View style={{ marginTop: ViewportService.commonPadding }}>
          <NormalText>{translate("search.propertyType")}</NormalText>
          <View style={{ marginVertical: 3 }}></View>
          <View style={{ marginTop: ViewportService.commonPadding }}>
            <BottomSectionLister
              height={ViewportService.hight / 1.5}
              items={propertyTypes}
              valueKey={"propertyTypeName"}
              sectionValuesKey={"subCategory"}
              sectionKey={"propertyTypeName"}
              defaultText={"Select Property type"}
              defaultSelected={propertyTypes[0].subCategory[0]}
              onSelect={(item: PropertyTypeSubCategory) => {
                setType(item.propertyTypeId);
              }}
            />
          </View>
        </View>
        {isFilter && (
          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("search.soldBy")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            <RadioOptions
              onChange={(value: string) => {
                setSoldBy(value);
              }}
              selectedKey={selectedSoldBy}
              options={soldByOptions}
            />
          </View>
        )}

        {isFilter && propertyTypeId != "" && (
          <View style={{ marginTop: ViewportService.commonPadding }}>
            <NormalText>{translate("search.bedrooms")}</NormalText>
            <View style={{ marginVertical: 3 }}></View>
            <SelectionLister
              disable={bedRooms.length == 0}
              label={translate("search.bedrooms")}
              height={500}
              items={bedRooms}
              valueKey={"value"}
              idKey={"key"}
              showSelected
              selectionType={"multi"}
              onApply={(values) => {
                setSelectedBedrooms(values);
              }}
            />
          </View>
        )}

        {isFilter &&
          propertyTypeId != "" &&
          attributes.length > 0 &&
          attributes?.map((attribute, index) => {
            let options = attribute.attrOptName.split(",");
            let keyValues: IKeyValuePair[] = [];
            options.map((opt) => {
              let val = opt.split("_")[0];
              keyValues.push({ key: `${attribute.attrID}~${val}`, value: val });
            });
            return (
              <View
                key={`${attribute.attrName}_${attribute.attrID}`}
                style={{ marginTop: ViewportService.commonPadding }}
              >
                <NormalText>{attribute.attrName ?? ""}</NormalText>
                <View style={{ marginVertical: 3 }}></View>
                <SelectionLister
                  label={"All"}
                  height={
                    keyValues.length >= 10 ? 500 : 200 + keyValues.length * 30
                  }
                  items={keyValues}
                  valueKey={"value"}
                  idKey={"key"}
                  showSelected
                  selectionType={
                    attribute.selection == "single" ? "single" : "multi"
                  }
                  onApply={(values) => {}}
                  onSelect={(item: IKeyValuePair) => {
                    if (!attributesStr.find((str) => str === item.key)) {
                      attributesStr.push(item.key);
                    } else {
                      attributesStr.filter((str) => str !== item.key);
                    }
                    setAttributesStr(attributesStr);
                  }}
                />
              </View>
            );
          })}

        <View style={{ marginTop: ViewportService.commonPadding }}>
          <NormalText>{translate("search.pricerange")}</NormalText>
          <View style={{ marginVertical: 3 }}></View>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View style={{ flex: 1 }}>
              {/* <NormalText>{"Price from"}</NormalText> */}
              <View style={{ marginVertical: 3 }}></View>
              <BottomLister
                height={300}
                containerStyle={{
                  marginRight: ViewportService.commonPadding / 2,
                }}
                items={selectedPurpose == "rent" ? rentMinPrices : buyMinPrices}
                defaultSelected={
                  selectedPurpose == "rent" ? rentMinPrices[0] : buyMinPrices[0]
                }
                valueKey={"priceValue"}
                onSelect={(price: BuyRentPrice) => {
                  setMinPrice(Number(price.price));
                }}
                defaultText={"Min Price"}
                itemPrefix={currency}
              />
            </View>
            <View style={{ flex: 1 }}>
              {/* <View style={{ paddingLeft: ViewportService.commonPadding / 2 }}>
                <NormalText>{"Price to"}</NormalText>
              </View> */}

              <View style={{ marginVertical: 3 }}></View>
              <BottomLister
                height={300}
                containerStyle={{
                  marginLeft: ViewportService.commonPadding / 2,
                }}
                items={selectedPurpose == "rent" ? rentMaxPrices : buyMaxPrices}
                defaultSelected={
                  selectedPurpose == "rent" ? rentMaxPrices[0] : buyMaxPrices[0]
                }
                valueKey={"priceValue"}
                onSelect={(price: BuyRentPrice) => {
                  setMaxPrice(price.price);
                }}
                defaultText={"Max Price"}
                itemPrefix={currency}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <AppButton
        title={isFilter ? "Apply" : translate("search.search")}
        onPress={() => {
          onSearch();
        }}
        style={{ borderRadius: 0 }}
      />
      <TermSearchModel
        term={locationTerm.value}
        status={termSearchModel}
        onCancel={() => {
          setTermSearchModel(false);
        }}
        onSelect={(location: RegionLocation) => {
          setTermSearchModel(false);
          locationTerm.setValue(location.text);
          setLocationId(location.id);
        }}
      />
    </RootLayout>
  );
};
