import React, { useEffect } from "react";
import { View, Dimensions } from "react-native";
import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";
import { IAppState } from "@app/store/state-props";
import { useSelector, useDispatch } from "react-redux";
import { AppConfig } from "@app/config/app-config";
import { AuthDetails } from "@app/store/auth/auth-models";
export const AuthCheckerScreen: React.FunctionComponent = () => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);

  useEffect(() => {
    _bootstrapAsync();
  }, []);
  const dispatch = useDispatch();
  const _bootstrapAsync = async () => {
    let isAuthenticated = auth?.token ?? false;
    if (!isAuthenticated) {
      NavigationService.navigate(AppRoutes.Slider);
      return;
    } else {
      NavigationService.navigate(AppRoutes.Landing);
    }
  };
  return <View />;
};
