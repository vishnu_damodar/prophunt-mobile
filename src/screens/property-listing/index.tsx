import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, TouchableOpacity, Keyboard, Image } from "react-native";
import DeviceInfo from "react-native-device-info";
import { AuthDetails } from "@app/store/auth/auth-models";
import { RootLayout, HeaderType } from "@app/container/layout/root/root-layout";

import { IAppState } from "@app/store/state-props";
import { NormalText } from "@app/components/common/text/normal-text";
import Img from "@app/assets/images";
import { ViewportService } from "@app/config/ui/viewport-service";
import { AppColors } from "@app/config/ui/color";
import useSearch from "@app/hooks/useSearch";
import useProperty from "@app/hooks/useProperty";
import { PropertyListing } from "./components/property-list";
import BottomLister from "@app/components/common/bottom-lister";
import { NavigationService } from "@app/services/navigation-service";
import { AppRoutes } from "@app/route/route-keys";
import { useNavigationParam } from "react-navigation-hooks";
import { translate } from "@app/config/locale/i18n";
import { SetPropertySearchParamsAction } from "@app/store/search/search-actions";
import { IKeyValuePair } from "@app/components/common/widgets/form/dropdown";
const hasNotch = DeviceInfo.hasNotch();
export const PropertyListingScreen: React.FunctionComponent = (props: any) => {
  const auth: AuthDetails = useSelector((state: IAppState) => state.auth);
  const { searchParams } = useSearch();
  const { getPropertyList } = useProperty();
  const dispatch = useDispatch();
  // useEffect(() => {
  //   getPropertyList(0);
  // }, [searchParams]);
  const sortItems = [
    { key: "featured/orderby/desc", value: "Relevance" },
    { key: "price/orderby/desc", value: "Price High to Low" },
    { key: "price/orderby/asc", value: "price Low to High" },
    { key: "date/orderby/desc", value: "Newest Listing" },
    { key: "date/orderby/asc", value: "Oldest Listing" },
    { key: "sqrft/orderby/desc", value: "m2 High/Low" },
    { key: "sqrft/orderby/asc", value: "m2 Low/High" },
  ];
  const [selectedPurpose, setPurpose] = useState<
    "rent" | "buy" | string | undefined
  >(useNavigationParam("purpose"));
  const onSort = (item: IKeyValuePair) => {
    searchParams.sortBy = item.key;
    dispatch(SetPropertySearchParamsAction(searchParams));
  };
  return (
    <RootLayout
      showFooter={true}
      headerType={HeaderType.Normal}
      title={
        selectedPurpose === "buy"
          ? translate("search.buy")
          : translate("search.rent")
      }
    >
      <View
        style={{
          flexDirection: "row",
          borderBottomColor: AppColors.borderColor,
          borderBottomWidth: 1,
          //   justifyContent: "space-between",
        }}
      >
        <BottomLister
          items={sortItems}
          valueKey={"value"}
          height={350}
          onSelect={(item: IKeyValuePair) => {
            onSort(item);
          }}
          leftIcon={Img.sort}
          defaultSelected={sortItems[0]}
          defaultText={"Sort"}
          showSelected={false}
          showLeftIcon
          containerStyle={{
            flex: 1,
            justifyContent: "center",
            borderWidth: 0,
            borderRadius: 0,
            borderRightColor: AppColors.borderColor,
            borderRightWidth: 1,
          }}
          iconStyle={{
            height: 15,
            width: 15,
            resizeMode: "contain",
          }}
          labelStyle={{
            paddingHorizontal: ViewportService.smallPadding,
            fontWeight: "600",
            color: AppColors.grayTextColor,
          }}
          showIconSelectionLeft
          itemContainerStyle={{ justifyContent: "flex-start" }}
        />
        {/* <TouchableOpacity
          style={{
            flexDirection: "row",
            flex: 1,
            padding: ViewportService.commonPadding,
            alignItems: "center",
            justifyContent: "center",
            borderRightColor: AppColors.borderColor,
            borderRightWidth: 1,
          }}
        >
          <Image
            style={{
              height: 15,
              width: 15,
              paddingHorizontal: ViewportService.commonPadding,
            }}
            source={Img.sort}
            resizeMode={"contain"}
          />
          <NormalText>Sort</NormalText>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={{
            flexDirection: "row",
            flex: 1,
            padding: ViewportService.commonPadding,
            alignItems: "center",
            justifyContent: "center",
          }}
          onPress={() => {
            NavigationService.navigate(AppRoutes.Search, {
              isFilter: true,
              purpose: selectedPurpose,
            });
          }}
        >
          <Image
            style={{
              height: 15,
              width: 15,
            }}
            source={Img.filter}
            resizeMode={"contain"}
          />
          <NormalText
            style={{
              paddingHorizontal: ViewportService.smallPadding,
              fontWeight: "600",
              color: AppColors.grayTextColor,
            }}
          >
            Filter
          </NormalText>
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1 }}>
        <PropertyListing />
      </View>
    </RootLayout>
  );
};
