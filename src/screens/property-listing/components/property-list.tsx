import React, { useEffect } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { NormalText } from "@app/components/common/text/normal-text";
import useProperty from "@app/hooks/useProperty";
import { InfiniteScroller } from "@app/components/common/infinite-scroller";
import useSearch from "@app/hooks/useSearch";
import { PropertyItem } from "./property-item";
import {
  ApiResponse,
  PropertyList,
  PropertyListApiResponse,
} from "@app/services/api/models";
import usePaginatedDataLoader from "@app/hooks/usePaginatedDataLoader";
import NoResult from "@app/components/common/no-result";
import { useNavigationParam } from "react-navigation-hooks";
import Loader from "@app/components/common/Loader";
import { useSelector } from "react-redux";
import { IAppState } from "@app/store/state-props";
interface Props {}
export const PropertyListing: React.FunctionComponent<Props> = (props) => {
  const { getPropertyList } = useProperty();
  useEffect(() => {}, []);
  const locationTerm = useNavigationParam("locationTerm");
  const { searchParams } = useSearch();
  const {
    data,
    list,
    loading,
    handleScroll,
    reload,
    allPagesLoaded,
    loaded,
  } = usePaginatedDataLoader<
    ApiResponse<PropertyListApiResponse>,
    PropertyList
  >({
    loadPage: (page?: number) => getPropertyList(page || 0),
    defaultData: [],
    isError: (data: ApiResponse<PropertyListApiResponse>) => {
      return data.responseStatus === "Failure"
        ? data.message || "Unable to load property list"
        : undefined;
    },
    resolveList: (data: ApiResponse<PropertyListApiResponse>) => {
      return data.data?.propertyLists!;
    },
    startPage: 0,
  });
  useEffect(() => {
    reload()
  }, [searchParams.sortBy]);
  const loader = () => {
    if (allPagesLoaded || !loading) {
     
      return <View></View>;
    }
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          height: 40,
        }}
      >
        <ActivityIndicator animating size="small"></ActivityIndicator>
      </View>
    );
  };
  const listitems: PropertyList[] = list || [];
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        style={{}}
        data={listitems ?? []}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => `property_${item.propertyID}`}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={reload} />
        }
        onEndReached={() => handleScroll("pageDown")}
        onEndReachedThreshold={0}
        ListFooterComponent={loader}
        ListEmptyComponent={!loaded || loading ? null : <NoResult />}
        renderItem={({ item }) => {
          return (
            <PropertyItem
              key={`propertyItem_${item.propertyID}`}
              item={item}
              imageURL={data.data?.imageURL ?? ""}
              companyLogoPath={data.data?.companyLogoPath ?? ""}
            />
          );
        }}
        ListHeaderComponent={() => {
          return (
            <View style={{ padding: ViewportService.commonPadding }}>
              {locationTerm ? (
                <SubHeading style={{ textAlign: "left" }}>
                  Properties for Sale in {locationTerm ?? ""}
                </SubHeading>
              ) : null}
            </View>
          );
        }}
      />
    </View>
  );
};
