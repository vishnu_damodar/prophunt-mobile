import React, { useState } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { styles } from "../style";
import { ViewportService } from "@app/config/ui/viewport-service";
import { SubHeading } from "@app/components/common/text/sub-heading";
import { NormalText } from "@app/components/common/text/normal-text";
import { PropertyList } from "@app/services/api/models";
import { Item } from "native-base";
import Img from "@app/assets/images";
import { AppColors } from "@app/config/ui/color";
import useAppSettings from "@app/hooks/useAppSettings";
import { decodeEntity, encode } from "html-entities";
import AppButton from "@app/components/common/AppButton";
import { DateUtils } from "@app/utils/date-utils";
interface Props {
  item: PropertyList;
  imageURL: string;
  companyLogoPath: string;
}
export const PropertyItem: React.FunctionComponent<Props> = (props) => {
  const { currency, unit } = useAppSettings();
  const property = props.item;
  const [isImageErr, setImageErr] = useState(false);
  const [hasCompanyImageError, setCompanyImageError] = useState(false);
  return (
    <View
      style={{
        marginBottom: 5,
        paddingHorizontal: ViewportService.commonPadding,
        paddingVertical: ViewportService.commonPadding * 0.5,
      }}
    >
      <View
        style={{
          borderColor: AppColors.borderColor,
          borderRadius: 4,
          borderWidth: 0.7,
        }}
      >
        <View>
          <Image
            style={{
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
              height: ViewportService.width / 2,
              width: ViewportService.width / 1.09,
            }}
            source={
              property.propertyCoverImage && !isImageErr
                ? { uri: `${props.imageURL}${property.propertyCoverImage}` }
                : Img.placeholder
            }
            onError={(error) => {
              setImageErr(true);
            }}
            resizeMode={isImageErr ? "contain" : "cover"}
          />
          {property?.propertyFeatured && property?.propertyFeatured == "ON" && (
            <View
              style={{
                position: "absolute",
                backgroundColor: AppColors.redColor,
                paddingVertical: ViewportService.smallPadding / 2,
                paddingHorizontal: ViewportService.smallPadding,
                borderRadius: 4,
                margin: ViewportService.commonPadding,
              }}
            >
              <SubHeading color={AppColors.lightColor} bold>
                Featured
              </SubHeading>
            </View>
          )}
          {property.propertySoldOutStatus !== "No" &&
            property?.propertySoldOutDate && (
              <View
                style={{
                  position: "absolute",
                  backgroundColor: AppColors.primaryThemeColor,
                  paddingVertical: ViewportService.smallPadding / 2,
                  paddingHorizontal: ViewportService.smallPadding,
                  borderRadius: 4,
                  margin: ViewportService.commonPadding,
                  bottom: 0,
                }}
              >
                <SubHeading
                  color={AppColors.lightColor}
                  bold
                  style={{ fontSize: ViewportService.normalFont }}
                >
                  {"Sold Out On"}{" "}
                  {DateUtils.formatDate(
                    property?.propertySoldOutDate,
                    "MMM DD,yyyy"
                  )}
                </SubHeading>
              </View>
            )}
        </View>
        <View
          style={{
            padding: ViewportService.commonPadding,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <SubHeading bold>
            {currency} {property.propertyPrice ?? ""}
          </SubHeading>
          <TouchableOpacity
            onPress={() => {}}
            style={{
              borderWidth: 0.7,
              borderRadius: 4,
              borderColor: AppColors.borderColor,
              padding: ViewportService.smallPadding,
            }}
          >
            <Image
              source={Img.share}
              style={{ height: 20, width: 20 }}
              resizeMode={"contain"}
            />
          </TouchableOpacity>
        </View>
        <View style={{ paddingHorizontal: ViewportService.commonPadding }}>
          <SubHeading bold>{property.propertyName ?? ""}</SubHeading>
          <NormalText
            numberOfLines={3}
            style={{
              width: ViewportService.width / 1.2,
              color: AppColors.grayTextColor,
              paddingVertical: ViewportService.commonPadding,
            }}
          >
            {property.propertyDescription ?? ""}
          </NormalText>
        </View>
        <View
          style={{
            flexDirection: "row",
            padding: ViewportService.commonPadding,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Image
              source={Img.bed}
              resizeMode={"contain"}
              style={{ height: 25, width: 25 }}
            />
            <NormalText
              style={{ paddingHorizontal: ViewportService.smallPadding }}
            >
              {property.propertyBedRoom ?? "1"}
            </NormalText>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              marginHorizontal: ViewportService.commonPadding,
            }}
          >
            <Image
              source={Img.bathtub}
              resizeMode={"contain"}
              style={{ height: 25, width: 25 }}
            />
            <NormalText
              style={{ paddingHorizontal: ViewportService.smallPadding }}
            >
              {property.propertyBathRoom ?? "1"}
            </NormalText>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Image
              source={Img.plan}
              resizeMode={"contain"}
              style={{ height: 15, width: 15 }}
            />
            <NormalText
              style={{ paddingHorizontal: ViewportService.smallPadding }}
            >
              {property.propertySqrFeet ?? ""} {unit}
            </NormalText>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: ViewportService.commonPadding,
            flexDirection: "row",
            paddingVertical: ViewportService.commonPadding,
            borderTopColor: AppColors.borderColor,
            borderTopWidth: 1,
            alignItems: "center",
          }}
        >
          <View
            style={{
              backgroundColor: AppColors.lightGray,
              padding: 2,
              borderRadius: 20,
            }}
          >
            <Image
              source={
                property.projectUserCompanyLogo && !hasCompanyImageError
                  ? {
                      uri: `${props.companyLogoPath}${property.projectUserCompanyLogo}`,
                    }
                  : Img.placeholder
              }
              onError={() => {
                setCompanyImageError(true);
              }}
              resizeMode={
                !property.projectUserCompanyLogo || hasCompanyImageError
                  ? "contain"
                  : "cover"
              }
              style={{ height: 40, width: 40, borderRadius: 20 }}
            />
          </View>

          <NormalText style={{ padding: ViewportService.smallPadding }}>
            {property.projectUserCompanyName ?? ""}
          </NormalText>
        </View>
        <AppButton
          title={"Details"}
          onPress={() => {}}
          style={{ margin: ViewportService.commonPadding }}
        />
      </View>
    </View>
  );
};
