import { OORJIT_API_DOMAIN } from "@app/config/app-config";
import ApiClient from "./client";

const api = new ApiClient(OORJIT_API_DOMAIN);

export default api;