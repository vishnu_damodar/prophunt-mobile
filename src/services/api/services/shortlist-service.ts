import { plainToClassFromExist } from "class-transformer";
import ApiService from "./base-service";
import { ApiResponse } from "../models";
import { ShortlistedAgentResult, ShortlistedCountResult, ShortlistedDeveloperResult, ShortlistedExpertResult, ShortlistedProjectResult, ShortlistedPropertyResult } from "../models/shortlist";
import { UnFavoriteParamModel } from "@app/store/shortlist/shortlist-models";
export default class ShortlistService extends ApiService {

  public getShortlistedCount = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedCountResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/shortListedCount/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ShortlistedCountResult>(ShortlistedCountResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedCountResult>(ShortlistedCountResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getShortlistedProperties = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedPropertyResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/favouriteProperties/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ShortlistedPropertyResult>(ShortlistedPropertyResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedPropertyResult>(ShortlistedPropertyResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getShortlistedProjects = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedProjectResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/favouriteProjects/",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<ShortlistedProjectResult>(ShortlistedProjectResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedProjectResult>(ShortlistedProjectResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getShortlistedAgents = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedAgentResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/favouriteAgents/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ShortlistedAgentResult>(ShortlistedAgentResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedAgentResult>(ShortlistedAgentResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getShortlistedDevelopers = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedDeveloperResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/favouriteBuilders/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ShortlistedDeveloperResult>(ShortlistedDeveloperResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedDeveloperResult>(ShortlistedDeveloperResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getShortlistedExperts = async (
    userId?: string
  ): Promise<ApiResponse<ShortlistedExpertResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/favouriteExperts/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ShortlistedExpertResult>(ShortlistedExpertResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ShortlistedExpertResult>(ShortlistedExpertResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public unFavoriteProperty = async (
    params?: UnFavoriteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("propertyID", params?.id);

    try {
      const response = await this.http.postFormData(
        "dashboard/unFavourProp/",
        formData
      );
      
      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };

  public unFavoriteProject = async (
    params?: UnFavoriteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("projectID", params?.id);

    try {
      const response = await this.http.postFormData(
        "dashboard/unFavourProj/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };

  public unFavoriteUser = async (
    params?: UnFavoriteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("favouriteID", params?.id);

    try {
      const response = await this.http.postFormData(
        "dashboard/UnFavourUser/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };

}
