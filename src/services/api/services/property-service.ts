import { plainToClassFromExist } from "class-transformer";
import ApiService from "./base-service";
import { ApiResponse, PropertyListApiResponse } from "../models";
import { PropertySearchParamModel } from "@app/store/search/search-models";
import { PauseResumeDeleteParamModel } from "@app/store/dashboard/dashboard-models";
export default class SearchService extends ApiService {
  public getPropertyList = async (
    params: PropertySearchParamModel
  ): Promise<ApiResponse<PropertyListApiResponse>> => {
    const formData = new FormData();

    for (var key in params) {
      formData.append(key, params[key]);
    }

    try {
      const response = await this.http.postFormData(
        "property/searchList",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<PropertyListApiResponse>(PropertyListApiResponse),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<PropertyListApiResponse>(PropertyListApiResponse),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public deleteProperty = async (
    params?: PauseResumeDeleteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();
    formData.append("userID", params?.userID);
    formData.append("propID", params?.id);
    formData.append("newstatus", params?.status);
    try {
      const response = await this.http.postFormData(
        "dashboard/statusChange",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };
}
