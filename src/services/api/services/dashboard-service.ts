import { plainToClassFromExist } from "class-transformer";
import ApiService from "./base-service";
import { ApiResponse } from "../models";
import { EnquiriesResult, MyPropertiesResult, ProfileResult, SavedSearchResult } from "../models/dashboard";
import { ChangePasswordParamModel, ListParamModel, PauseResumeDeleteParamModel, SaveProfileParamModel } from "@app/store/dashboard/dashboard-models";
export default class DashboardService extends ApiService {

  public getSavedSearchList = async (
    params?: ListParamModel
  ): Promise<ApiResponse<SavedSearchResult>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("pageID", params?.page);

    try {
      const response = await this.http.postFormData(
        "dashboard/savedSearchesList",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<SavedSearchResult>(SavedSearchResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<SavedSearchResult>(SavedSearchResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public pauseResumeSavedSearch = async (
    params?: PauseResumeDeleteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();
    formData.append("userID", params?.userID);
    formData.append("searchID", params?.id);
    formData.append("newstatus", params?.status);
    try {
      const response = await this.http.postFormData(
        "dashboard/savedSearchStatus",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };

  public deleteSavedSearch = async (
    params?: PauseResumeDeleteParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();
    formData.append("userID", params?.userID);
    formData.append("searchID", params?.id);
    try {
      const response = await this.http.postFormData(
        "dashboard/savedSearchDelete",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response
        );
      } else {
        throw error;
      }
    }
  };

  public getProfileData = async (
    userId?: string
  ): Promise<ApiResponse<ProfileResult>> => {
    const formData = new FormData();

    formData.append("userID", userId);

    try {
      const response = await this.http.postFormData(
        "dashboard/editProfile",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<ProfileResult>(ProfileResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<ProfileResult>(ProfileResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public changePassword = async (
    params?: ChangePasswordParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("userPassword", params?.userPassword);
    formData.append("userNewPassword", params?.userNewPassword);
    formData.append("userConfirmPassword", params?.userConfirmPassword);

    try {
      const response = await this.http.postFormData(
        "dashboard/changePassword/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public saveProfile = async (
    params?: SaveProfileParamModel
  ): Promise<ApiResponse<any>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("userFirstName", params?.userFirstName);
    formData.append("userLastName", params?.userLastName);
    formData.append("userGender", params?.userGender);
    formData.append("userPhone", params?.userPhone);
    formData.append("googleCountryName", params?.googleCountryName);
    formData.append("googleStateName", params?.googleStateName);
    formData.append("googleCityName", params?.googleCityName);
    formData.append("googleLocality", params?.googleLocality);
    formData.append("countryCode", params?.countryCode);
    formData.append("userAddress1", params?.userAddress1);
    formData.append("userAddress2", params?.userAddress2);
    formData.append("userEmail", params?.userEmail);
    formData.append("userZip", params?.userZip);
    formData.append("userProfilePicture", params?.userProfilePicture);

    try {
      const response = await this.http.postFormData(
        "dashboard/updateProfile/",
        formData
      );

      return plainToClassFromExist(
        new ApiResponse<any>(),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<any>(),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getMyPropertiesList = async (
    params?: ListParamModel
  ): Promise<ApiResponse<MyPropertiesResult>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    // formData.append("pageID", params?.page);

    try {
      const response = await this.http.postFormData(
        "dashboard/myPropertyList",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<MyPropertiesResult>(MyPropertiesResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<MyPropertiesResult>(MyPropertiesResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getEnquiriesList = async (
    params?: ListParamModel
  ): Promise<ApiResponse<EnquiriesResult>> => {
    const formData = new FormData();

    formData.append("userID", params?.userID);
    formData.append("pageID", params?.page);

    try {
      const response = await this.http.postFormData(
        "dashboard/propertyEnqList/",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<EnquiriesResult>(EnquiriesResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<EnquiriesResult>(EnquiriesResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

}
