import { HttpClient } from "../http/http";
import ApiServiceDataStore from "../interfaces/api-data-store";

export default class ApiService {
    public store: ApiServiceDataStore;

    public http: HttpClient;

    constructor(store: ApiServiceDataStore, http: HttpClient) {
        this.store = store;
        this.http = http;
    }

    get token() {
        return this.store.token;
    }

    get apiDomain() {
        return this.store.apiDomain;
    }
}
