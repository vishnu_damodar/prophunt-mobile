import { ILoginModel, IRegisterModel } from "@app/store/auth/auth-models";
import { plainToClassFromExist } from "class-transformer";
import { ApiResponse, AuthUserLoginResult, LoginResult } from "../models";
import ApiService from "./base-service";

export default class AuthService extends ApiService {
  public loginWithPassword = async (
    userName: string,
    password: string,
    deviceToken: string,
    deviceId: string
  ): Promise<ApiResponse<AuthUserLoginResult>> => {
    const formData = new FormData();
    formData.append("userName", userName);
    formData.append("userPassword", password);
    formData.append("deviceToken", deviceToken);
    // formData.append("deviceId",deviceId);
    try {
      const response = await this.http.postFormData("user/password/", formData);
      return plainToClassFromExist(
        new ApiResponse<AuthUserLoginResult>(AuthUserLoginResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<AuthUserLoginResult>(AuthUserLoginResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public loginApi = async (
    loginDetails: ILoginModel
  ): Promise<ApiResponse<LoginResult>> => {
    const formData = new FormData();
    formData.append("userLoginEmail", loginDetails.username);
    formData.append("userLoginPassword", loginDetails.password);
    // formData.append("deviceId",deviceId);
    try {
      const response = await this.http.postFormData(
        "login/checkUserLogin/",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<LoginResult>(LoginResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<LoginResult>(LoginResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public signupApi = async (
    signupDetails: IRegisterModel
  ): Promise<ApiResponse<LoginResult>> => {
    const formData = new FormData();
    for (let key in signupDetails) {
      formData.append(key, signupDetails[key]);
    }
    // formData.append("deviceId",deviceId);
    try {
      const response = await this.http.postFormData(
        "login/usersignup/",
        formData
      );
      return plainToClassFromExist(new ApiResponse<LoginResult>(LoginResult), response.data);
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<LoginResult>(LoginResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };
}
