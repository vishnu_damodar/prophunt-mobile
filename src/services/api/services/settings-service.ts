import { plainToClassFromExist } from "class-transformer";
import ApiService from "./base-service";
import {
  ApiResponse,
  CityList,
  CityListResponse,
  CommonListingResult,
  LanguageResult,
} from "../models";
import { AppConfig } from "@app/config/app-config";
export default class SettingsService extends ApiService {
  public getLanguageList = async (): Promise<ApiResponse<LanguageResult>> => {
    const url = `index/languageList/`;
    try {
      const response = await this.http.get(url);
      return plainToClassFromExist(
        new ApiResponse<LanguageResult>(LanguageResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<LanguageResult>(LanguageResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };
  public getCommonListing = async (): Promise<
    ApiResponse<CommonListingResult>
  > => {
    const formData = new FormData();
    formData.append("countryID", AppConfig.countryId);
    try {
      const response = await this.http.postFormData(
        "index/commonListing",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<CommonListingResult>(CommonListingResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<CommonListingResult>(CommonListingResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getCityList = async (): Promise<ApiResponse<CityListResponse>> => {
    const formData = new FormData();
    formData.append("countryID", AppConfig.countryId);
    try {
      const response = await this.http.postFormData("index/cityList", formData);
      return plainToClassFromExist(
        new ApiResponse<CityListResponse>(CityListResponse),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<CityListResponse>(CityListResponse),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };
}
