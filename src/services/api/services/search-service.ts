import { plainToClassFromExist } from "class-transformer";
import ApiService from "./base-service";
import {
  ApiResponse,
  Attribute,
  CityList,
  CityListResponse,
  CommonListingResult,
  LanguageResult,
  RegionResult,
  AttributesResponse,
  BedroomResponse,
} from "../models";
import { AppConfig } from "@app/config/app-config";
export default class SearchService extends ApiService {
  public getRegionApi = async (
    term: string,
    isProject?: boolean
  ): Promise<ApiResponse<RegionResult>> => {
    const formData = new FormData();
    formData.append("countryID", AppConfig.countryId);
    formData.append("selectedCity", "1");
    formData.append("location", "");
    formData.append("keyword", term);
    if (isProject) {
      formData.append("projectList", "1");
    }
    try {
      const response = await this.http.postFormData(
        "index/listRegion",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<RegionResult>(RegionResult),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<RegionResult>(RegionResult),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getAttributesApi = async (
    typeId: string
  ): Promise<ApiResponse<AttributesResponse>> => {
    const formData = new FormData();
    formData.append("typeID", typeId);
    try {
      const response = await this.http.postFormData(
        "property/bindPropertyAttributes",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<AttributesResponse>(AttributesResponse),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<AttributesResponse>(AttributesResponse),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };

  public getBedroomsApi = async (
    typeId: string
  ): Promise<ApiResponse<BedroomResponse>> => {
    const formData = new FormData();
    formData.append("typeID", typeId);
    try {
      const response = await this.http.postFormData(
        "index/bindPropertyBedroom",
        formData
      );
      return plainToClassFromExist(
        new ApiResponse<BedroomResponse>(BedroomResponse),
        response.data
      );
    } catch (error) {
      if (error.response) {
        return plainToClassFromExist(
          new ApiResponse<BedroomResponse>(BedroomResponse),
          error.response.data
        );
      } else {
        throw error;
      }
    }
  };


}


