export default interface ApiServiceDataStore {
    token?: string;
    apiDomain: string;
    languageId: string;
    webSiteId: string;
    subSiteId: string;
}