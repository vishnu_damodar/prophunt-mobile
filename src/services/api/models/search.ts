import { StringToBooleanTransform } from "@app/utils/transforms/BooleanTransform";
import { Exclude, Expose, Transform, Type } from "class-transformer";

@Exclude()
export class RegionResult {
  @Expose()
  @Type(() => RegionLocation)
  location!: RegionLocation[];

  @Expose()
  @Type(() => RegionLocation)
  projects!: RegionLocation[];
}

@Exclude()
export class RegionLocation {
  @Expose()
  id!: string;
  @Expose()
  originalText!: string;
  @Expose()
  text!: string;
}

@Exclude()
export class AttributesResponse {
  @Expose()
  @Type(() => Attribute)
  allAttributes!: Attribute[];
}

@Exclude()
export class Attribute {
  @Expose()
  attrName!: string;
  @Expose()
  attrID!: string;
  @Expose()
  selection!: string;
  @Expose()
  attrGroupName!: string;
  @Expose()
  propertyAttrSelectedOptionID!: string;
  @Expose()
  propertyTypeID!: string;
  @Expose()
  attrOptName!: string;
}

@Exclude()
export class BedroomResponse {
  @Expose()
  @Type(() => BedRoomAttribute)
  bedroom!: BedRoomAttribute[];
}

@Exclude()
export class BedRoomAttribute {
  @Expose()
  attributeID!: string;
  @Expose()
  attrOptionID!: string;
  @Expose()
  attrOptName!: string;
}
