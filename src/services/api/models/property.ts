import { StringToBooleanTransform } from "@app/utils/transforms/BooleanTransform";
import { Exclude, Expose, Transform, Type } from "class-transformer";

@Exclude()
export class PropertyListApiResponse {
  @Expose()
  // @Transform(Number)
  totalCount!: string;

  @Expose()
  // @Transform(Number)
  searchProjectCount!: string;

  @Expose()
  // @Transform(Number)
  searchPropertyCount!: string;

  @Expose()
  // @Transform(Number)
  searchAgentCount!: string;

  @Expose()
  imageURL!: string;

  @Expose()
  companyLogoPath!: string;

  @Expose()
  siteUrl!: string;
  @Expose()
  @Type(() => PropertyList)
  propertyLists!: PropertyList[];
}

@Exclude()
export class PropertyList {
  @Expose()
  itemType!: string;
  @Expose()
  propertyID!: string;
  @Expose()
  projectID!: string;
  @Expose()
  propertyType!: string;
  @Expose()
  propertyName!: string;
  @Expose()
  projectName?: string;
  @Expose()
  imageCount!: string;
  @Expose()
  propertyFeatured!: string;
  @Expose()
  projectBuilderName?: string;
  @Expose()
  projectFeatured?: string;
  @Expose()
  propertyKey!: string;
  @Expose()
  projectKey?: string;
  @Expose()
  propertyPurpose!: string;
  @Expose()
  cityID!: string;
  @Expose()
  cityLocationID!: string;
  @Expose()
  cityLocName!: string;
  @Expose()
  propertyCurrentStatus!: string;
  @Expose()
  projectCurrentStatus?: string;
  @Expose()
  propertyAddedDate!: string;
  @Expose()
  propertyUpdateDate!: string;
  @Expose()
  projectAddedDate!: string;
  @Expose()
  propertyDescription!: string;
  @Expose()
  projectDescription?: string;
  @Expose()
  furnishingStatus?: any;
  @Expose()
  propertyTypeName!: string;
  @Expose()
  isNegotiable!: string;
  @Expose()
  proLatitude!: string;
  @Expose()
  proLongitude!: string;
  @Expose()
  possessionDate!: string;
  @Expose()
  projectPossessionDate?: string;
  @Expose()
  isOffer!: string;
  @Expose()
  propertyOfferStartTime!: string;
  @Expose()
  propertyOfferEndTime!: string;
  @Expose()
  propertyOfferDiscount!: string;
  @Expose()
  propertySoldOutStatus!: string;
  @Expose()
  propertySoldOutPrice!: string;
  @Expose()
  propertySoldOutDate!: string;
  @Expose()
  openhouseStatus!: string;
  @Expose()
  projectUserCompanyName!: string;
  @Expose()
  projectUserCompanyLogo!: string;
  @Expose()
  propertyFavourite!: string;
  @Expose()
  projectFavourite!: string;
  @Expose()
  projectCoverImage?: string;
  @Expose()
  propertyCoverImage!: string;
  @Expose()
  propertyBedRoom!: string;
  @Expose()
  propertySqrFeet!: string;
  @Expose()
  propertyPlotArea?: any;
  @Expose()
  plotareaUnit?: any;
  @Expose()
  areaType?: any;
  @Expose()
  planPropertyDetails!: string;
  @Expose()
  planEnquiry!: string;
  @Expose()
  userTypeID!: string;
  @Expose()
  userID!: string;
  @Expose()
  exact?: any;
  @Expose()
  type!: string;
  @Expose()
  userPlanID!: string;
  @Expose()
  propertyBathRoom!: string;
  @Expose()
  propertyTypeKey!: string;
  @Expose()
  propertyTypeIcon!: string;
  @Expose()
  propertyplanEnquiryCountBalance!: string;
  @Expose()
  planEnquiryCountBalance!: string;
  @Expose()
  currencyID_1!: string;
  @Expose()
  currencyID_5!: string;
  @Expose()
  currencyID_7!: string;
  @Expose()
  propertyPrices!: PropertyPrices;
  @Expose()
  pricesPerSqfts!: PricesPerSqfts;
  @Expose()
  pricePerUnitValue!: string;
  @Expose()
  pricePerUnit!: string;
  @Expose()
  pricePerSqft!: string;
  @Expose()
  propertyPrice!: string;
  @Expose()
  propertyPriceMap!: string;
  @Expose()
  link!: string;
}

interface PricesPerSqfts {
  "1": number;
  "5": number;
  "7": number;
}

interface PropertyPrices {
  "1": string;
  "5": string;
  "7": string;
}
