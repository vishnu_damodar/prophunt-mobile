import { Exclude, Expose, Type } from "class-transformer";

@Exclude()
export class ShortlistedPropertyResult {
  @Expose({ name: "totalcount" })
  // @Transform(Number)
  totalCount!: Number;
  @Expose()
  imgPath!: string;
  @Expose({ name: "propertylist" })
  @Type(() => ShortlistedProperty)
  propertyList!: ShortlistedProperty[];
}

@Exclude()
export class ShortlistedProperty {
  @Expose()
  propertyID!: string;
  @Expose()
  propertyKey!: string;
  @Expose()
  userID!: string;
  @Expose()
  propertyTypeID!: string;
  @Expose()
  propertyFeatured!: string;
  @Expose()
  propertyPurpose!: string;
  @Expose()
  propertyLocality!: string;
  @Expose()
  propCoverImage!: string;
  @Expose()
  favoriteDate!: string;
  @Expose()
  propertyName!: string;
  @Expose()
  propertyDescription!: string;
  @Expose()
  communityID!: string;
  @Expose()
  subCommunityID?: string;
  @Expose()
  propertyZipCode!: string;
  @Expose()
  propertyStatus?: string;
  @Expose()
  projectID?: string;
  @Expose()
  type?: string;
  @Expose()
  isNegotiable!: string;
  @Expose()
  propertyMetaTitle!: string;
  @Expose()
  propertyAddedDate!: string;
  @Expose()
  propertyCurrentStatus?: string;
  @Expose()
  possessionDate!: string;
  @Expose()
  planExpiryDate!: string;
  @Expose()
  planEnquiry!: string;
  @Expose()
  planPropertyDetails!: string;
  @Expose()
  planEnquiryCountBalance?: string;
  @Expose()
  userTypeID?: String;
  @Expose()
  userEmail!: string;
  @Expose()
  propertyAddress1!: string;
  @Expose()
  propertyAddress2!: string;
  @Expose()
  propertyLatitude!: string;
  @Expose()
  propertyLongitude!: string;
  @Expose()
  cityID?: string;
  @Expose()
  stateID!: string;
  @Expose()
  countryID!: string;
  @Expose()
  cityLocID!: string;
  @Expose()
  userFirstname!: string;
  @Expose()
  userLastName!: string;
  @Expose()
  userCompanyName!: string;
  @Expose()
  propertyTypeKey!: string;
  @Expose()
  propertyTypeName!: string;
  @Expose()
  propertyPrice!: string;
  @Expose()
  currencySymbolLeft!: string;
  @Expose()
  currencySymbolRight!: string;
  @Expose()
  countryName!: string;
  @Expose()
  expiryStatus?: string;
  @Expose()
  date_diff!: string;
  @Expose()
  projectUserCompanyName!: string;
  @Expose()
  projectName!: string;
  @Expose()
  projectBuilderName?: string;
  @Expose()
  propertyBedRoom?: string;
  @Expose()
  propertySqrFeet?: string;
  @Expose()
  propertyBuiltUpArea!: string;
  @Expose()
  furnishingStatus!: string;
  @Expose()
  cityLocName!: string;
  @Expose()
  stateName?: string;
}


@Exclude()
export class ShortlistedCountResult {
  @Expose()
  favPropertyCount!: Number;
  @Expose()
  favProjectCount!: Number;
  @Expose()
  favAgentCount!: Number;
  @Expose()
  favBuilderCount!: Number;
  @Expose()
  favExpertCount!: Number;
  @Expose()
  totalFavorites!: Number;
  @Expose()
  settingsPhoneNum!: String;
  @Expose()
  settingsEmail!: String;
}

@Exclude()
export class ShortlistedProjectResult {
  @Expose({ name: "totalcount" })
  totalCount!: Number;
  @Expose()
  imgPath!: string;
  @Expose({ name: "projectlist" })
  @Type(() => ShortlistedProject)
  projectList!: ShortlistedProject[];
}

@Exclude()
export class ShortlistedProject {
  @Expose()
  projectID!: string;
  @Expose()
  cityID!: string;
  @Expose()
  stateID!: string;
  @Expose()
  cityLocID!: string;
  @Expose()
  projectKey!: string;
  @Expose()
  projectTypeID!: string;
  @Expose()
  projectLatitude!: string;
  @Expose()
  projectLongitude!: string;
  @Expose()
  projectCurrentStatus!: string;
  @Expose()
  possessionDate!: string;
  @Expose()
  projectName!: string;
  @Expose()
  projectAddress1!: string;
  @Expose()
  projectAddress2!: string;
  @Expose()
  projectDescription!: string;
  @Expose()
  projectShortDescription!: string;
  @Expose()
  projectMetaTitle!: string;
  @Expose()
  projectMetaKeyword!: string;
  @Expose()
  projectMetaDescription!: string;
  @Expose()
  propertyTypeName!: string;
  @Expose()
  propertyTypeKey!: string;
  @Expose()
  projectBuilderName!: string;
  @Expose()
  userID!: string;
  @Expose()
  userTypeID!: string;
  @Expose()
  userCompanyLogo!: string;
  @Expose()
  userFullName!: string;
  @Expose()
  userFirstName!: string;
  @Expose()
  userLastName!: string;
  @Expose()
  userDesignation!: string;
  @Expose()
  userCompanyUrlKey!: string;
  @Expose()
  userCompanyName!: string;
  @Expose()
  userPhone!: string;
  @Expose()
  userAddress1!: string;
  @Expose()
  projectAddedDate!: string;
  @Expose()
  userAddress2!: string;
  @Expose()
  userZip!: string;
  @Expose()
  userAboutMe!: string;
  @Expose()
  userSkills!: string;
  @Expose()
  planEnquiry!: string;
  @Expose()
  planEnquiryCountBalance!: string;
  @Expose()
  planPropertyDetails!: string;
  @Expose()
  planBusinessLogo!: string;
  @Expose()
  cityKeyID!: string;
  @Expose()
  stateUrlKey!: string;
  @Expose()
  cityName!: string;
  @Expose()
  stateName!: string;
  @Expose()
  googleLocName!: string;
  @Expose()
  projCoverImage!: string;
  @Expose()
  projectUserCompanyName?: string;
  @Expose()
  propertyUnitPriceRange!: string;
  @Expose()
  propertyUnitSqftRange!: string;
  @Expose()
  furnishingStatus?: string;
}

@Exclude()
export class ShortlistedAgentResult {
  @Expose({ name: "totalcount" })
  totalCount!: Number;
  @Expose()
  imgPath!: string;
  @Expose({ name: "agentlist" })
  @Type(() => ShortlistedItemData)
  agentList!: ShortlistedItemData[];
}

@Exclude()
export class ShortlistedItemData {
  @Expose()
  favouriteID!: string;
  @Expose()
  favouriteUserID!: string;
  @Expose()
  cityID!: string;
  @Expose()
  stateID!: string;
  @Expose()
  cityLocID!: string;
  @Expose()
  userEmail!: string;
  @Expose()
  userUrlKey!: string;
  @Expose()
  userRegNumber!: string;
  @Expose()
  userProfilePicture!: string;
  @Expose()
  userCompanyLogo!: string;
  @Expose()
  usercompanyAgentBanner!: string;
  @Expose()
  userFirstName!: string;
  @Expose()
  userLastName!: string;
  @Expose()
  userCompanyName!: string;
  @Expose()
  userPhone!: string;
  @Expose()
  userAddress1!: string;
  @Expose()
  cityKeyID!: string;
  @Expose()
  stateUrlKey!: string;
  @Expose()
  cityName!: string;
  @Expose()
  stateName!: string;
  @Expose()
  googleLocName!: string;
}

@Exclude()
export class ShortlistedDeveloperResult {
  @Expose({ name: "totalcount" })
  totalCount!: Number;
  @Expose()
  imgPath!: string;
  @Expose({ name: "builderlist" })
  @Type(() => ShortlistedItemData)
  developerList!: ShortlistedItemData[];
}

@Exclude()
export class ShortlistedExpertResult {
  @Expose({ name: "totalcount" })
  totalCount!: Number;
  @Expose()
  imgPath!: string;
  @Expose({ name: "expertlist" })
  @Type(() => ShortlistedItemData)
  expertList!: ShortlistedItemData[];
}

