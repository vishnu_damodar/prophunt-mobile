import { Exclude, Expose, Transform, Type } from "class-transformer";

@Exclude()
export class ApiResponse<T = String> {
  private type: Function;

  @Expose({ name: "response" })
  responseStatus!: "Success" | "Failure";

  @Expose({ name: "message" })
  message?: string;

  @Expose({ name: "data" })
  @Type((options) => {
    return (options?.newObject as ApiResponse<T>).type;
  })
  data?: T;

  @Expose({ name: "error" })
  @Transform(Boolean)
  isError?: boolean;

  @Expose({ name: "code" })
  responseStatusCode!: number;

  constructor(type: Function = String) {
    this.type = type;
  }

  get isSuccess() {
    return this.responseStatus === "Success";
  }
}
