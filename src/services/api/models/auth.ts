import { Exclude, Expose, Transform } from "class-transformer";

@Exclude()
export class LoginResult {
  @Expose({ name: "acessToken" })
  token!: string;

  @Expose({ name: "userID" })
  userId!: string;

  @Expose()
  userTypeID!: string;

  @Expose()
  userEmail!: string;

  @Expose()
  userFirstname!: string;

  @Expose()
  userLastname!: string;

  @Expose()
  userPhone!: string;

  @Expose()
  imagePath!: string;

  @Expose()
  userProfilePicture!: string;

  @Expose({ name: "OTP" })
  otp?: boolean;

  @Expose()
  countryCode!: string;

  @Expose()
  dialCode!: string;
}
