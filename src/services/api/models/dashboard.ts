import { Exclude, Expose, Type } from "class-transformer";

@Exclude()
export class SavedSearchResult {
    @Expose({ name: "totalcount" })
    totalCount!: Number;
    @Expose({ name: "savedSearchLists" })
    @Type(() => SavedSearch)
    savedSearchList!: SavedSearch[];
}

@Exclude()
export class SavedSearch {
    @Expose()
    searchID!: string;
    @Expose()
    searchTitle!: string;
    @Expose()
    searchStatus!: string;
    @Expose()
    searchParameters!: SearchParameters;
    @Expose()
    searchDate!: string;
    @Expose()
    searchType!: string;
    @Expose()
    matchCount!: string;
    @Expose()
    searchUrl!: string;
    @Expose()
    saveSearchParameter!: SaveSearchParameter;
    @Expose()
    searchMinPriceValue!: string;
    @Expose()
    searchMaxPriceValue!: string;
    @Expose()
    locationName!: string;
    @Expose()
    StatusValue!: string;
    @Expose()
    projectName!: string;
    @Expose()
    bhkRes!: string;
    propertyID: any;
}

@Exclude()
export class SaveSearchParameter {
    @Expose()
    countryName!: string;
    @Expose()
    propertyTypeName!: string;
    @Expose()
    purposeText!: string;
    @Expose()
    searchPropertyPurpose?: any;
    @Expose()
    attrDetails!: AttrDetails;
    @Expose()
    minSearchPrice!: string;
    @Expose()
    maxSearchPrice!: string;
    @Expose()
    priceRange!: string[];
    @Expose()
    searchKeyword!: string;
    @Expose()
    searchRegion!: string;
    @Expose()
    commonMetaTitle!: string;
    @Expose()
    commonMetaDescription!: string;
    @Expose()
    regionName!: string;
    @Expose()
    searchLocation!: string;
}

@Exclude()
export class AttrDetails {
    @Expose({ name: "Bed Rooms" })
    bedrooms!: string;
    @Expose({ name: "Furnished" })
    Furnished: string = '';
}

@Exclude()
export class SearchParameters {
    @Expose()
    parameters?: any;
    @Expose()
    languageID!: string;
    @Expose()
    languageKey!: string;
    @Expose()
    searchCountryID!: string;
    @Expose()
    searchCountryKey!: string;
    @Expose()
    purpose?: any;
    @Expose()
    searchPropertyTypeKey?: any;
    @Expose()
    searchPropertyAdded!: string;
    @Expose()
    searchKeyword!: string;
    @Expose()
    searchRegion!: string;
    @Expose()
    searchPropertyPurpose?: any;
    @Expose()
    searchMinPrice!: string;
    @Expose()
    searchMaxPrice!: string;
    @Expose()
    searchPropertyTypeID!: string;
    @Expose()
    searchAttributesStr!: string;
    @Expose()
    searchType!: string;
    @Expose()
    searchAgencyIDs?: any;
    @Expose()
    searchUserTypeID?: any;
    @Expose()
    alertPropertyIDs?: any;
    @Expose()
    alertProjectIDs?: any;
    @Expose()
    page?: any;
    @Expose()
    tb?: any;
    @Expose()
    priceRange!: string;
    @Expose()
    searchPropertyBedRoom!: string;
    @Expose()
    referrer?: any;
    @Expose()
    limitFrom!: string;
    @Expose()
    limitTo!: string;
}

@Exclude()
export class ProfileResult {
    @Expose()
    imgPath!: string;
    @Expose()
    profileDetails!: ProfileDetails;
    @Expose()
    country!: Country[];
}

@Exclude()
export class ProfileDetails {
    @Expose()
    userID!: string;
    @Expose()
    userTypeID!: string;
    @Expose()
    userTokenID!: string;
    @Expose()
    userName!: string;
    @Expose()
    userEmail!: string;
    @Expose()
    userPassword!: string;
    @Expose()
    userRegNumber!: string;
    @Expose()
    userRegistredDate!: string;
    @Expose()
    userUpdateDate!: string;
    @Expose()
    userLastAccessed!: string;
    @Expose()
    userNewsLetterMode!: string;
    @Expose()
    cityID!: string;
    @Expose()
    countryID!: string;
    @Expose()
    stateID!: string;
    @Expose()
    userFacebook!: string;
    @Expose()
    userTwitter!: string;
    @Expose()
    userLinkedin!: string;
    @Expose()
    userGoogle!: string;
    @Expose()
    userProfilePicture!: string;
    @Expose()
    userCompanyLogo!: string;
    @Expose()
    userDOB!: string;
    @Expose()
    userGender!: string;
    @Expose()
    userLoginType!: string;
    @Expose()
    userStatus!: string;
    @Expose()
    userLockedDate!: string;
    @Expose()
    userInvalidAccessCount!: string;
    @Expose()
    userNewsletterSubscribeStatus!: string;
    @Expose()
    userPropertyAlerts!: string;
    @Expose()
    userRewardPoints!: string;
    @Expose()
    userUrlKey!: string;
    @Expose()
    userPropertyAlertType!: string;
    @Expose()
    userDefaultLanguage!: string;
    @Expose()
    userYearOfInception!: string;
    @Expose()
    isPremium!: string;
    @Expose()
    userSmsAlerts!: string;
    @Expose()
    userAlternateEmail!: string;
    @Expose()
    userOwnerID!: string;
    @Expose()
    otpSentDate!: string;
    @Expose()
    otp!: string;
    @Expose()
    otpVerified!: string;
    @Expose()
    emailVerified!: string;
    @Expose()
    emailVerificationUrlSentDate!: string;
    @Expose()
    emailVerificationCode!: string;
    @Expose()
    countryCode!: string;
    @Expose()
    cityLocID!: string;
    @Expose()
    userPriority!: string;
    @Expose()
    userAlternatePhone!: string;
    @Expose()
    onboardAgencyID!: string;
    @Expose()
    accessToken!: string;
    @Expose()
    salesRepID!: string;
    @Expose()
    individualUserType!: string;
    @Expose()
    userDayFrom!: string;
    @Expose()
    userDayTo!: string;
    @Expose()
    userHoursFrom!: string;
    @Expose()
    userHoursTo!: string;
    @Expose()
    userFeaturedexpert!: string;
    @Expose()
    expertUserOffer!: string;
    @Expose()
    usercompanyAgentBanner!: string;
    @Expose()
    oldUserID!: string;
    @Expose()
    saleCommision?: any;
    @Expose()
    rentCommision?: any;
    @Expose()
    verified!: string;
    @Expose()
    idProof!: string;
    @Expose()
    utilityBill!: string;
    @Expose()
    userPlanType!: string;
    @Expose()
    planID!: string;
    @Expose()
    currencyID!: string;
    @Expose()
    planPrice!: string;
    @Expose()
    planPropertyCount!: string;
    @Expose()
    planAgentCount!: string;
    @Expose()
    planDisplayTime!: string;
    @Expose()
    planEnquiry!: string;
    @Expose()
    planBusinessLogo!: string;
    @Expose()
    planPropertyPerformance!: string;
    @Expose()
    planStatus!: string;
    @Expose()
    planDate!: string;
    @Expose()
    userDetailsID!: string;
    @Expose()
    languageID!: string;
    @Expose()
    userFirstName!: string;
    @Expose()
    userLastName!: string;
    @Expose()
    userCompanyName!: string;
    @Expose()
    userDesignation!: string;
    @Expose()
    userCompanyUrlKey!: string;
    @Expose()
    userPhone!: string;
    @Expose()
    userAddress1!: string;
    @Expose()
    userAddress2!: string;
    @Expose()
    userZip!: string;
    @Expose()
    userAboutMe!: string;
    @Expose()
    userSkills!: string;
    @Expose()
    userDealsIn!: string;
    @Expose()
    userWorkingHour?: any;
    @Expose()
    googleStateName!: string;
    @Expose()
    googleCityName!: string;
    @Expose()
    googleCountryName!: string;
    @Expose()
    googleLocality!: string;
    @Expose()
    dialCode!: string;
}

export class Country {
    @Expose()
    countryID!: string;
    @Expose()
    countryName!: string;
    @Expose()
    countryAvailable!: string;
    @Expose()
    countryIsoA2!: string;
    @Expose()
    countryIsoA3!: string;
    @Expose()
    countryIsoNumber!: string;
    @Expose()
    countryPriority!: string;
    @Expose()
    addressFormatID!: string;
    @Expose()
    countryUrlKey!: string;
}

@Exclude()
export class MyPropertiesResult {
    @Expose({ name: "totalcount" })
    totalCount!: Number;
    @Expose()
    imgPath!: string;
    @Expose({ name: "proprtyList" })
    @Type(() => MyProperty)
    myProperties!: MyProperty[];
}

@Exclude()
export class MyProperty {
    @Expose()
    propertyID!: string;
    @Expose()
    countryName!: string;
    @Expose()
    stateName!: string;
    @Expose()
    cityName!: string;
    @Expose()
    currencySymbolLeft!: string;
    @Expose()
    currencySymbolRight!: string;
    @Expose()
    planID!: string;
    @Expose()
    premiumEndDate!: string;
    @Expose()
    userPlanID!: string;
    @Expose()
    propertyStatus!: string;
    @Expose()
    propertySoldOutStatus!: string;
    @Expose()
    propertyCoverImage!: string;
    @Expose()
    propertyPurpose!: string;
    @Expose()
    address1!: string;
    @Expose()
    address2!: string;
    @Expose()
    propertyAddedDate!: string;
    @Expose()
    propertyUpdateDate!: string;
    @Expose()
    propertyPrice!: string;
    @Expose()
    propertyName!: string;
    @Expose()
    propertyLocality!: string;
    @Expose()
    propertySqrFeet!: string;
    @Expose()
    propertyPlotArea?: any;
    @Expose()
    propertyAreaType?: any;
    @Expose()
    propEnqCount!: string;
    @Expose()
    planTitle!: string;
    @Expose()
    changePlan!: string;
    @Expose()
    activePlanList!: ActivePlanList[];
    @Expose()
    reslastDayViews!: string;
    @Expose()
    restotalListViews?: string;
    @Expose()
    restotalViews?: string;
    @Expose()
    restotalEmailViews!: string;
    @Expose()
    restotalDetailViews!: string;
    @Expose()
    restotalfavor!: string;
    @Expose()
    restotalPhoneViews!: string;
}

@Exclude()
export class ActivePlanList {
    @Expose()
    userPlanID!: string;
    @Expose()
    userID!: string;
    @Expose()
    planDetailsID!: string;
    @Expose()
    planID!: string;
    @Expose()
    planTitle!: string;
    @Expose()
    languageID!: string;
    @Expose()
    userTypeID!: string;
    @Expose()
    currencyID!: string;
    @Expose()
    planPrice!: string;
    @Expose()
    planPropertyCount!: string;
    @Expose()
    planProjectCount!: string;
    @Expose()
    planAgentCount!: string;
    @Expose()
    planEmployeeCount!: string;
    @Expose()
    planDisplayTime!: string;
    @Expose()
    planEnquiry!: string;
    @Expose()
    planEnquiryCount!: string;
    @Expose()
    planPropertyDetails!: string;
    @Expose()
    planBusinessLogo!: string;
    @Expose()
    planPropertyPerformance!: string;
    @Expose()
    planFeatured!: string;
    @Expose()
    planFeaturedCount!: string;
    @Expose()
    planProjectFeatured!: string;
    @Expose()
    planProjectFeaturedCount!: string;
    @Expose()
    planStatus!: string;
    @Expose()
    planDate!: string;
    @Expose()
    planUpdateDate!: string;
    @Expose()
    isDefault!: string;
    @Expose()
    featuredPlanDisplayTime!: string;
    @Expose()
    planphotouploadCount!: string;
    @Expose()
    planVideoUploadingCount!: string;
    @Expose()
    showWhatsAppButton!: string;
    @Expose()
    planOrder!: string;
    @Expose()
    userPremium!: string;
    @Expose()
    planDesigns!: string;
    @Expose()
    planDesignsCount!: string;
}

@Exclude()
export class EnquiriesResult {
    @Expose({ name: "totalcount" })
    totalCount!: Number;
    @Expose({ name: "enquiries" })
    @Type(() => Enquiry)
    enquiries!: Enquiry[];
}

@Exclude()
export class Enquiry {
    @Expose()
    enquiryID!: string;
    @Expose()
    userName!: string;
    @Expose()
    enquiryName!: string;
    @Expose()
    enquiryEmail!: string;
    @Expose()
    enquiryPhone!: string;
    @Expose()
    userEmail!: string;
    @Expose()
    userPhone!: string;
    @Expose()
    enquiryContent!: string;
    @Expose()
    propertyName!: string;
    @Expose()
    userOfferPrice?: string;
    @Expose()
    enquiryPostedDate!: string;
    @Expose()
    callingCode!: string;
}

