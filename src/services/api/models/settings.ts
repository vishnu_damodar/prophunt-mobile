import { StringToBooleanTransform } from "@app/utils/transforms/BooleanTransform";
import { Exclude, Expose, Transform, Type } from "class-transformer";

@Exclude()
export class LanguageResult {
  @Expose()
  @Type(() => LanguageList)
  languageList!: LanguageList[];

  @Expose()
  @Type(() => DefaultCity)
  defaultCity!: DefaultCity;
}

@Exclude()
export class DefaultCity {
  @Expose()
  cityID!: string;
  @Expose()
  cityKeyID!: string;
  @Expose()
  cityName!: string;
  @Expose()
  cityLat!: string;
  @Expose()
  cityLng!: string;
}

@Exclude()
export class LanguageList {
  @Expose()
  languageID!: string;
  @Expose()
  languageName!: string;
}

@Exclude()
export class CommonListingResult {
  @Expose()
  locations!: any[];

  @Expose()
  imgPath!: string;

  @Expose()
  @Type(() => PropertyType)
  propertyType!: PropertyType[];

  @Expose()
  @Type(() => BuyRentPrice)
  buyMinPrice!: BuyRentPrice[];

  @Expose()
  @Type(() => BuyRentPrice)
  buyMaxPrice!: BuyRentPrice[];

  @Expose()
  @Type(() => BuyRentPrice)
  rentMinPrice!: BuyRentPrice[];

  @Expose()
  @Type(() => BuyRentPrice)
  rentMaxPrice!: BuyRentPrice[];
}

@Exclude()
export class BuyRentPrice {
  @Expose()
  "0"?: number;

  @Expose()
  "1"?: string;

  @Expose()
  @Transform(Number)
  price!: number;

  @Expose()
  priceValue!: string;
}

@Exclude()
export class PropertyType {
  @Expose({ name: "propertyTypeID" })
  propertyTypeId!: string;

  @Expose()
  propertyTypeName!: string;

  @Expose()
  @Type(() => PropertyTypeSubCategory)
  subCategory!: PropertyTypeSubCategory[];

  @Expose()
  priceValue?: string;
}

@Exclude()
export class PropertyTypeSubCategory {
  @Expose({ name: "propertyTypeID" })
  propertyTypeId!: string;

  @Expose()
  propertyTypeIcon!: string;

  @Expose()
  propertyTypeName!: string;
}

@Exclude()
export class CityListResponse {
  @Expose()
  @Type(() => CityList)
  cityList!: CityList[];
}

@Exclude()
export class CityList {
  @Expose({ name: "cityID" })
  cityId!: string;

  @Expose()
  cityName!: string;

  @Expose()
  cityLat!: string;

  @Expose()
  cityLng!: string;

  @Expose()
  @Transform(StringToBooleanTransform("Yes"), { toClassOnly: true })
  isDefault!: boolean;
}
