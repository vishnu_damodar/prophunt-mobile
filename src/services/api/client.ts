import {
  SettingsService,
  AuthService,
  SearchService,
  PropertyService,
} from "./services";
import ApiServiceDataStore from "./interfaces/api-data-store";
import { HttpClient } from "./http/http";
import ShortlistService from "./services/shortlist-service";
import DashboardService from "./services/dashboard-service";

export default class ApiClient {
  public auth: AuthService;
  public settings: SettingsService;
  public search: SearchService;
  public property: PropertyService;
  public shortlist: ShortlistService;
  public dashboard: DashboardService;

  private store: ApiServiceDataStore;

  public http: HttpClient;

  constructor(apiDomain: string) {
    this.store = {
      apiDomain,
      token: undefined,
      languageId: "1",  
      webSiteId: "1",
      subSiteId: "1",
    };
    this.http = new HttpClient(this.store);

    this.auth = new AuthService(this.store, this.http);
    this.settings = new SettingsService(this.store, this.http);
    this.search = new SearchService(this.store, this.http);
    this.property = new PropertyService(this.store, this.http);
    this.shortlist = new ShortlistService(this.store, this.http);
    this.dashboard = new DashboardService(this.store, this.http);
  }

  public get apiDomain() {
    return this.store.apiDomain;
  }

  public get token() {
    return this.store.token;
  }

  public setToken(token?: string) {
    this.store.token = token;
  }

  public get websiteId() {
    return this.store.webSiteId;
  }

  public setWebsiteId(webSiteId: string) {
    this.store.webSiteId = webSiteId;
  }

  public get subsiteId() {
    return this.store.subSiteId;
  }

  public setSubsiteId(subSiteId: string) {
    this.store.subSiteId = subSiteId;
  }

  public get languageId() {
    return this.store.languageId;
  }

  public setLanguageId(languageId: string) {
    this.store.languageId = languageId;
  }
}
