import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import ApiServiceDataStore from "../interfaces/api-data-store";
import DeviceInfo from "react-native-device-info";

export class HttpClient {

    public dataStore: ApiServiceDataStore;

    public instance: AxiosInstance;

    constructor(dataStore: ApiServiceDataStore) {
        this.dataStore = dataStore;
        this.instance = axios.create({
            baseURL: dataStore.apiDomain,
            timeout: 5000
        });
    }

    private _getHeaders() {
        if (!this.dataStore.token) {
            return {};
        }
        return {
            Authorization: `Bearer ${this.dataStore.token}`
        };
    }

    public async get<T = any>(url: string): Promise<AxiosResponse<T>> {
        var lastChar = url.substr(-1);
        if (lastChar != "/") {
            url = url + "/";
        }
        const options = {
            headers: {
                ...this._getHeaders()
            }
        };
        const response = this.instance.get<T>(url, options);
        return response;
    }

    public async postFormData<T = any>(
        url: string,
        formData: FormData,
        config?: {
            includeTokenInFormData?: boolean;
            userTokenKey?: string;
            langId?: string;
        }
    ): Promise<AxiosResponse<T>> {
        const fullUrl = `${this.dataStore.apiDomain}${url}`;

        if (config?.includeTokenInFormData && !!this.dataStore.token) {
            formData.append(
                config?.userTokenKey || "userID",
                this.dataStore.token
            );
        }
        formData.append("website_id", this.dataStore.webSiteId);
        formData.append("subsite_id", this.dataStore.subSiteId);
        if (config?.langId) {
            formData.append("languageID", config?.langId);
        } else {
            formData.append("languageID", this.dataStore.languageId);
        }
        // formData.append("deviceID", DeviceInfo.getUniqueId());
        const options: AxiosRequestConfig = {
            url: fullUrl,
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "multipart/form-data;"
            }
        };
        const response = await axios.post<T>(fullUrl, formData, options);
        return response;
    }
}