import { type } from "os";

export class ToasterReferences {
    private static errRef: any;

    private static sucessRef: any;

    static setErrorRef(ref: any) {
        this.errRef = ref;
    }

    static setSucessRef(ref: any) {
        this.sucessRef = ref;
    }

    static showErrorMsg(msg: string) {
        if (msg != null) {
            this.errRef?.current?.show(msg, 1500, "error");
        }
    }

    static showSucessMsg(msg: string) {
        this.sucessRef.current.show(msg, 1500, "success");
    }
}
