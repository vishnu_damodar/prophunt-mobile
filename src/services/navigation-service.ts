import {
    NavigationActions,
    NavigationParams,
    StackActions
} from "react-navigation";
import { DrawerActions } from "react-navigation-drawer";
import { AppRoutes } from "@app/route/route-keys";

export class NavigationService {
    public static navigator: any;

    static initNavigationService(navigatorRef: any) {
        NavigationService.navigator = navigatorRef;
    }

    static navigateAndReset(routeName: string, params?: NavigationParams) {
        NavigationService.navigator.dispatch(
            StackActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({
                        routeName: AppRoutes.Slider
                    }),
                    NavigationActions.navigate({
                        routeName,
                        params
                    })
                ]
            })
        );
    }

    static navigate(routerName: any, params?: NavigationParams) {
        NavigationService.navigator.dispatch(
            NavigationActions.navigate({
                routeName: routerName,
                params
            })
        );
    }

    static pushReplace(routerName: any, params?: NavigationParams) {
        NavigationService.navigator.dispatch(
            StackActions.replace({
                routeName: routerName,
                params
            })
        );
    }

    static goBack() {
        NavigationService.navigator.dispatch(NavigationActions.back());
    }

    static openDrawer() {
        NavigationService.navigator.dispatch(DrawerActions.openDrawer());
    }

    static closeDrawer() {
        NavigationService.navigator.dispatch(DrawerActions.closeDrawer());
    }

    static getCurrentLocation(): IRouteDetails {
        const _navigator = NavigationService.navigator;
        let route = _navigator?.state?.nav;
        while (route?.routes) {
            route = route?.routes[route.index];
        }
        return route;
    }
}

export interface IRouteDetails {
    key: string;
    params: any;
    routeName: string;
}
