/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Platform,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {ToasterReferences} from '@app/services/toast-service';
import Toast from '@app/components/common/toast';
import codePush, {DownloadProgress, UpdateDialog} from 'react-native-code-push';
import {ViewportService} from '@app/config/ui/viewport-service';
import {store, persistor} from './src/store/store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import { AppColors } from '@app/config/ui/color';
import { Fonts } from '@app/config/ui/font';
import { Root } from "./src/main";
import DeviceInfo from 'react-native-device-info';
const hasNotch = DeviceInfo.hasNotch();

const App = (props:any) => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  useEffect(() => {
    const screenWidth = Math.round(Dimensions.get('window').width);

    let updateDetails: UpdateDialog = {
      title: 'Update available ',
      optionalUpdateMessage:
        'An update is available. Would you like to install it?',
    };
    codePush.sync(
      {
        updateDialog: updateDetails,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      syncStatusChangeCallback,
      downloadProgressCallback,
    );
  }, []);
  const syncStatusChangeCallback = (status: number) => {};
  const downloadProgressCallback = (progress: DownloadProgress) => {};

  const successToastRef = useRef(null);
  const errRef = useRef(null);

  useEffect(() => {
    ToasterReferences.setSucessRef(successToastRef);
    ToasterReferences.setErrorRef(errRef);
    const screenWidth = Math.round(Dimensions.get('window').width);
    ViewportService.initialize(screenWidth);
  }, []);

  const CustomToast = Toast as any;

  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <View style={styles.container}>
            <Root {...props} />
            <CustomToast
              ref={errRef}
              style={styles.errorToast}
              position="top"
              positionValue={Platform.OS === 'ios' && hasNotch ? 36 : 0}
              fadeInDuration={700}
              fadeOutDuration={700}
              opacity={1}
              textStyle={styles.errorText}
            />
            <CustomToast
              ref={successToastRef}
              style={styles.successToast}
              position="top"
              positionValue={Platform.OS === 'ios' && hasNotch ? 36 : 0}
              fadeInDuration={700}
              fadeOutDuration={700}
              opacity={1}
              textStyle={styles.successText}
            />
          </View>
        </PersistGate>
      </Provider>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "white"
  },
  errorToast: {
      backgroundColor: AppColors.lightColor,
      padding: 20,
      borderWidth: 1,
      borderColor: AppColors.dangerTextColor,
      width: "100%",
      borderRadius: 0
  },
  successToast: {
      backgroundColor: AppColors.lightColor,
      flex: 1,
      width: "100%",
      padding: 10,
      borderRadius: 0,
      borderColor: AppColors.primaryThemeColor,
      borderWidth: 1,
  },
  errorText: {
      fontSize: ViewportService.normalFont,
      color: AppColors.dangerTextColor,
      fontFamily: Fonts.SemiBold,
      paddingLeft: 10
  },
  successText: {
      color: AppColors.primaryThemeColor,
      fontFamily: Fonts.SemiBold,
      fontSize: ViewportService.normalFont,
      paddingLeft: 10,
      paddingVertical: 10
  }
});
let codePushOptions = {checkFrequency: codePush.CheckFrequency.MANUAL};
export default codePush(codePushOptions)(App);
// export default App;
