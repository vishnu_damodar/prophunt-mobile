module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    "babel-plugin-transform-typescript-metadata",
    ["@babel/plugin-proposal-decorators", { legacy: true }],
    [
        "module-resolver",
        {
            cwd: "babelrc",
            extensions: [".ts", ".tsx", ".js", ".ios.js", ".android.js"],
            alias: {
                "@app": "./src"
            }
        }
    ],
    "jest-hoist",
]
};
